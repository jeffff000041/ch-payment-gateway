<?php

return [

    /*
    |--------------------------------------------------------------------------
    | OGPS Logs URL
    |--------------------------------------------------------------------------
    |
    | This value is the url of the OGPS Logs.
    |
     */

    'url' => env('OGPS_URL', 'https://ogpay.orientalgame88.com/#/'),
    /**
     * @todo  needs to move to a Validation Class
     */
    'password_regex' => env('PASSWORD_REGEX', '/^(?=.*\d)(?=.*[@#$%^&!\*]?)(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#$%^&!\*]/i'),
    'login_key_regex' => env('LOGIN_KEY_REGEX', '/^[0-9]{4,4}$/i'),
    'username_regex' => env('USERNAME_REGEX', '/^[A-Za-z0-9\.\_]+$/'),
    /**
     * @todo  will move to config/walle.php
     */
    'mpdf_tmp_path' => env('MPDF_TMP_PATH', '/tmp/mpdf'),
    'captcha_secret_key' => env('RECAPTCHA_SECRET_KEY', '6Lexz38UAAAAAKR38a74oHsNIWnflumjSlVC7SgZ'),
    'inv_captcha_secret_key' => env('INV_RECAPTCHA_SECRET_KEY', '6Lfqu6AUAAAAAJT1Wut0D2NDWktK3HpeOLRn5iD8'),
    'captcha_url' => env('RECAPTCHA_URL', 'https://www.google.com/recaptcha/api/siteverify'),
];
