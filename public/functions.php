<?php

defined('ENC_METHOD') or define('ENC_METHOD', 'AES-256-CBC');
defined('SECRET_KEY') or define('SECRET_KEY', '(_f~qB 3xz4FV^3/R Qf~C/]v)`|Tw3fek$P[MPW:<huId_a,xv8%+b62.=ruDf');
defined('SECRET_IV') or define('SECRET_IV', 'u8ysY+o|qFFw#T<q]u_if@{v[`v!b&AT<]inf,T-|yR{!X8 /%p^W1&^6k|1MSvW');

if (!function_exists('generateCode')) {
    /**
     * Generate Code
     *
     * @param $prefix
     * @param $id
     *
     * @return string
     */
    function generateCode($prefix, $id)
    {
        $ref_no = $prefix . $id . '-' . strtotime(date("Y-m-d H:i:s"));
        return $ref_no;
    }
}

if (!function_exists('walle_encrypt')) {
    /**
     * @param $value
     * @return mixed
     */
    function walle_encrypt($value)
    {
        // hash
        $key = hash('sha256', SECRET_KEY);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', SECRET_IV), 0, 16);

        $output = openssl_encrypt($value, ENC_METHOD, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }
}

if (!function_exists('walle_decrypt')) {
    /**
     * @param $value
     * @return mixed
     */
    function walle_decrypt($value)
    {
        $key = hash('sha256', SECRET_KEY);
        $iv = substr(hash('sha256', SECRET_IV), 0, 16);

        $output = openssl_decrypt(base64_decode($value), ENC_METHOD, $key, 0, $iv);

        return $output;
    }
}

if (!function_exists('obfuscate_email')) {
    /**
     * @param $email (sample: 'mysampleemail@domain.com')
     * @return string (sample: 'my***********@domain.com')
     */
    function obfuscate_email($email)
    {
        $em   = explode("@",$email);
        $name = implode(array_slice($em, 0, count($em)-1), '@');
        $showIndex = 2;
        $len  = strlen($name) - $showIndex;

        return substr($name,0, $showIndex) . str_repeat('*', $len) . "@" . end($em);   
    }
}

if (!function_exists('generate_login_key')) {
    /**
     * @param $value
     * @return mixed
     */
    function generate_login_key()
    {
        $digits = 4;
        return str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT); 
    }
}

if (!function_exists('generate_reference_key')) {
    /**
     * @param $value
     * @return mixed
     */
    function generate_reference_key()
    {
        $limit = 6;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < $limit; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;   
    }
}

if (!function_exists('ogps_url')) {
    /**
     * @param $value
     * @return mixed
     */
    function ogps_url($value)
    {
        return config('ogps.url') . $value;   
    }
}