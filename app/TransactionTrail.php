<?php

namespace App;

use App\Traits\EncryptID;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class TransactionTrail extends Model
{
    // use EncryptID;
    use EncryptID;
    /**
     * Status
     */
    const PROCESSING = 'PROCESSING';
    const CANCELLED = 'CANCELLED';
    const CONFIRMED = 'CONFIRMED';
    const EXPIRED = 'EXPIRED';
    const FAILED = 'FAILED';

    /**
     * Execution Type
     */
    const EXEC_TYPE_REQUEST = 'REQUEST';
    const EXEC_TYPE_RESPONSE = 'RESPONSE';

    /**
     * Message Text
     */
    const TRANSACTION_TEXT = 'This transaction has';

    /**
     * @var array
     */
    protected $fillable = [
        'transaction_id',
        'status',
        'execution_type',
        'data_json',
    ];

    protected $hidden = [
        'transaction_id',
        'hash_transaction_id',
    ];

    protected $appends = [
        'hash_transaction_id',
    ];

    /**
     * @return mixed
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    /**
     * @param $value
     */
    public function getHashTransactionIdAttribute()
    {
        return walle_encrypt($this->transaction_id);
    }

    /**
     * @param $value
     */
    public function setHashTransactionIdAttribute()
    {
        return walle_decrypt($this->transaction_id);
    }
}
