<?php

namespace App;

use App\Traits\EncryptID;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class BackOfficeNote extends Model implements Auditable
{
    use EncryptID, \OwenIt\Auditing\Auditable;


    /**
     * @var array
     */
    protected $fillable = [
        'notes',
        'transaction_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
