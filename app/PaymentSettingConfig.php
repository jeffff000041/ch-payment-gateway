<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class PaymentSettingConfig extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    /**
     * @return mixed
     */
    public function payment_setting()
    {
        return $this->hasOne(PaymentSetting::class);
    }

    /**
     * @return mixed
     */
    public function field_category_payment_provider_field()
    {
        return $this->hasOne(FieldCategoryPaymentProviderField::class);
    }
}
