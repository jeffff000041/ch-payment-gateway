<?php

namespace App\Observers;

use Log;
use DB;
use Carbon\Carbon;
use App\TransactionTrail;
use App\PaymentSetting;
use App\Transaction;
use App\Repositories\Contracts\TransactionContract;
use App\Repositories\Contracts\PaymentSettingContract;
use App\Modules\Projects\ProjectHookFactory;

class TransactionTrailObserver
{
    /**
     * @var mixed
     */
     protected $transactionService;

    /**
    * @param PaymentSettingContract $transactionService
     */
    public function __construct(TransactionContract $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * Handle the transactionTrail "created" event.
     *
     * @param  \App\TransactionTrail  $transactionTrail
     * @return void
     */
    public function created(TransactionTrail $transactionTrail)
    {

        $this->updateOverrideAccumulatedAmount($transactionTrail);

        $this->updateApprovalStatus($transactionTrail);

        $this->callNotifications($transactionTrail);

        // Log::info('ID: '. $payment_settings->id);
        // Log::info('Payment Settings Max Amount: '.$payment_settings->maximum_amount);
        // Log::info('Total TransactionTrail: '.$this->transactionService->getTotalTransactions($payment_settings->id));
    }

    /**
     * Update column overrride_accumulated_transaction_amount
     *
     * @param  transactionTrail
     * @return void
     */
    private function updateOverrideAccumulatedAmount(TransactionTrail $transactionTrail) {
        $payment_settings = $transactionTrail->transaction->payment_setting;
        
        if (!is_null($payment_settings->override_accumulated_amount) && 
            $transactionTrail->execution_type == TransactionTrail::EXEC_TYPE_RESPONSE) {
            if ($transactionTrail->status === PaymentSetting::STATUS_CONFIRMED) {
                $payment_settings->update([
                    'override_accumulated_amount' => 
                    ($payment_settings->override_accumulated_amount + $transactionTrail->transaction->amount),
                ]);
            }
        }
    }

    /**
     * Update column approval status when failed or processing
     *
     * @param  transactionTrail
     * @return void
     */
    private function updateApprovalStatus(TransactionTrail $transactionTrail) {
        $transaction = $transactionTrail->transaction;

        if ($transactionTrail->execution_type == TransactionTrail::EXEC_TYPE_REQUEST) {
            if ($transactionTrail->status == TransactionTrail::PROCESSING || 
                $transactionTrail->status == TransactionTrail::FAILED) {
                $transaction->update([
                    'approval_status' => $transactionTrail->status,
                ]);    
            }
        }
        if ($transactionTrail->execution_type == TransactionTrail::EXEC_TYPE_RESPONSE) {
            if ($transactionTrail->status == TransactionTrail::FAILED) {
                $transaction->update([
                    'approval_status' => $transactionTrail->status,
                ]);    
            }
            if ($transactionTrail->status == TransactionTrail::CONFIRMED) {
                $transaction->update([
                    'approval_status' => Transaction::FOR_REVIEW,
                ]);    
            }
        }
    }

    private function callNotifications($transactionTrail)
    {

        try{
            $projectHookFactory = new ProjectHookFactory();
            $payment_settings = $transactionTrail->transaction->payment_setting;
            $maximum_amount = $payment_settings->maximum_amount;
            $percentage_to_notify = $payment_settings->percentage_to_notify;
            $accumulated_amount = $this->transactionService->getTotalTransactions($payment_settings->id);
            $amount_percentage = function ($amount, $percentage) {
                return ceil(($amount * $percentage) / 100);
            };

            if ($accumulated_amount >= $amount_percentage($maximum_amount, Transaction::AUTO_DISABLED_PERCENTAGE)) {
                $payment_settings->update([
                    'auto_disabled' => PaymentSetting::AUTO_DISABLED,
                    'is_active' => PaymentSetting::INACTIVE,
                ]);
                $projectHookFactory->getClass()->maxAmountNotification($transactionTrail, $accumulated_amount);
            }

            if ($accumulated_amount >= $amount_percentage($maximum_amount, $percentage_to_notify)) {
                $projectHookFactory->getClass()->maxPercentageNotification($transactionTrail, $accumulated_amount);
            }
        }catch(\InvalidArgumentException $ex){
            \Log::info('---NOTIFICATION HOOK START----');
            \Log::info("Post Request From Payment Provider Without using JWT Auth to update Transaction Trail");
            \Log::info($ex);
            \Log::info('---NOTIFICATION HOOK END----');
        }


    }
}