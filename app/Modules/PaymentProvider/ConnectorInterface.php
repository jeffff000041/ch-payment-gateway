<?php

namespace App\Modules\PaymentProvider;

use App\Http\Requests\API\TransactionRequest;

interface PaymentProviderInterface
{
    /**
     * Generate request data
     */
    public function generateRequestData($bank_code);

    /**
     * Check transaction status
     */
    public function checkTransactionStatus($string);

    /**
     * do something before save
     */
    public function preSave(TransactionRequest $request, $payment_provider);

    /**
     * do something before update
     */
    public function postSave($transaction, $payment_provider, $request, $currency);

}
