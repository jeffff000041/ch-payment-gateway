<?php

namespace App\Modules\PaymentProvider\Exceptions;

use App\Exceptions\BaseException;

class ValidationException extends BaseException
{
}
