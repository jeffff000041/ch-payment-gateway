<?php

namespace App\Modules\PaymentProvider\Exceptions;

use Exception;

class ConnectorNotFoundException extends Exception
{
}
