<?php

namespace App\Modules\PaymentProvider\Exceptions;

use App\TransactionTrail;
use App\Exceptions\BaseException;

class UnknownPaymentProviderError extends BaseException
{
    /**
     * @var mixed
     */
    protected $transaction;

    public function storeFailTransactionTrail()
    {
        $transaction = $this->getTransaction();

        //save transaction trail
        $transaction_trail = new TransactionTrail([
            'transaction_id' => $transaction->id,
            'amount' => $transaction->amount,
            'status' => TransactionTrail::FAILED,
            'execution_type' => TransactionTrail::EXEC_TYPE_RESPONSE,
            'data_json' => '',
        ]);

        $transaction_trail->save();
    }

    /**
     * @param $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
}
