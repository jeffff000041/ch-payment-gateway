<?php

namespace App\Modules\PaymentProvider\Exceptions;

use App\TransactionTrail;
use App\Exceptions\BaseException;

class PostValidationException extends BaseException
{
    /**
     * @var mixed
     */
    protected $transaction;

    /**
     * @var mixed
     */
    protected $form;


    public function storeFailTransactionTrail()
    {
    	$transaction = $this->getTransaction();
    	$form = $this->getForm();
        
        //save transaction trail
        $transaction_trail = new TransactionTrail([
            'transaction_id' => $transaction->id,
            'amount' => $transaction->amount,
            'status' => TransactionTrail::FAILED,
            'execution_type' => TransactionTrail::EXEC_TYPE_RESPONSE,
            'data_json' => json_encode([
                'transaction_data' => $transaction,
                'status' => isset($form['transaction_status']) ? $form['transaction_status'] : '',
                'post_data' => isset($form['post_data']) ? $form['post_data'] : '',
                'payment_provider_response' => isset($form['response_data']) ? $form['response_data'] : '',
            ]),
        ]);

        $transaction_trail->save();
    }

    /**
     * @param $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param $transaction
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return mixed
     */
    public function getForm()
    {
        return $this->form;
    }


}
