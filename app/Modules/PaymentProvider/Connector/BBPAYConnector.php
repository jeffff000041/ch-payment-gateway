<?php

namespace App\Modules\PaymentProvider\Connector;

use App\TransactionTrail;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\URL;
use App\Http\Requests\API\TransactionRequest;
use App\Modules\PaymentProvider\BaseConnector;
use App\Modules\PaymentProvider\PaymentProviderInterface;
use App\Modules\PaymentProvider\Exceptions\ValidationException;
use App\Modules\PaymentProvider\Exceptions\UnknownPaymentProviderError;

class BBPAYConnector extends BaseConnector implements PaymentProviderInterface
{
    const SUBMIT_URL_PROD_DEPOSIT = 'http://payment.juhuizf.cn/bank/index.aspx';

    const POSTBACK_URL = '/provider/update';

    /**
     * @inheritdoc
     */
    public function generateRequestData($bank_code)
    {
        return '200';
    }

    public function checkStransactionStatus()
    {
        return '200';
    }

    public function generateSignature($arr_params, $merchant_key)
    {
        $keyString = '';
        foreach ($arr_params as $key => $value) {
            $keyString .= $value;
        }

        return md5($arr_params);
    }

    /**
     * @return mixed
     */
    public function generateFormDeposit()
    {
        $fields = array();

        return $fields;
    }

    /**
     * @return int
     */
    public function catchReturnURL($request)
    {
        $transaction = new \stdClass();
        $transaction->amount = $request->amount;
        $transaction->id = explode('-', $request->company_order_num)[1];

        if (isset($request->pay_time)) {
            $transaction->status = TransactionTrail::CONFIRMED;
        } else {
            $transaction->status = TransactionTrail::FAILED;
        }

        \Log::info('---NOTIFICATION HOOK START----');
        \Log::info('__BBPAYConnector Success response__');
        \Log::info($request);
        \Log::info('---NOTIFICATION HOOK END----');

        //TODO: return the redirection url, success/error message on transaction status
        $data = array(
            'data_json' => json_encode($request->all()),
            'transaction_status' => $transaction->status,
            'transaction' => $transaction,
            'PP_expected_return' => array(
                'company_order_num' => $request->company_order_num,
                'mownecum_order_num' => $request->mownecum_order_num,
                'status' => '1'
            ),
        );

        return $data;
    }

    /**
     * Check transaction status
     */
    public function checkTransactionStatus($string)
    {

    }

    /**
     * do something before save
     */
    public function preSave(TransactionRequest $request, $payment_provider)
    {
        return true;
    }

    /**
     * do something before update
     */

    public function postSave($transaction, $payment_provider, $request, $currency)
    {
        //this is for transaction type = deposit
        if ($request->get('type') == 'deposit') {

            $data_array = array(
                'parter' => '',
                'type' => $payment_provider->bank_code,
                'value' => number_format((float)$request->amount, 2, '.', ''),
                'orderid' => $transaction->transaction_number,
                'callbackurl' => URL::current() . self::POSTBACK_URL,
//                'sign' => '',
//                'attach' => '',
//                'hrefbackurl' => '',
            );

            $merchant_key = '';
            foreach ($payment_provider->configuration as $v) {
                if ($v['value'] != '' && $v['code'] != 'merchant_key') {
                    $data_array[$v['code']] = $v['value'];
                } else if ($v['code'] == 'merchant_key') {
                    $merchant_key = $v['value'];
                }
            }

            $sign = $this->generateSignature($data_array, $merchant_key);
            $data_array['sign'] = $sign;

            // Get the current environment
            //$url = App::environment() == 'local' ? $url = self::SUBMIT_URL_DEV : $url = self::SUBMIT_URL_PROD;
            $url = self::SUBMIT_URL_PROD_DEPOSIT;

            $response = Curl::to($url)
                ->withHeaders([
                    'Content-Type' => 'multipart/form-data',
                ])
                ->withData(
                    $data_array
                )
//                ->asJson(true)
                ->returnResponseObject(true)
                ->post();

//            var_dump($response,$data_array);exit;

            if (!isset($response['Success'])) {

                \Log::info('---NOTIFICATION HOOK START----');
                \Log::info($url);
                \Log::info(json_encode($data_array));
                \Log::info('__BBPAYConnector__');
                \Log::info($response);
                \Log::info('---NOTIFICATION HOOK END----');

                $exception = new UnknownPaymentProviderError('', 422);
                $arr_error = array();
                $arr_error['errors']['payment_provider'] = ['Unknown error by payment provider'];
                $exception->setErrors($arr_error);
                $exception->setTransaction($transaction);

                throw $exception;
            }

            $success = false;
            if ($response['error_msg'] == '') { //transaction success
                $success = true;
            }

            //TODO: return the redirection url, success/error message on transaction status
            $data = array(
                'response_data' => $response,
                'transaction_status' => 'PROCESSING',
                'post_data' => $data_array,
                'redirect' => true,
                'success' => $success,
            );

            return $data;
        }

    }
}
