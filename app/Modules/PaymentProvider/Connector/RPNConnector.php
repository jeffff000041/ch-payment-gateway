<?php

namespace App\Modules\PaymentProvider\Connector;

use http\Env\Request;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\URL;
use phpDocumentor\Reflection\Types\Boolean;
use App\Http\Requests\API\TransactionRequest;
use App\Modules\PaymentProvider\BaseConnector;
use App\Modules\PaymentProvider\PaymentProviderInterface;
use App\Modules\PaymentProvider\Exceptions\UnknownPaymentProviderError;

class RPNConnector extends BaseConnector implements PaymentProviderInterface
{
    //const SUBMIT_URL_PROD = 'http://deposit-api.rapidpaymentsnetwork.com/pay.php';
    //const SUBMIT_URL_PROD = 'http://deposit-api.rapidpaymentsnetwork.com/pay.php';
    const SUBMIT_URL_PROD = 'https://deposit.paylomo.net/pay.php';

    //const SUBMIT_URL_PROD_WITHDRAW = "https://withdrawal-api.rpnpay.com/payout.php";

    const POSTBACK_URL = '/provider/update';

    /**
     * @inheritdoc
     */
    public function generateRequestData($bank_code)
    {
        return 200;
    }

    /**
     * @inheritdoc
     */
    public function postSave($transaction, $payment_provider, $request, $currency)
    {
        $key = '';
        $notify_url = URL::current() . self::POSTBACK_URL; // backendcatchReturnURL
        $return_url = "http://frontendOGPS/notifyUser"; // frontend
        $data_array = array(
            'version' => '1.0',
            'sign_type' => 'MD5',
            'mid' => '',
            'return_url' => $return_url,
            'notify_url' => $notify_url,
            'order_id' => $transaction->transaction_number,
            'order_amount' => $transaction->amount * 100,
            'order_time' => $this->convertToAlpaNum($transaction->created_at),
            'bank_id' => $payment_provider->bank_code,
        );

        foreach ($payment_provider->configuration as $v) {
//            if ($v['value'] != '' && $v['code'] != 'key'){
            if ($v['value'] != '') {
                $data_array[$v['code']] = $v['value'];
            }
        }

        $params = array();
        foreach ($data_array as $field => $value) {
            if ($value == '') continue;
            $params[] = "$field=$value";
        }

        $data_array['signature'] = md5(implode('|', $params));

        $url = self::SUBMIT_URL_PROD;

        $response = Curl::to($url)
            ->withData(http_build_query($data_array))
            ->withContentType('application/x-www-form-urlencoded')
            ->post();

        $success = true;

        //TODO: Catch the html valid string and return error if not found
        if (!strstr($response,'agent_bill_time')) {

            \Log::info('---NOTIFICATION HOOK START----');
            \Log::info($url);
            \Log::info(json_encode($data_array));
            \Log::info('__RPNConnector__');
            \Log::info($response);
            \Log::info('---NOTIFICATION HOOK END----');

            $exception = new UnknownPaymentProviderError('', 422);
            $arr_error = array();
            $arr_error['errors']['payment_provider'] = ['Unknown error by payment provider'];
            $exception->setErrors($arr_error);
            $exception->setTransaction($transaction);
        }

        //TODO: return the redirection url, success/error message on transaction status
        $data = array(
            'response_data' => $response,
            'transaction_status' => 'PROCESSING',
            'post_data' => $data_array,
            'redirect' => true,
            'success' => $success,
        );

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function checkTransactionStatus($data): Boolean
    {
        //TODO: check the status of transaction in the response of the payment provider

        return true;
    }

    private function convertToAlpaNum($value)
    {
        return preg_replace("/[^a-zA-Z0-9]+/", "", $value);
    }

    /**
     * @inheritdoc
     */
    public function preSave(TransactionRequest $request, $payment_provider)
    {
        //test Connection
        /*
        $response = Curl::to(self::submit_url_prod_deposit)
            ->withResponseHeaders()
            ->returnResponseObject()
            ->get();

        return $response->content();
        */

        return true;
    }


    public function generateSignature()
    {
        //$signature2 = md5(implode('|', $params));
        return 'Signature: samplesignature_12342314e2fsd';
    }

    public function generateFormDeposit()
    {
        return array(
            'version' => '1.0', // constant
            'sign_type' => 'MD5', // constant
            'mid' => 'val1', // from PPconfig
            'return_url' => 'URL', // it will show to frontier
            'notify_url' => 'URL', // it must be the route the lead to catchReturnURL()
            'order_id' => '', // system generated
            'order_amount' => '10.00',
            'order_time' => '20140603200401',
            'bank_id' => '22',
            'signature' => 'val2' // $signature2 = md5(implode('|', $params));
        );
    }

    public function catchReturnURL($request): String
    {
        $fileLocation = storage_path('logs/PPResponseText.txt');
        $file = fopen($fileLocation,"w");
        $content = json_encode($request->all());
        fwrite($file,$content);
        fclose($file);

        return '[Success]';

        /*
        $transaction = new \stdClass();
        $transaction->amount = $request->amount;
        $transaction->id = explode('-', $request->company_order_num)[1];

        if (isset($request->pay_time)) {
            $transaction->status = TransactionTrail::CONFIRMED;
        } else {
            $transaction->status = TransactionTrail::FAILED;
        }

        //TODO: return the redirection url, success/error message on transaction status
        $data = array(
            'data_json' => json_encode($request->all()),
            'transaction_status' => $transaction->status,
            'transaction' => $transaction,
            'PP_expected_return' => '200',
        );

        return $data;
        */
    }

}