<?php

namespace App\Modules\PaymentProvider\Connector;

use App\TransactionTrail;
use Ixudra\Curl\Facades\Curl;
use App\Http\Requests\API\TransactionRequest;
use App\Modules\PaymentProvider\BaseConnector;
use App\Modules\PaymentProvider\PaymentProviderInterface;
use App\Modules\PaymentProvider\Exceptions\ValidationException;
use App\Modules\PaymentProvider\Exceptions\UnknownPaymentProviderError;

class JBPConnector extends BaseConnector implements PaymentProviderInterface
{
    const SUBMIT_URL_PROD_DEPOSIT = 'https://api.jbp-pay.com/apply/Deposit';

    const POSTBACK_URL = '/provider/update';

    /**
     * @inheritdoc
     */
    public function generateRequestData($bank_code)
    {
        return '200';
    }

    public function checkStransactionStatus()
    {
        return '200';
    }

    public function generateSignature()
    {
        return 'Signature: samplesignature_12342314e2fsd';
    }

    /**
     * @return mixed
     */
    public function generateFormDeposit()
    {
        $fields = array();

        return $fields;
    }

    /**
     * @return int
     */
    public function catchReturnURL($request)
    {
        $transaction = new \stdClass();
        $transaction->amount = $request->amount;
        $transaction->id = explode('-', $request->company_order_num)[1];

        if (isset($request->pay_time)) {
            $transaction->status = TransactionTrail::CONFIRMED;
        } else {
            $transaction->status = TransactionTrail::FAILED;
        }

        \Log::info('---NOTIFICATION HOOK START----');
        \Log::info('__JBPConnector Success response__');
        \Log::info($request);
        \Log::info('---NOTIFICATION HOOK END----');

        //TODO: return the redirection url, success/error message on transaction status
        $data = array(
            'data_json' => json_encode($request->all()),
            'transaction_status' => $transaction->status,
            'transaction' => $transaction,
            'PP_expected_return' => array(
                'company_order_num' => $request->company_order_num,
                'mownecum_order_num' => $request->mownecum_order_num,
                'status' => '1'
            ),
        );

        return $data;
    }

    /**
     * Check transaction status
     */
    public function checkTransactionStatus($string)
    {

    }

    /**
     * do something before save
     */
    public function preSave(TransactionRequest $request, $payment_provider)
    {
        $exception = new ValidationException('test message', 422);
        $arr_error = array();

        if (empty($request->get('type'))) {
            $arr_error['errors']['err_msg']['type'] = 'Type field is required';
            $exception->setErrors($arr_error);
            throw $exception;
        }

        if ($payment_provider->bank_name == 'Alipay') {
            if ($request->amount < 100 || $request->amount > 10000) { //based on JBP groupchat
                $arr_error['errors']['amount'] = ['Amount must be 100.00 -  10,000.00'];
                $exception->setErrors($arr_error);

                throw $exception;
            }
        }

        if ($payment_provider->bank_name == 'UNIONPAY') {
            if ($request->amount < 50 || $request->amount > 3000) { //based on docs
                $arr_error['errors']['amount'] = ['Amount must be 100.00 -  3,000.00'];
                $exception->setErrors($arr_error);

                throw $exception;
            }
        }

        return true;
    }

    /**
     * do something before update
     */

    public function postSave($transaction, $payment_provider, $request, $currency)
    {
        //this is for transaction type = deposit
        if ($request->get('type') == 'deposit') {
            $return_url = "http://frontendOGPS/notifyUser"; // frontend

            $data_array = array(
                'company_id' => '',
                'bank_id' => $payment_provider->bank_code,
                'amount' => number_format((float)$request->amount, 2, '.', ''),
                'company_order_num' => $transaction->transaction_number,
                'company_user' => $request->reference_number,
                'estimated_payment_bank' => $payment_provider->bank_code,
                'deposit_mode' => '2', // Deposit Mode: 2 Faster deposit process; Online Banking, Wechat, Alipay
                'group_id' => '0', //default: 0
                'web_url' => $return_url,
                'memo' => $transaction->transaction_number,
                'note' => 'null', //If Deposit Mode 2, this field can be empty
                'note_model' => '2', // 1 Merchant Note 2 Payment Platform Note 3 Payment Platform Amount 4 Merchant Amount 5 Name Match (Payer’s Name)
            );

            $config = '';
            foreach ($payment_provider->configuration as $v) {
                if ($v['value'] != '' && $v['code'] != 'config') {
                    $data_array[$v['code']] = $v['value'];
                } else if ($v['code'] == 'config') {
                    $config = $v['value'];
                }
            }

            $config = md5($config);

            $keyString = $config;
            foreach ($data_array as $key => $value) {
                if ($value == null) {
                    $keyString .= 'null';
                } else {
                    $keyString .= $value;
                }
            }

            $data_array['terminal '] = 'null';
            $data_array['key'] = md5($keyString);

            // Get the current environment
            //$url = App::environment() == 'local' ? $url = self::SUBMIT_URL_DEV : $url = self::SUBMIT_URL_PROD;
            $url = self::SUBMIT_URL_PROD_DEPOSIT;

            $response = Curl::to($url)
                ->withHeaders([
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ])
                ->withData(
                    $data_array
                )
                ->asJson(true)
                ->post();

            if (!isset($response['mownecum_order_num']) || $response['mownecum_order_num'] == "") {

                \Log::info('---NOTIFICATION HOOK START----');
                \Log::info($url);
                \Log::info(json_encode($data_array));
                \Log::info('__JBPConnector__');
                \Log::info($response);
                \Log::info('---NOTIFICATION HOOK END----');

                $exception = new UnknownPaymentProviderError('', 422);
                $arr_error = array();
                $arr_error['errors']['payment_provider'] = ['Unknown error by payment provider'];
                $exception->setErrors($arr_error);
                $exception->setTransaction($transaction);

                throw $exception;
            }

            $success = false;
            if ($response['error_msg'] == '') { //transaction success
                $success = true;
            }

            //TODO: return the redirection url, success/error message on transaction status
            $data = array(
                'response_data' => $response,
                'transaction_status' => 'PROCESSING',
                'post_data' => $data_array,
                'redirect' => true,
                'success' => $success,
            );

            return $data;
        }

    }
}
