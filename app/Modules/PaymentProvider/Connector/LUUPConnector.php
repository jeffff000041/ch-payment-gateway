<?php

namespace App\Modules\PaymentProvider\Connector;

use Ixudra\Curl\Facades\Curl;
use App\Modules\PaymentProvider\BaseConnector;
use App\Modules\PaymentProvider\PaymentProviderInterface;
use Illuminate\Support\Facades\App;

class LUUPConnector extends BaseConnector implements PaymentProviderInterface
{
    const submit_url_prod = 'https://api.astropaycard.com/';

    const submit_url_dev = 'https://sandbox-api.astropaycard.com/';

    const deposit_method = 'verif/validator';

    const transaction_status_method = 'verif/transtatus';


    /**
     * @inheritdoc
     */
    public function postSave($transaction,$payment_provider)
    {

        $data_array = array();

        $data_array['x_login'] = '';
        $data_array['x_trans_key'] = '';
        $data_array['x_invoice_num'] = $transaction->transaction_number;
        $data_array['x_amount'] = $transaction->amount;
        $data_array['x_type'] = "AUTH_CAPTURE";
        $data_array['x_response_format'] = 'json';

        //user input
        $data_array['x_currency'] = 'RMB';
        $data_array['x_card_num'] = '1615700205928872';
        $data_array['x_card_code'] = '2228';
        $data_array['x_exp_date'] = '09-2019';


        foreach ($data_array as $key=>$val) {
            if($val == ''){
                foreach ($payment_provider->configuration as $v) {
                    if($key == $v['code']){
                        $data_array[$key] = $v['value'];
                    }
                }
            }
        }

        // Get the current environment
        $url = App::environment() == 'local' ? $url = self::submit_url_dev : $url = self::submit_url_prod;

        $response = Curl::to($url.self::deposit_method)
            ->withData(
                $data_array
            )
            ->post();

        $response = json_decode($response);

//        if(__::get($response, 'response_code') == 1){
//
//        }

        //TODO: return the redirection url, success/error message on transaction status
        return array(
            'response_data' => $response,
            'transaction_status' => '',
            'post_data' => $data_array,
        );
    }

    /**
     * @inheritdoc
     */
    public function preSave(){
        //test Connection
        /*
        $response = Curl::to(self::submit_url_prod_deposit)
            ->withResponseHeaders()
            ->returnResponseObject()
            ->get();

        return $response->content();
        */

        return true;
    }

    /**
     * @inheritdoc
     */
    public function generateRequestData($bank_code)
    {
        return '200';
    }

    public function checkTransactionStatus($string)
    {
        $fields = array(
            'x_login' => '_login', // from PPconfig
            'x_trans_key' => 'pass', // from PPconfig
            'x_invoice_num' => 'dsa3jg438n', // from transaction db
            'x_response_format' => 'json' //constant
        );

        return $fields;
    }

    public function generateSignature()
    {
        return 'Signature: samplesignature_12342314e2fsd';
    }

    public function generateFormDeposit()
    {
        $fields = array(
            'x_login' => "_login", // from PPconfig
            'x_trans_key' => "_pass", // from PPconfig
            'x_type' => 'AUTH_CAPTURE', // constant
            'x_card_num' => 'x_card_num', // from user
            'x_card_code' => 'x_card_code', // from user
            'x_exp_date' => 'x_exp_date', // from user
            'x_amount' => 'x_amount', // from user
            'x_currency' => 'x_currency', // from user
            'x_invoice_num' => 'x_invoice_num',
            'x_response_format' => 'json' // constant
        );

        return $fields;
    }

    public function catchReturnURL()
    {
        return 200;
    }
}
