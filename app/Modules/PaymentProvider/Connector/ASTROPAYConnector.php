<?php

namespace App\Modules\PaymentProvider\Connector;

use App\TransactionTrail;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\App;
use App\Http\Requests\API\TransactionRequest;
use App\Modules\PaymentProvider\BaseConnector;
use App\Modules\PaymentProvider\PaymentProviderInterface;
use App\Modules\PaymentProvider\Exceptions\ValidationException;
use App\Modules\PaymentProvider\Exceptions\PostValidationException;
use App\Modules\PaymentProvider\Exceptions\UnknownPaymentProviderError;


class ASTROPAYConnector extends BaseConnector implements PaymentProviderInterface
{
    const SUBMIT_URL_PROD = 'https://api.astropaycard.com/';

    const SUBMIT_URL_DEV = 'https://sandbox-api.astropaycard.com/';

    const DEPOSIT_METHOD = 'verif/validator';

    const WITHDRAWAL_METHOD = 'cashOut/sendCardToMobile';

    const TRANSACTION_STATUS_METHOD = 'verif/transtatus';

    /**
     * @inheritdoc
     */
    public function postSave($transaction, $payment_provider, $request, $currency)
    {
        $exception = new ValidationException('test message', 422);
        $arr_error = array();

        if (empty($request->get('type'))) {
            $arr_error['errors']['err_msg']['type'] = 'Type field is required';
            $exception->setErrors($arr_error);
            throw $exception;
        }

        // Get the current environment
        $url = App::environment() == 'local' ? $url = self::SUBMIT_URL_DEV : $url = self::SUBMIT_URL_PROD;

        $response = "";

        //this is for transaction type = deposit
        if ($request->get('type') == 'deposit') {

            $data_array = array();

            $data_array['x_login'] = '';
            $data_array['x_trans_key'] = '';
            $data_array['x_invoice_num'] = $transaction->transaction_number;
            $data_array['x_amount'] = $transaction->amount;
            $data_array['x_type'] = "AUTH_CAPTURE";
            $data_array['x_response_format'] = 'json';

            //user input
            $data_array['x_currency'] = $currency->currency_code; //'RMB';
            $data_array['x_card_num'] = $request->get('card_num'); //'1615700205928872';
            $data_array['x_card_code'] = $request->get('card_code'); //'2228';
            $data_array['x_exp_date'] = $request->get('card_expiration'); //'09-2019';

            foreach ($data_array as $key => $val) {
                if ($val == '') {
                    foreach ($payment_provider->configuration as $v) {
                        if ($key == $v['code']) {
                            $data_array[$key] = $v['value'];
                        }
                    }
                }
            }

            // Get the current environment
            $url = App::environment() == 'local' ? $url = self::SUBMIT_URL_DEV : $url = self::SUBMIT_URL_PROD;

            $response = Curl::to($url . self::DEPOSIT_METHOD)
                ->withData(
                    $data_array
                )
                ->post();

            $response = json_decode($response);

            if (!isset($response->response_code)) {

                \Log::info('---NOTIFICATION HOOK START----');
                \Log::info($url);
                \Log::info(json_encode($data_array));
                \Log::info('__ASTROPAYConnector__');
                \Log::info($response);
                \Log::info('---NOTIFICATION HOOK END----');

                $exception = new UnknownPaymentProviderError('', 422);
                $arr_error = array();
                $arr_error['errors']['payment_provider'] = ['Unknown error by payment provider'];
                $exception->setErrors($arr_error);
                $exception->setTransaction($transaction);

                throw $exception;
            }
        } else {

            $data_array = array();

            // secret_key ok
            $data_array['x_login'] = '';
            $data_array['x_trans_key'] = '';
            $data_array['x_amount'] = $transaction->amount; //Emma backend
            $data_array['x_currency'] = $currency->currency_code; //Emma backend
            $data_array['x_mobile_number'] = $request->mobile_number; //Emma backend
            $data_array['x_name'] = $request->receiver_name; //Emma backend
            $data_array['x_document'] = $transaction->reference_number; // Emma backend
            $data_array['x_country'] = $currency->country_code; // User country code - receiver country code eg. CNY, PHP, etc.. //Emma backend
            $data_array['x_reference'] = $transaction->transaction_number; // Merchant's internal cashout reference -
            $data_array['notification_url'] = 'http://youtube.com/confirmed'; // from settings


            $secret_key = '';
            foreach ($data_array as $key => $val) {
                if ($val == '') {
                    foreach ($payment_provider->configuration as $v) {
                        if ($key == $v['code']) {
                            $data_array[$key] = $v['value'];
                        }

                        if ($v['code'] == 'secret_key') {
                            $secret_key = $v['value'];
                        }
                    }
                }
            }


            $data_array['x_control'] = sha1($secret_key . number_format($data_array['x_amount'], 2, '.', '') . $data_array['x_currency'] . $data_array['x_mobile_number']);

            $response = Curl::to($url . self::WITHDRAWAL_METHOD)
                ->withData(
                    $data_array
                )
                ->post();

        }


        $success = false;
        $transaction_status = '';
        if ($request->type == 'deposit') {
            if ($response->response_code == 1) {
                $success = true;
                $transaction_status = TransactionTrail::CONFIRMED;
            }
        } else if ($request->type == 'withdraw') {
            //$response = json_decode($response);

            //if ($response->response == 'SUCCESS') {
            $success = true;
            $transaction_status = TransactionTrail::PROCESSING;

            // }
        }


        //TODO: return the redirection url, success/error message on transaction status
        $data = array(
            'response_data' => $response,
            'transaction_status' => $transaction_status,
            'post_data' => $data_array,
            'redirect' => false,
            'success' => $success,
        );

        if (!$success) {
            $exception = new PostValidationException('', 422);
            $arr_error = array();
            $arr_error['errors'][$request->payment_provider_code][0] = $response->response_reason_text;
            $exception->setErrors($arr_error);
            $exception->setTransaction($transaction);
            $exception->setForm($data);

            throw $exception;
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function preSave(TransactionRequest $request, $payment_provider)
    {
        //test Connection
        /*
        $response = Curl::to(self::submit_url_prod_deposit)
        ->withResponseHeaders()
        ->returnResponseObject()
        ->get();

        return $response->content();
        */

        return true;
    }

    /**
     * @inheritdoc
     */
    public function generateRequestData($bank_code)
    {
        return '200';
    }

    /**
     * @param $string
     * @return mixed
     */
    public function checkTransactionStatus($string)
    {
        $fields = array(
            'x_login' => '_login', // from PPconfig
            'x_trans_key' => 'pass', // from PPconfig
            'x_invoice_num' => 'dsa3jg438n', // from transaction db
            'x_response_format' => 'json', //constant
        );

        return $fields;
    }

    public function generateSignature()
    {
        return 'Signature: samplesignature_12342314e2fsd';
    }

    /**
     * @return mixed
     */
    public function generateFormDeposit()
    {
        $fields = array(
            'x_login' => "_login", // from PPconfig
            'x_trans_key' => "_pass", // from PPconfig
            'x_type' => 'AUTH_CAPTURE', // constant
            'x_card_num' => 'x_card_num', // from user
            'x_card_code' => 'x_card_code', // from user
            'x_exp_date' => 'x_exp_date', // from user
            'x_amount' => 'x_amount', // from user
            'x_currency' => 'x_currency', // from user
            'x_invoice_num' => 'x_invoice_num',
            'x_response_format' => 'json', // constant
        );

        return $fields;
    }

    /**
     * @return int
     */
    public function catchReturnURL()
    {
        return 200;
    }
}
