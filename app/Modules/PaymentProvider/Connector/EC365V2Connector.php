<?php

namespace App\Modules\PaymentProvider\Connector;

use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\URL;
use App\Http\Requests\API\TransactionRequest;
use App\Modules\PaymentProvider\BaseConnector;
use App\Modules\PaymentProvider\PaymentProviderInterface;
use App\Modules\PaymentProvider\Exceptions\ValidationException;
use App\Modules\PaymentProvider\Exceptions\UnknownPaymentProviderError;

class EC365V2Connector extends BaseConnector implements PaymentProviderInterface
{
    //const SUBMIT_URL_PROD_DEPOSIT = 'http://pay.ecshop365.net/api/pay/execute.do';
    //const SUBMIT_URL_PROD_DEPOSIT = 'http://payment.yingjikeji.com/api/pay/gateway.do';
    const SUBMIT_URL_PROD_DEPOSIT = 'http://payment.dieyingkj.com/api/pay/gateway.do';
    const POSTBACK_URL = '/provider/update';

    const API_VERSION = '0100';

    //http://pay.ecshop365.net/api/pay/execute.do
    //API for (M..) like M20,21...

    //payment gateway redirect address：
    //http://pay.ecshop365.net/api/pay/gateway.do(Use this address without special instructions)
    //http://payment.kj-mall.net/api/pay/gateway.do(Love farmer)
    //http://payment.718489.top/api/pay/gateway.do(Open Unicom 1)
    //http://hai.luaia.top/api/pay/gateway.do(Open Unicom 2)

    /**
     * @inheritdoc
     */
    public function generateRequestData($bank_code)
    {
        return '200';
    }

    public function checkStransactionStatus()
    {
        return '200';
    }

    public function generateSignature()
    {
        return 'Signature: samplesignature_12342314e2fsd';
    }

    /**
     * @return mixed
     */
    public function generateFormDeposit()
    {
        $fields = array();

        return $fields;
    }

    /**
     * @return int
     */
    public function catchReturnURL()
    {
        return 200;
    }

    /**
     * Check transaction status
     */
    public function checkTransactionStatus($string)
    {

    }

    /**
     * do something before save
     */
    public function preSave(TransactionRequest $request, $payment_provider)
    {
        return true;
    }

    /**
     * do something before update
     */

    public function postSave($transaction, $payment_provider, $request, $currency)
    {
        $exception = new UnknownPaymentProviderError('', 422);
        $arr_error = array();

        if ($payment_provider->bank_name == 'WeChat') {
            if ($request->amount < 1000 || $request->amount > 10000) { //based on docs
                $arr_error['errors']['amount'] = ['Invalid Amount'];
                $exception->setErrors($arr_error);
                $exception->setTransaction($transaction);

                throw $exception;
            }
        }

        if (empty($request->get('type'))) {
            $arr_error['errors']['err_msg']['type'] = 'Type field is required';
            $exception->setErrors($arr_error);
            throw $exception;
        }

        //this is for transaction type = deposit
        if ($request->get('type') == 'deposit') {
            $notify_url = URL::current() . self::POSTBACK_URL; //TODO: backend catch ReturnURL
            $return_url = "http://frontendOGPS/notifyUser"; // frontend

            $data_array = array(
                'amount' => $request->amount,
                //'bankid' => $payment_provider->bank_code,
                'bankid' => '666666666666',
                'cardtype' => '01', //debit card
                'currency' => $currency->currency_code == 'RMB' ? 'CNY' : $currency->currency_code,
                'ip' => $request->ip(),
                'merchOrderid' => $transaction->transaction_number,
                'merchno' => '',
                'notifyUrl' => $notify_url,
                'orderDesc' => 'Deposit', //any text
                'orderName' => $request->payor_name,
                'ordersn' => $transaction->transaction_number,
                'paytype' => '00', //00 - Online payment;  02 - wechat scan code
                'transtype' => '00', // payment
                'version' => self::API_VERSION,
            );

            $api_key = '';
            foreach ($payment_provider->configuration as $v) {
                if ($v['value'] != '' && $v['code'] != 'api_key') {
                    $data_array[$v['code']] = $v['value'];
                } else if ($v['code'] == 'api_key') {
                    $api_key = $v['value'];
                }
            }

            $signSrc = '';
            foreach ($data_array as $key => $value) {
                $signSrc .= '&' . $key . "=" . $value;
            }

            $signSrc .= $api_key;
            $signSrc = substr($signSrc, 1);
            $data_array['sign'] = md5($signSrc);

            //var_dump($data_array,$payment_provider);exit;

            // Get the current environment
            //$url = App::environment() == 'local' ? $url = self::SUBMIT_URL_DEV : $url = self::SUBMIT_URL_PROD;
            $url = self::SUBMIT_URL_PROD_DEPOSIT;

            $response = Curl::to($url)
                ->withHeaders([
                    'Content-Type' => 'application/json',
                ])
                ->withData(
                    $data_array
                )
                //->returnResponseObject(true)
                ->post();

//            var_dump($response);exit;

            if (!isset($response)) {
                \Log::info('---NOTIFICATION HOOK START----');
                \Log::info($url);
                \Log::info(json_encode($data_array));
                \Log::info('__EC365Connector__');
                \Log::info($response);
                \Log::info('---NOTIFICATION HOOK END----');

                $exception = new UnknownPaymentProviderError('', 422);
                $arr_error = array();
                $arr_error['errors']['payment_provider'] = ['Unknown error by payment provider'];
                $exception->setErrors($arr_error);
                $exception->setTransaction($transaction);

                throw $exception;
            }

//            $success = false;
//            if ($response->status == '200') { //transaction success
            $success = true;
            //}

            //TODO: return the redirection url, success/error message on transaction status
            $data = array(
                'response_data' => $response,
                'transaction_status' => 'PROCESSING',
                'post_data' => $data_array,
                'redirect' => true,
                'success' => $success,
            );

            return $data;
        }

    }
}
