<?php

namespace App\Modules\PaymentProvider;

use App\Modules\PaymentProvider\Connector\EC365V2Connector;
use App\Modules\PaymentProvider\Connector\RPNConnector;
use App\Modules\PaymentProvider\Connector\JBPConnector;
use App\Modules\PaymentProvider\Connector\JUHUIZFConnector;
use App\Modules\PaymentProvider\Connector\ASTROPAYConnector;
use App\Modules\PaymentProvider\Connector\BBPAYConnector;
use App\Modules\PaymentProvider\Exceptions\ConnectorNotFoundException;

class Factory
{
    /**
     * @var list of available providers
     */
    private static $connector = [
        'rpn' => RPNConnector::class,
        'astropay' => ASTROPAYConnector::class,
        'ec365v2' => EC365V2Connector::class,
        'jbp' => JBPConnector::class,
        'juhuizf' => JUHUIZFConnector::class,
        'bbpay' => BBPAYConnector::class,
    ];

    /**
     * Make provider connector
     *
     * @param  [type] $provider [description]
     * @return [type]           [description]
     */
    public static function make($provider)
    {
        if (!array_key_exists($provider, self::$connector)) {
            throw new ConnectorNotFoundException('Connector not found');
        }

        return new self::$connector[$provider];
    }
}
