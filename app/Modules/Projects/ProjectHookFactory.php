<?php

namespace App\Modules\Projects;

use App\Traits\TokenParserTrait;
use App\Modules\Projects\Hooks\DefaultHook;

class ProjectHookFactory
{
    use TokenParserTrait;

    /**
     * @var mixed
     */
    protected $class;

    /**
     * Make provider connector
     *
     * @param  [type] $provider [description]
     * @return [type]           [description]
     */
    public function __construct()
    {
        $this->setClient();

        if ($this->getClient()->user->project) {

            $class = 'App\\Modules\\Projects\\Hooks\\' . strtoupper($this->getClient()->user->project->name) . 'Hook';

            $this->setClass(new $class);
            return true;
        }

        $this->setClass(new DefaultHook);

        return true;
    }

    /**
     * @param $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }
}
