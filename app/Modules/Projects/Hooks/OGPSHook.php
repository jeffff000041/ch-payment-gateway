<?php

namespace App\Modules\Projects\Hooks;

use App\Modules\Projects\Hooks\ProjectHookContract;
use App\User;
use App\Jobs\EmailTransaction;
use App\Jobs\EmailMaxAmountReached;
use App\Jobs\EmailMaxPercentage;

class OGPSHook implements ProjectHookContract
{
    public function adminNotification($transaction_data)
    {
        $users = User::adminNotifiable()->get();

        foreach ($users as $key => $value) {
            $transaction_data_email = [
                'transaction' => $transaction_data,
                'user_data' => $value,
                'redirect_url' => ogps_url('payment-management/logs')
            ];
            EmailTransaction::dispatch($transaction_data_email);
        }
    }

    public function maxAmountNotification($transaction_data, $accumulated_amount)
    {
        $users = User::adminNotifiable()->get();

        foreach ($users as $key => $value) {
            $transaction_data_email = [
                'transaction' => $transaction_data,
                'accumulated_amount' => $accumulated_amount,
                'user_data' => $value
            ];
            EmailMaxAmountReached::dispatch($transaction_data_email);
        }
    }

    public function maxPercentageNotification($transaction_data, $accumulated_amount)
    {
        $users = User::adminNotifiable()->get();

        foreach ($users as $key => $value) {
            $transaction_data_email = [
                'transaction' => $transaction_data,
                'accumulated_amount' => $accumulated_amount,
                'user_data' => $value
            ];
            EmailMaxPercentage::dispatch($transaction_data_email);
        }
    }
}
