<?php

namespace App\Modules\Projects\Hooks;

use App\Modules\Projects\Hooks\ProjectHookContract;

class DefaultHook implements ProjectHookContract
{
    public function adminNotification($transaction_data)
    {
    	return 'default';
    }

    public function maxAmountNotification($transaction_data, $accumulated_amount)
    {
        return;
    }

    public function maxPercentageNotification($transaction_data, $accumulated_amount)
    {
        return;
    }
}
