<?php

namespace App\Modules\Projects\Hooks;

interface ProjectHookContract
{
    public function adminNotification($transaction_data);
    public function maxAmountNotification($transaction_data, $accumulated_amount);
    public function maxPercentageNotification($transaction_data, $accumulated_amount);
}
