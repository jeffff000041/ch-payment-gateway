<?php

namespace App;

use App\Traits\EncryptID;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use Notifiable, HasApiTokens, HasRoles, SoftDeletes, EncryptID, 
        \OwenIt\Auditing\Auditable;

    const ENABLED = '0';
    const DISABLED = '0';

    const SUPER_USER_ID = 1;

    const CAPTCHA_TYPE_CHECKBOX = 'checkbox';

    const CAPTCHA_TYPE_INVISIBLE = 'invisible';


    /**
     * @var string
     */
    protected $guard_name = 'web'; // or whatever guard you want to use

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'username',
        'is_active',
        'project_id',
        'is_notifiable',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $appends = [
        'full_name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAdmin($query)
    {
        if (auth()->user()) {
            if (!is_null(auth()->user()->project_id)) {
                $query->where('project_id', auth()->user()->project_id);
            }
        }

        return $query->role('admin');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSuperAdmin($query)
    {
        if (auth()->user()) {
            if (!is_null(auth()->user()->project_id)) {
                $query->where('project_id', auth()->user()->project_id);
            }
        }

        return $query->role('super_admin');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeProjectUser($query)
    {
        return $query->whereNotNull('project_id')
            ->get();
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * @return mixed
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAdminNotifiable($query)
    {
        return $query->where('is_notifiable', 1)->role(['admin', 'super_admin']);
    }
}
