<?php

namespace App;

use Laravel\Passport\Client as PassportClientModel;
use OwenIt\Auditing\Contracts\Auditable;

class Client extends PassportClientModel implements Auditable
{
    use \OwenIt\Auditing\Auditable;

	const PERSONAL_ACCESS_CLIENT = 1;
	const PASSWORD_GRANT_CLIENT = 2;

	const IS_DEFAULT = 1;

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
