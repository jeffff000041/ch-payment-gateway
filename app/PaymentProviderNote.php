<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PaymentProviderNote extends Model implements Auditable
{
    use  \OwenIt\Auditing\Auditable;
    /**
     *
     * @var [type]
     */
    protected $fillable = [
        'payment_provider_id',
        'payment_provider_code',
        'notes',
        'user_id',
    ];
}
