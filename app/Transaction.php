<?php

namespace App;

use App\Traits\EncryptID;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Transaction extends Model
{
    use EncryptID;

    /**
     * type
     */
    const WITHDRAW = 'WITHDRAW';
    const DEPOSIT = 'DEPOSIT';

    /**
     * type
     */
    const FOR_REVIEW = 'FOR REVIEW';
    const CONFIRMED = 'CONFIRMED';

    /**
     * transaction payment setting error message
     */
    const PAYMENT_SETTING_CONTACT_SUPPORT_MSG = 'Your transaction could not be processed. Please contact the support team.';
    const PAYMENT_SETTING_NOT_AVAILABLE_MSG = 'Payment Settings not available.';

    /**
     * CSV columns
     */
    const TRANSACTION_EXPORT_COLUMNS = [
        'Date and Time',
        'Transaction Number',
        'Payor Name',
        'Payment Provider',
        'Payment Method',
        'Status',
        'Notes',
        'Currency',
        'Amount',
        // 'BO Notes',
    ];

    /**
     * type
     */
    const PDF = 'Pdf';
    const XLS = 'XLS';
    const XLSX = 'XLSX';
    const CSV = 'Csv';

    /**
     * type
     */
    const FILE_TYPES = [
        'pdf',
        'xls',
        'xlsx',
        'csv',
    ]; 

    /**
     * type
     */
    const FILE_PREFIX = 'TransactionReport-';

    /**
     * type
     */
    const AUTO_DISABLED_PERCENTAGE = 98;

    /**
     * @var array
     */
    protected $fillable = [
        'payment_setting_id',
        'user_id',
        'amount',
        'transaction_number',
        'reference_number',
        'type',
        'currency_id',
        'notes',
        'approval_status',
        'is_cancelled',
        'back_office_notes',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'payment_setting_id',
        'user_id',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'hash_payment_setting_id',
    ];

    /*
    |--------------------------------------------------------
    | RELATIONSHIPS
    |--------------------------------------------------------
     */

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    /**
     * @return mixed
     */
    public function payment_setting()
    {
        return $this->belongsTo(PaymentSetting::class)->withTrashed();
    }

    /**
     * @return mixed
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @return mixed
     */
    public function latest_transaction_trail()
    {
        return $this->hasOne(TransactionTrail::class)->latest();
    }
    /*
    |--------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------
     */

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithLatestTrail($query)
    {
        return $query->select('transactions.*', 'transaction_trails.status')
            ->join('transaction_trails', 'transactions.id', '=', 'transaction_trails.transaction_id')
            ->with('latest_transaction_trail')
            ->whereRaw('transaction_trails.created_at = (select max(created_at) FROM transaction_trails tt2 where transaction_trails.transaction_id = tt2.transaction_id)');
    }

    /*
    |--------------------------------------------------------
    | APPENDED FIELDS
    |--------------------------------------------------------
     */

    /**
     * @param $value
     */
    public function getHashPaymentSettingIdAttribute()
    {
        return walle_encrypt($this->payment_setting_id);
    }

    /**
     * @param $value
     */
    public function setHashPaymentSettingIdAttribute()
    {
        return walle_decrypt($this->payment_setting_id);
    }
}
