<?php

namespace App;

use App\PaymentProvider;
use App\Traits\EncryptID;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Currency extends Model implements Auditable
{
    use SoftDeletes, EncryptID, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'currency_name',
        'currency_code',
    ];

    //
    public function payment_providers()
    {
    	return $this->belongsToMany(PaymentProvider::class);
    }
}
