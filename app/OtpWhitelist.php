<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtpWhitelist extends Model
{
    use SoftDeletes;
    
    /**
     * 
     * @var [type]
     */
    protected $fillable = [
        'user_id', 
        'domain_name',
    ];
}
