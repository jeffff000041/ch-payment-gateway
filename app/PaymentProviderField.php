<?php

namespace App;

use App\Traits\EncryptID;
use App\PaymentProvider;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PaymentProviderField extends Model implements Auditable
{
    use EncryptID, \OwenIt\Auditing\Auditable;
    /**
     * @var array
     */
    protected $fillable = [
        'payment_providers_field_code',
        'payment_provider_id',
        'label',
        'is_visible',
        'data_type',
    ];
    
    /**
     * @return mixed
     */
    public function payment_provider()
    {
        return $this->belongsTo(PaymentProvider::class);
    }

    /**
     * @return mixed
     */
    public function payment_setting_config()
    {
        return $this->hasOne(PaymentSettingConfig::class);
    }

    /**
     * @return mixed
     */
    public function field_category_payment_provider_field()
    {
        return $this->hasOne(FieldCategoryPaymentProviderField::class);
    }
}
