<?php

namespace App\Repositories\Services;

use App\PaymentProvider;
use App\Repositories\Contracts\PaymentProviderContract;
use DB;
use App\Bank;
use App\User;
use stdClass;
use App\Client;
use Carbon\Carbon;
use App\PaymentSetting;
use Illuminate\Http\Request;
use App\Traits\TokenParserTrait;
use App\Repositories\BaseRepositoryAbstract;
use App\Repositories\Contracts\TransactionContract;
use App\Repositories\Contracts\PaymentSettingContract;

class PaymentSettingService extends BaseRepositoryAbstract implements PaymentSettingContract
{
    use TokenParserTrait;

    /**
     * @param PaymentSetting $projectPaymentSetting
     */
    public function __construct(PaymentSetting $projectPaymentSetting)
    {
        $this->setModel($projectPaymentSetting);
    }

    /**
     * Get available unque bank filtered by project ID and currency
     *
     * @param int $currency_id
     *
     * @return mixed
     */
    public function getAvailableUniqueBanksPerClient(int $currency_id, int $client_id)
    {
        $payment_settings = $this->getModel()
            // ->with('payment_provider.currencies')
            ->select('payment_settings.id', 'banks.bank_name', 'payment_settings.payment_provider_id')
            ->with('payment_provider')
            ->join('banks', 'payment_settings.bank_id', 'banks.id')
            ->whereIsActive(PaymentSetting::ACTIVE)
            ->whereHas('payment_provider.currencies', function ($query) use ($currency_id) {
                $query->where('currency_id', $currency_id);
            })
            ->groupBy('bank_name');

        $payment_settings = $this->filterPerProject($payment_settings);

        return $payment_settings->get();
    }

    /**
     * Get available unque bank filtered by project ID and currency
     *
     * @param int $currency_id
     *
     * @return mixed
     */
    public function getAllUniqueBanks($client_id = null)
    {
        $payment_settings = $this->getModel()
            // ->with('payment_provider.currencies')
            ->select('payment_settings.id', 'banks.bank_name', 'payment_settings.payment_provider_id')
            ->with('payment_provider')
            ->join('banks', 'payment_settings.bank_id', 'banks.id')
            ->join('payment_providers', 'banks.payment_provider_id', 'payment_providers.id')
            ->where('payment_providers.is_active', '!=', 1)
            ->groupBy('bank_name');

        if ($client_id) {
            $payment_settings->where('client_id', $client_id);
        }

        //add super admin / admin project
        $payment_settings = $this->filterPerProject($payment_settings);

        return $payment_settings->get();
    }

    /**
     * Get available unique bank filtered by project ID
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function findPaymentSettings(int $id, $client_id = null)
    {
        $payment_setting = $this->getModel()->with(
            [
                'payment_provider',
                'bank',
            ]
        );

        if ($client_id) {
            $payment_setting->where('client_id', $client_id);
        }

        return $payment_setting->findOrFail($id);
    }

    /**
     * Get priority payment provider settings
     *
     * @param PaymentSetting $payment_setting
     * @return mixed
     */
    // backup
    // public function getPriorityPaymentSetting(PaymentSetting $payment_setting, int $client_id)
    // {
    //     $getPriorityPaymentSetting = $this->getPriorityPaymentSettingQuery($payment_setting, $client_id);
    //     return $getPriorityPaymentSetting->first();
    // }

    public function getPriorityPaymentSetting(PaymentSetting $payment_setting, TransactionContract $transactionService, $amount)
    {
        $availablePaymentSettings = $this->getPriorityPaymentSettingQuery($payment_setting)->get();

        foreach ($availablePaymentSettings as $key => $availablePaymentSetting) {

            $totalTransaction = $transactionService->getTotalTransactions($availablePaymentSetting->id);

            if (((float) $amount + $totalTransaction) <= $availablePaymentSetting->maximum_amount) {
                $priority_payment_provider = $this
                    ->getPriorityPaymentSettingQueryFormat($availablePaymentSetting)
                    ->where('payment_settings.id', $availablePaymentSetting->id)
                    ->first();

                return $priority_payment_provider;
            }
        }
    }

    /**
     * Get priority payment provider settings
     *
     * @param PaymentSetting $payment_setting
     * @return mixed
     */
    public function getPriorityPaymentSettingQuery(PaymentSetting $payment_setting)
    {
        $payment_settings = $this->getPriorityPaymentSettingQueryFormat($payment_setting)
            ->orderBy('priority');

        $payment_settings = $this->filterPerProject($payment_settings);

        return $payment_settings;
    }

    /**
     * Get priority payment provider settings
     *
     * @param PaymentSetting $payment_setting
     * @return mixed
     */
    public function getPriorityPaymentSettingQueryFormat(PaymentSetting $payment_setting)
    {
        return $this->getModel()
            ->select('payment_settings.*', 'banks.bank_name')
            ->join('banks', 'payment_settings.bank_id', 'banks.id')
            ->whereIsActive(PaymentSetting::ACTIVE)
            ->where('bank_name', $payment_setting->bank->bank_name);
    }

    /**
     * Get Payment provider settings
     *
     * @param int $id
     *
     * @return array
     */
    public function getPaymentProviderSettings($id, $category_id = null, $client_id = null)
    {
        $payment_settings = PaymentSetting::with(
            [
                'payment_provider.field_category_unique_payment_provider_fields' => function ($query) use ($category_id) {
                    if ($category_id) {
                        $query->where('field_category_id', $category_id);
                    }
                },
                'payment_provider.field_category_unique_payment_provider_fields.payment_provider_field.payment_setting_config' => function ($query) use ($id) {
                    $query->where('payment_setting_id', $id);
                },
            ]
        );

        if ($client_id != null) {
            $payment_settings->where('client_id', $client_id);
        }

        $payment_settings = $payment_settings->findOrFail($id);

        $configurations = [];
        $payment_provider_fields = $payment_settings->payment_provider->field_category_unique_payment_provider_fields;

        //format configuration
        foreach ($payment_provider_fields as $key => $payment_provider_field) {
            array_push($configurations, [
                'hash_id' => $payment_provider_field->hash_payment_provider_field_id,
                'code' => $payment_provider_field->payment_provider_field->payment_providers_field_code,
                'hash_category_id' => $payment_provider_field->hash_category_id,
                'category_id' => $payment_provider_field->field_category_id,
                'label' => $payment_provider_field->payment_provider_field->label,
                'data_type' => $payment_provider_field->payment_provider_field->data_type,
                'is_visible' => $payment_provider_field->payment_provider_field->is_visible,
                'value' => isset($payment_provider_field->payment_provider_field->payment_setting_config->value) ? $payment_provider_field->payment_provider_field->payment_setting_config->value : '',
            ]);
        }

        $payment_provider_settings = new stdClass;

        $payment_provider_settings->hash_id = $payment_settings->hash_id;
        $payment_provider_settings->is_active = $payment_settings->is_active;
        $payment_provider_settings->priority = $payment_settings->priority;
        $payment_provider_settings->minimum_amount = $payment_settings->minimum_amount;
        $payment_provider_settings->maximum_amount = $payment_settings->maximum_amount;
        $payment_provider_settings->payment_provider_name = $payment_settings->payment_provider->payment_provider_name;
        $payment_provider_settings->bank_name = $payment_settings->bank->bank_name;
        $payment_provider_settings->bank_code = $payment_settings->bank->bank_code;
        $payment_provider_settings->code = $payment_settings->payment_provider->payment_provider_code;
        $payment_provider_settings->currency = $payment_settings->currency;
        $payment_provider_settings->configuration = $configurations;

        return $payment_provider_settings;
    }

    /**
     * @return mixed
     */
    public function getAllWithAccumulatedTransactionAmount()
    {
        // $payment_settings = $this->getModel()->select([
        //     '*', DB::raw('COALESCE( override_accumulated_amount, COALESCE(SUM(transactions.amount), 0)) as accoumulated_transaction_amount'),
        //     DB::raw('payment_settings.maximum_amount - COALESCE( override_accumulated_amount, COALESCE(SUM(transactions.amount), 0)) as remaining_balance ')
        // ])
        //     ->join('transactions', function ($join) {
        //         $join->select(['payment_setting_id'])
        //             ->where([
        //                 ['transactions.created_at', '>', 'payment_settings.start_enabled_date'],
        //                 ['transactions.created_at', '<=', Carbon::now()],
        //             ])
        //             ->on('transactions.payment_setting_id', '=', 'payment_settings.id');
        //             // ->on('transactions.created_at', '>', 'payment_settings.start_enabled_date');
        //     })
        //     ->join('transaction_trails', function ($join) {
        //         $join->select(['transaction_id', DB::raw('max(created_at) as created_at'), 'status'])
        //             ->whereIn('transaction_trails.status', [PaymentSetting::STATUS_CONFIRMED, PaymentSetting::STATUS_PROCESSING])
        //             ->on('transaction_trails.transaction_id', '=', 'transactions.id');
        //     })
        //     ->with(
        //         [
        //             'payment_provider',
        //             'bank',
        //             'client',
        //             'client.user',
        //             'client.user.project',
        //         ]
        //     );
        $payment_settings = $this->getModel()->select(
            '*', DB::raw('
            COALESCE
            (
            override_accumulated_amount
            , COALESCE
                (
                    (
                        SELECT SUM(transactions.amount)
                        FROM
                        transactions
                        JOIN
                        transaction_trails
                        ON
                        transactions.id = transaction_trails.transaction_id
                        WHERE
                        payment_setting_id = payment_settings.id
                        AND
                        (
                            transactions.created_at > payment_settings.start_enabled_date
                            AND
                            transactions.created_at <= "' . Carbon::now() . '"
                        )
                        AND
                        transaction_trails.created_at = (select max(created_at) FROM transaction_trails tt2 where transaction_trails.transaction_id = tt2.transaction_id)
                        AND
                        (
                            transaction_trails.status = "' . PaymentSetting::STATUS_CONFIRMED . '"
                            -- OR
                            -- transaction_trails.status = "' . PaymentSetting::STATUS_PROCESSING . '"

                        )
                    )
                , 0)
            )
            as accoumulated_transaction_amount'),
            DB::raw('payment_settings.maximum_amount - COALESCE( override_accumulated_amount, 
                COALESCE
                (
                    (
                        SELECT SUM(transactions.amount)
                        FROM
                        transactions
                        JOIN
                        transaction_trails
                        ON
                        transactions.id = transaction_trails.transaction_id
                        WHERE
                        payment_setting_id = payment_settings.id
                        AND
                        (
                            transactions.created_at > payment_settings.start_enabled_date
                            AND
                            transactions.created_at <= "' . Carbon::now() . '"
                        )
                        AND
                        transaction_trails.created_at = (select max(created_at) FROM transaction_trails tt2 where transaction_trails.transaction_id = tt2.transaction_id)
                        AND
                        (
                            transaction_trails.status = "' . PaymentSetting::STATUS_CONFIRMED . '"
                            -- OR
                            -- transaction_trails.status = "' . PaymentSetting::STATUS_PROCESSING . '"

                        )
                    )
                , 0)) as remaining_balance ')
        )
        ->with(
            [
                'payment_provider',
                'bank',
                'client',
                'client.user',
                'client.user.project',
            ]
        );
        //add super admin / admin project
        $payment_settings = $this->filterPerProject($payment_settings);

        return $payment_settings;
    }

    /**
     * @param $projectPaymentSettingRequest
     * @param $client_id
     * @param $user_id
     * @return mixed
     */
    public function create($projectPaymentSettingRequest, $client_id, $user_id)
    {

        #already decrypted in App\Http\Middleware\PaymentSettingsMiddleware
        $payment_provider_id = $projectPaymentSettingRequest->payment_provider_id;
        $payment_provider_id = walle_decrypt($payment_provider_id);
        #already decrypted in App\Http\Middleware\PaymentSettingsMiddleware
        $bank_id = $this->getBankId($projectPaymentSettingRequest->bank_id, $payment_provider_id);

        $validate_setting = PaymentSetting::where("payment_provider_id", $payment_provider_id)
                                            ->where("bank_id", walle_decrypt($projectPaymentSettingRequest->bank_id));
                                            // ->where("is_active", '1')
                                            // ->whereNull("deleted_at");
        // dd($validate_setting->count());
        if ($validate_setting->count() == 0) {
            $payment_setting = new PaymentSetting([
                'payment_provider_id' => $payment_provider_id,
                'bank_id' => walle_decrypt($projectPaymentSettingRequest->bank_id),
                'client_id' => $client_id,
                'is_active' => $projectPaymentSettingRequest->get('is_active'),
                'maximum_amount' => $projectPaymentSettingRequest->get('maximum_amount'),
                'created_by' => $user_id,
                'updated_by' => $user_id,
                // 'priority' => (PaymentSetting::where('client_id', '=', $client_id)->where('payment_provider_id', $payment_provider_id)->count() + 1),
                'priority' => (PaymentSetting::where('client_id', '=', $client_id)->whereNull("deleted_at")->where("is_active", '1')->where('bank_id', '=', walle_decrypt($projectPaymentSettingRequest->bank_id))->count() + 1),
                'start_enabled_date' => Carbon::now(),
                'percentage_to_notify' => $projectPaymentSettingRequest->get('percentage_to_notify'),
                'type' => $projectPaymentSettingRequest->get('type') ? $projectPaymentSettingRequest->get('type') : PaymentSetting::TYPE_DEPOSIT,
            ]);
            // dd($payment_setting);
            $payment_setting->save();

            $this->saveConfiguration($payment_setting, $projectPaymentSettingRequest, $user_id);
            return [true, 'rsp'=>$payment_setting];
    
        } else {
            return [false];
        }

    }

    /**
     * @param $projectPaymentSettingRequest
     * @param $payment_setting
     * @param $user_id
     * @return mixed
     */
    public function update($projectPaymentSettingRequest, $payment_setting, $user_id)
    {
        return DB::transaction(function () use (
            $projectPaymentSettingRequest,
            $payment_setting,
            $user_id
        ) {

            //reset if enabled
            if ($payment_setting->auto_disabled == 1 && $payment_setting->is_active = 1) {
                $payment_setting->start_enabled_date = Carbon::now();
                $payment_setting->auto_disabled = PaymentSetting::NOT_AUTO_DISABLED;
            }
            $payment_setting->is_active = $projectPaymentSettingRequest->get('is_active');
            $payment_setting->maximum_amount = $projectPaymentSettingRequest->get('maximum_amount');
            $payment_setting->percentage_to_notify = $projectPaymentSettingRequest->get('percentage_to_notify');
            $payment_setting->override_accumulated_amount = is_null($projectPaymentSettingRequest->get('override_accumulated_amount'))
                ? 0 : $projectPaymentSettingRequest->get('override_accumulated_amount');
            $payment_setting->updated_by = $user_id;
            $payment_setting->save();

            $this->saveConfiguration($payment_setting, $projectPaymentSettingRequest, $user_id);

            return $payment_setting;
        });
    }

    /**
     * @param $payment_setting
     * @param $configurations
     *
     * @return void
     */
    private function saveConfiguration($payment_setting, $projectPaymentSettingRequest, $user_id)
    {
        $configurations = json_decode($projectPaymentSettingRequest->get('configurations'));

        //save provider configs
        if ($configurations) {
            foreach ($configurations as $key => $configuration) {
                //detach old value
                $payment_setting->payment_setting_configs()
                    ->detach(walle_decrypt($configuration->id));

                //attach new value
                $payment_setting->payment_setting_configs()
                    ->attach(walle_decrypt($configuration->id), [
                        'created_by' => $user_id,
                        'updated_by' => $user_id,
                        'value' => $configuration->value,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);
            }
        }
    }

    /**
     * This is to filter the same user of the client's payment settings.
     * This method is "workaround" because of the constant change of the requirements.
     * @todo  Need to refactor!
     *
     * @param $payment_settings
     * @return mixed
     */
    public function filterPerProject($payment_settings)
    {
        $payment_settings->whereHas('payment_provider', function ($query){
            $query->where('is_active', '=' , '1');
        });
        //for admin / super user
        if (auth()->user()) {
            //for user that have project
            if (!is_null(auth()->user()->project_id)) {
                $project_id = auth()->user()->project->id;
            } else {
                return $payment_settings;
            }

        } else {
            // for client
            $this->setClient();

            $user_id = $this->getClient()->user_id;
            $user = User::findOrFail($user_id);

            //fetch per client payment settings client belongs to super user
            if ($user_id == User::SUPER_USER_ID) {

                return $payment_settings->whereHas('client', function ($query) {
                    $query->where('id', $this->getClient()->id);
                });
            }

            $project_id = $user->project->id;
        }

        $payment_settings->whereHas('client.user.project', function ($query) use ($project_id) {
            $query->where('id', $project_id);
        });

        return $payment_settings;
    }

    private function getBankId($request_bank_id, $payment_provider_id)
    {

        $request_bank_id = walle_decrypt($request_bank_id);
        $bank_from_bank_id = Bank::findOrFail($request_bank_id);
        $pp_bank = Bank::whereBankName($bank_from_bank_id->bank_name)
            //remove this for prioritization
            ->wherePaymentProviderId($payment_provider_id)
            ->firstOrFail();

        return $pp_bank->id;
    }

    public function sortPriority($payment_settings, $user_id)
    {
        $user = User::superAdmin()->firstOrFail();
        $clients = Client::where('user_id', $user->id)->pluck('id')->toArray();

        DB::transaction(function () use ($payment_settings, $clients) {
            foreach ($payment_settings as $key => $payment_setting) {
                PaymentSetting::where([
                    'id' => walle_decrypt($payment_setting),
                ])
                    ->whereIn('client_id', $clients)
                    ->update(['priority' => ($key + 1)]);
            }
        });
    }
}
