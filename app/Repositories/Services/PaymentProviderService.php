<?php

namespace App\Repositories\Services;

use stdClass;
use App\PaymentProvider;
use App\PaymentProviderNote;
use App\Repositories\BaseRepositoryAbstract;
use App\Repositories\Contracts\PaymentProviderContract;

class PaymentProviderService extends BaseRepositoryAbstract implements PaymentProviderContract
{
    /**
     * @param PaymentProvider $projectPaymentProvider
     */
    public function __construct(PaymentProvider $projectPaymentProvider)
    {
        $this->setModel($projectPaymentProvider);
    }

        /**
     * Get Payment provider settings
     *
     * @param int $id
     *
     * @return array
     */
    public function getPaymentProviderSettings($id, $category_id = null)
    {
        $payment_provider = PaymentProvider::with(
                [
                    'field_category_unique_payment_provider_fields' => function ($query) use ($category_id) {
                        if($category_id){
                            $query->where('field_category_id', $category_id);
                        }
                    }
                ]
            )->findOrFail($id);

        $configurations = [];
        $payment_provider_fields = $payment_provider->field_category_unique_payment_provider_fields;

        //format configuration
        foreach ($payment_provider_fields as $key => $payment_provider_field) {
        array_push($configurations, [
                'hash_id' => $payment_provider_field->hash_payment_provider_field_id,
                'code' => $payment_provider_field->payment_provider_field->payment_providers_field_code,
                'hash_category_id' => $payment_provider_field->hash_category_id,
                'category_id' => $payment_provider_field->field_category_id,
                'label' => $payment_provider_field->payment_provider_field->label,
                'data_type' => $payment_provider_field->payment_provider_field->data_type,
                'is_visible' => $payment_provider_field->payment_provider_field->is_visible,
                'value' => '',
            ]);
        }

        $payment_provider_config = new stdClass;

        $payment_provider_config->hash_id = $payment_provider->hash_id;
        $payment_provider_config->code = $payment_provider->payment_provider_code;
        $payment_provider_config->configuration = $configurations;

        return $payment_provider_config;
    }

    public function getPaymentProviders()
    {
        return PaymentProvider::query();//::all();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function getActivePaymentProviders($query)
    {
        return PaymentProvider::WhereStatusActive();
    }

    public function createPaymentProviderBONotes($request)
    {
        $payment_provider_note = new PaymentProviderNote([
            'payment_provider_id' => walle_decrypt($request->hash_id),
            'payment_provider_code' => $request->payment_provider_code,
            'notes' => $request->note,
            'user_id' => auth()->id(),
        ]);

        $payment_provider_note->save();
    }

    public function disablePaymentProvider($request)
    {
        $payment_provider_id = walle_decrypt($request->hash_id);

        $payment_provider = PaymentProvider::findOrFail($payment_provider_id);

        $payment_provider->is_active = (string) $request->switch_val;

        $payment_provider->save();

    }
}
