<?php

namespace App\Repositories\Services;

use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;

class MpdfService extends Mpdf
{
    /**
     * Gets the implementation of external PDF library that should be used.
     *
     * @param array $config Configuration array
     *
     * @return \Mpdf\Mpdf implementation
     */
    protected function createExternalWriterInstance($config)
    {   
        return new \Mpdf\Mpdf(['tempDir' => config('ogps.mpdf_tmp_path')]);
    }
}