<?php

namespace App\Repositories\Services;

use App\User;
use App\Audit;
use App\Transaction;
use App\Repositories\BaseRepositoryAbstract;
use App\Repositories\Contracts\AuditContract;
use App\DataTables\AuditDataTable;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use App\Repositories\Services\MpdfService;


class AuditService extends BaseRepositoryAbstract implements AuditContract
{
    public $prefix = 'AuditReport-';

    /**
     * @param Audit $audit
     */
    public function __construct(Audit $audit)
    {
        $this->setModel($audit);
    }

    /**
     * @param $request
     */
    public function getAllAuditLog($request)
    {
        $audit = $this->getModel()->with('user');

        return $audit;
    }

    public function create($request, $event)
    {
        $user = User::
            where('users.id', auth()->id())
            ->get()->first();

        $audit_log = new Audit([
            'user_id' => auth()->id(),
            'email' => $user->email,
            'role' => '',
            'event' => $event,
            'old_values' => '',
            'new_values' => json_encode($request->all()),
            'ip_address' => $request->ip(),
            'platform' => $request->route('module_name'),
            'remarks' => '',
        ]);

        $audit_log->save();
    }

        /**
     * @return mixed
     */
    public function exportAuditLogs($request)
    {
        if (!in_array($request->type, Transaction::FILE_TYPES)) {
            return response([
                'error' => 'Invalid file type.'
            ], 422);
        }

        $auditDataTable = new AuditDataTable($this, $request);
        $audit = $auditDataTable->render();

        $array_center = [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'textRotation' => 0, 'wrapText' => true,
        ];

        $array_border = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $array_font = [
            'fill' => array(
                'fillType' => Fill::FILL_SOLID,
                'color' => array('rgb' => 'E5E4E2' )
            ),
            'font'  => [
                'bold'  =>  true,
            ],
        ];

        $columns = [
            "Date and Time",
            "User",
            "Event Type",
            "Item Affected"
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Audit Logs');
        // $range = ' ( '. $request->date_from . ' - ' . $request->date_to . ' )';
        // $title = empty($request->date_from) ? 'Transaction Logs' : 'Transaction Logs' . $range;
        // $sheet->setCellValue('A1', $title);
        // $sheet->getStyle("A1")->getFont()->setSize(16);
        // $sheet->getStyle('A1:J3')->applyFromArray(array_merge($array_border, $array_font));

        $sheet->fromArray($columns, null, 'A1');
        $sheet->getStyle('A1:D1')->applyFromArray(['font' => ['bold' => true]]);
        $sheet->getStyle('A1:D1')->getAlignment()->applyFromArray($array_center);

        $count = 2;
        $row = count($audit) + 1;

        foreach ($audit as $transaction) {


            $sheet->setCellValue('A'.$count, $transaction->created_at);
            $sheet->setCellValue('B'.$count, $transaction->user->username);
            $sheet->setCellValue('C'.$count, $transaction->event);
            $sheet->setCellValue('D'.$count, $transaction->auditable_type);

            // $sheet->setCellValue('J'.$count, $this->backOfficeNotesThread($transaction->hash_id));

            $sheet->getStyle('A'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('B'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('C'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('D'.$count)->getAlignment()->applyFromArray($array_center);
            // $sheet->getStyle('J'.$count)->getAlignment()->applyFromArray($array_center);
            $count++;
        }

        foreach (range('A', 'D') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
            $sheet->getStyle('A:D')->getAlignment()->setHorizontal('center');
        }

        $sheet->getStyle('A1:D' . $row)->applyFromArray($array_border);

        // $sheet->mergeCells('A1:J3');
        // $sheet->getStyle('A1:J3')->getAlignment()->applyFromArray($array_center);

        if ($request->type == strtolower(Transaction::PDF)) {
            IOFactory::registerWriter(Transaction::PDF, MpdfService::class);
        }

        $writer = IOFactory::createWriter($spreadsheet, ucfirst($request->type));

        $now = gmdate('d-m-Y');

        $path = storage_path('app/public') . '/files/' . $this->prefix . $now;

        \File::makeDirectory($path, $mode = 0777, true, true);

        $folder_path = storage_path() . '/app/public/files/' . $this->prefix . $now . '/';

        $res = $writer->save($folder_path . $this->prefix . $now . '.' . $request->type);

        /**
         * @todo  refactor to getSchemeAndHttpHost()
         *        after auto redirect to https in server is done
         */
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        // dd($protocol);
        $download_path = $protocol.'://' . $request->getHttpHost() . '/storage/files/' .
        $this->prefix . $now . '/' . $this->prefix . $now . '.' . $request->type;

        return $download_path;
    }
}
