<?php

namespace App\Repositories\Services;

use App\Project;
use App\Repositories\BaseRepositoryAbstract;
use App\Repositories\Contracts\ProjectContract;

class ProjectService extends BaseRepositoryAbstract implements ProjectContract
{
    /**
     * @param Project $Project
     */
    public function __construct(Project $projectProvider)
    {
        $this->setModel($projectProvider);
    }
}
