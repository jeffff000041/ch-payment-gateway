<?php

namespace App\Repositories\Services;

use App\Transaction;
use App\TransactionTrail;
use App\Repositories\BaseRepositoryAbstract;
use App\Repositories\Contracts\TransactionTrailContract;

class TransactionTrailService extends BaseRepositoryAbstract implements TransactionTrailContract
{
    /**
     * @param TransactionTrail $transaction
     */
    public function __construct(TransactionTrail $transaction)
    {
        $this->setModel($transaction);
    }

    /**
     * @todo Refactor!
     *
     * @param $transaction
     * @param $form
     * @param $status
     * @param $request
     */
    public function storeTransactionTrail($transaction, $execution_type, $status, $data_json)
    {
        //save transaction trail
        $transaction_trail = new TransactionTrail([
            'transaction_id' => $transaction->id,
            'amount' => $transaction->amount,
            'status' => $status,
            'execution_type' => $execution_type,
            'data_json' => $data_json,
        ]);

        $transaction_trail->save();
    }
}
