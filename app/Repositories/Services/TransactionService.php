<?php

namespace App\Repositories\Services;

use DB;
use Carbon\Carbon;
use App\Transaction;
use App\BackOfficeNote;
use App\TransactionTrail;
use App\PaymentSetting;
use Illuminate\Http\Request;
use App\Repositories\BaseRepositoryAbstract;
use App\Repositories\Contracts\TransactionContract;
use App\DataTables\TransactionDataTable;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use App\Repositories\Services\MpdfService;

class TransactionService extends BaseRepositoryAbstract implements TransactionContract
{
    /**
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->setModel($transaction);
    }

    /**
     * @param $request
     */
    public function getAllTransaction($request)
    {
        $transaction = $this->getModel()
            ->withLatestTrail()
            ->with([
                'payment_setting.payment_provider',
                'payment_setting.bank',
                'currency',
                'client',
            ]);

        if (auth()->user()) {
            if (!is_null(auth()->user()->project_id)) {
                $transaction->whereHas('client.user', function ($query) {
                    $query->where('project_id', auth()->user()->project_id);
                });
            }
        }

        return $transaction;
    }

    /**
     * @todo Ask transaction number format
     *
     * Generate transaction number
     *
     * @param $id
     */
    public function generateTransactionNumber($id)
    {
        return generateCode('WALLE-', $id);
    }

    /**
     * @param $payment_setting
     * @param $request
     * @return mixed
     */
    public function storeTransaction($payment_setting_id, $request, $client)
    {
        return DB::transaction(function () use ($payment_setting_id, $request, $client) {
            $transaction = $this->getModel();

            $transaction->payment_setting_id = $payment_setting_id;
            $transaction->amount = $request->get('amount');
            $transaction->client_id = $client->id;
            $transaction->currency_id = walle_decrypt($request->get('currency'));
            $transaction->type = strtoupper($request->get('type'));
            $transaction->reference_number = $request->get('reference_number');
            $transaction->notes = $request->get('notes');
            $transaction->approval_status = $request->get('approval_status') ?: 'FOR REVIEW';
            $transaction->payor_name = $request->get('payor_name');
            $transaction->save();

            $transaction->transaction_number = $this->
            generateTransactionNumber($transaction->id);

            $transaction->save();

            return $transaction;
        });
    }

    /**
     * @return mixed
     */
    public function getTotalTransactions($payment_settings_id)
    {
        //get valid payment provider settings
        $payment_provider_settings = PaymentSetting::findOrFail($payment_settings_id);

        if (!is_null($payment_provider_settings->override_accumulated_amount)) {
            return $payment_provider_settings->override_accumulated_amount;
        } else {
            return $this->getModel()
            ->withLatestTrail()
            ->where('payment_setting_id', $payment_settings_id)
            ->where(function ($query) use ($payment_provider_settings) {
                $query->where('transactions.created_at', '>', $payment_provider_settings->start_enabled_date)
                    ->where('transactions.created_at', '<=', Carbon::now());
            })
            ->where(function ($query) {
                $query->where('transaction_trails.status', PaymentSetting::STATUS_CONFIRMED);
                // ->orWhere('transaction_trails.status', PaymentSetting::STATUS_PROCESSING);
            })
            ->sum('amount');
        }
    }

    /**
     * Expire transactions greater that 1 day
     *
     * @return int Number of transaction that are expired
     */
    public function expireProcessingTransaction()
    {
        $transactions = $this->getModel()
            ->withLatestTrail()
            ->where('status', TransactionTrail::PROCESSING)
            ->whereRaw('transaction_trails.created_at < (NOW() - INTERVAL 1 DAY)')
            ->get();

        $data = [];

        foreach ($transactions as $key => $transaction) {
            array_push($data, [
                'transaction_id' => walle_decrypt($transaction->hash_id),
                'status' => TransactionTrail::FAILED,
                'data_json' => json_encode(array(
                    'payment_provider_response' => array(
                        'response_reason_text' => TransactionTrail::TRANSACTION_TEXT .' '. strtolower(TransactionTrail::EXPIRED)
                    ),
                )),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        TransactionTrail::insert($data);
    }

    /**
     * @return mixed
     */
    public function exportTransactionLogs($request)
    {
        if (!in_array($request->type, Transaction::FILE_TYPES)) {
            return response([
                'error' => 'Invalid file type.'
            ], 422);
        }

        $transactionDataTable = new TransactionDataTable($this, $request);
        $transactions = $transactionDataTable->render();

        $array_center = [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'textRotation' => 0, 'wrapText' => true,
        ];

        $array_border = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $array_font = [
            'fill' => array(
                'fillType' => Fill::FILL_SOLID,
                'color' => array('rgb' => 'E5E4E2' )
            ),
            'font'  => [
                'bold'  =>  true,
            ],
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Transaction Logs');
        // $range = ' ( '. $request->date_from . ' - ' . $request->date_to . ' )';
        // $title = empty($request->date_from) ? 'Transaction Logs' : 'Transaction Logs' . $range;
        // $sheet->setCellValue('A1', $title);
        // $sheet->getStyle("A1")->getFont()->setSize(16);
        // $sheet->getStyle('A1:J3')->applyFromArray(array_merge($array_border, $array_font));

        $sheet->fromArray(Transaction::TRANSACTION_EXPORT_COLUMNS, null, 'A1');
        $sheet->getStyle('A1:I1')->applyFromArray(['font' => ['bold' => true]]);
        $sheet->getStyle('A1:I1')->getAlignment()->applyFromArray($array_center);

        $count = 2;
        $row = count($transactions) + 1;

        foreach ($transactions as $transaction) {
            $status = $transaction->approval_status;

            if ($transaction->status == TransactionTrail::PROCESSING) {
                $status = TransactionTrail::PROCESSING;
            }
            if ($transaction->status == TransactionTrail::FAILED) {
                $status = TransactionTrail::FAILED;
            }

            $note = json_decode($transaction->latest_transaction_trail->data_json);
            $note = isset($note->payment_provider_response) ? $note->payment_provider_response->response_reason_text : '';

            $sheet->setCellValue('A'.$count, $transaction->created_at);
            $sheet->setCellValue('B'.$count, $transaction->transaction_number);
            $sheet->setCellValue('C'.$count, $transaction->payor_name);
            $sheet->setCellValue('D'.$count, $transaction->payment_setting->payment_provider->payment_provider_name);
            $sheet->setCellValue('E'.$count, $transaction->payment_setting->bank->bank_name);
            $sheet->setCellValue('F'.$count, $status);
            $sheet->setCellValue('G'.$count, $note);
            $sheet->setCellValue('H'.$count, $transaction->currency->currency_code);
            $sheet->setCellValue('I'.$count, $transaction->amount);
            // $sheet->setCellValue('J'.$count, $this->backOfficeNotesThread($transaction->hash_id));
            
            $sheet->getStyle('A'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('B'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('C'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('D'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('E'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('F'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('G'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('H'.$count)->getAlignment()->applyFromArray($array_center);
            $sheet->getStyle('I'.$count)->getAlignment()->applyFromArray($array_center);
            // $sheet->getStyle('J'.$count)->getAlignment()->applyFromArray($array_center);
            $count++;
        }

        foreach (range('A', 'I') as $col) {
            $sheet->getColumnDimension($col)->setAutoSize(true);
            $sheet->getStyle('A:I')->getAlignment()->setHorizontal('center');
        }

        $sheet->getStyle('A1:I' . $row)->applyFromArray($array_border);

        // $sheet->mergeCells('A1:J3');
        // $sheet->getStyle('A1:J3')->getAlignment()->applyFromArray($array_center);
        
        if ($request->type == strtolower(Transaction::PDF)) {
            IOFactory::registerWriter(Transaction::PDF, MpdfService::class);
        }

        $writer = IOFactory::createWriter($spreadsheet, ucfirst($request->type));

        $now = gmdate('d-m-Y');

        $path = storage_path('app/public') . '/files/' . Transaction::FILE_PREFIX . $now;

        \File::makeDirectory($path, $mode = 0777, true, true);

        $folder_path = storage_path() . '/app/public/files/' . Transaction::FILE_PREFIX . $now . '/';

        $res = $writer->save($folder_path . Transaction::FILE_PREFIX . $now . '.' . $request->type);

        /**
         * @todo  refactor to getSchemeAndHttpHost() 
         *        after auto redirect to https in server is done
         */
        $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
        $download_path = $protocol.'://' . $request->getHttpHost() . '/storage/files/' .
            Transaction::FILE_PREFIX . $now . '/' . Transaction::FILE_PREFIX . $now . '.' . $request->type;

        return $download_path;
    }

    private function backOfficeNotesThread($id)
    {
        $note = '';
        $back_office_notes = BackOfficeNote::where(
            'transaction_id',
            '=',
            $id
        )->with('user')->get();

        foreach ($back_office_notes as $key => $value) {
            $note .= $value->notes . ' ( ' . $value->created_at .
                ' Noted by ' . $value->user->first_name . ' ' .
                 $value->user->last_name . ')' . PHP_EOL;
        }

        return $note;
    }

    public function getTransactionByID($id){
        return $this->getModel()
            ->whereid($id)
            ->get();
    }
}
