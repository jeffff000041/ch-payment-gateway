<?php

namespace App\Repositories\Services;

use Auth;
use App\User;
use Carbon\Carbon;
use App\OtpWhitelist;
use App\Repositories\BaseRepositoryAbstract;
use App\Repositories\Services\AuthTokenService;

class OtpWhitelistService extends BaseRepositoryAbstract
{
    /**
     * [$authTokenService description]
     * @var [type]
     */
    protected $authTokenService;

    /**
     * @param OtpWhitelist $otpWhitelist
     */
    public function __construct(OtpWhitelist $otpWhitelist, 
        AuthTokenService $authTokenService)
    {
        $this->setModel($otpWhitelist);
        $this->authTokenService = $authTokenService;
    }

    /**
     * Check if origin is whitelisted
     *
     * @return mixed
     */
    public function checkIfWhitelistedOrigin($user, $request)
    {
        if ($request->ip()) {
            $requestHost = $request->ip();
            $otpWhitelist = $this->getModel()
            ->where('user_id', $user->id)
            ->get();
            
            if ($otpWhitelist && $this->isAllowed($otpWhitelist, $requestHost)) {
                return $this->authTokenService->createUserToken($user, $request);
            } else {
                return response()->json([
                    'message' => 'Unable to generate token.',
                ], 412);
            }
        } else {
            return response()->json([
                'message' => 'Unable to generate token.',
            ], 412);
        }
    }

    /**
     * Get request credentials if has username/password
     *
     * @return mixed
     */
    public function getUserRequest($request)
    {
        $email = [
            'email' => $request->get('username'),
            'password' => $request->get('password'),
        ];

        $username = [
            'username' => $request->get('username'),
            'password' => $request->get('password'),
        ];

        if (!Auth::attempt($email) && !Auth::attempt($username)) {
            return response([
                'message' => 'Unauthorized: Invalid Credentials',
            ], 401);
        }

        $user = $request->user();

        if ($user->is_active == User::DISABLED){
           return response()->json([
                'message' => 'Unauthorized: Account is locked',
            ], 401); 
        }

        return $this->checkIfWhitelistedOrigin($user, $request);
    }

    private function isAllowed($remotes, $ip) {
        $whitelisted = [];
        foreach ($remotes as $remote) {
            array_push($whitelisted, $remote->domain_name);
        }
        if (in_array($ip, $whitelisted))
            return true;
        else {
            foreach($whitelisted as $i) {
                $wildcardPos = strpos($i, "*");
                if($wildcardPos !== false && substr($ip, 0, $wildcardPos) . "*" == $i)
                    return true;
            }
        }
        return false;
    }
}