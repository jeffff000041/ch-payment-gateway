<?php

namespace App\Repositories\Services;

use App\User;
use App\Client;
use App\Repositories\BaseRepositoryAbstract;
use App\Repositories\Contracts\ClientContract;

class ClientService extends BaseRepositoryAbstract implements ClientContract
{
    /**
     * @param Client $clientProvider
     */
    public function __construct(Client $clientProvider)
    {
        $this->setModel($clientProvider);
    }

    /**
     * Get clients individually and grouped by other users.
     *
     * @return [type] [description]
     */
    public function getClientsGroupByProjects()
    {
        $merged_clients = [];
        $clients = [];

        $user_projects = $this->getModel()
            ->whereIsDefault(Client::IS_DEFAULT) //for default clients
            ->get();

        // // Initially OGPS only
        foreach ($user_projects as $key => $user_project) {
            array_push($merged_clients, [
                'id' => $user_project->id,
                'name' => $user_project->user->first_name,
            ]);
        }

        $clients = $this->getModel()
            ->whereNotIn('id', [Client::PERSONAL_ACCESS_CLIENT, Client::PASSWORD_GRANT_CLIENT]) //for default clients
            ->where('user_id', User::SUPER_USER_ID)
            ->get(['id', 'name'])
            ->toArray();

        return array_merge($merged_clients, $clients);


    }

    public function getClientData($client_id)
    {
        $client = $this->getModel()
            ->where('id', $client_id)
            ->get()
            ->toArray();

        return $client;
    }
}
