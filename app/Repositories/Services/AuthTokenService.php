<?php

namespace App\Repositories\Services;

use App\User;
use Carbon\Carbon;
use App\OneTimePasswords;

class AuthTokenService
{
    /**
     * Check if origin is whitelisted
     *
     * @return mixed
     */
    public function createUserToken($user, $request)
    {
        if ($request->get('login_key')) {
            $loginKeyCreate = OneTimePasswords::where([
                ['key', $request->login_key],
            ])->first();
        }

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me) {
            if ($token) {
                $token->expires_at = Carbon::now()->addWeeks(1);
            }
        }
        if ($token) {
            $token->save();
            if ($request->get('login_key')) {
                $loginKeyCreate->delete();
            }
            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
            ]);
        } else {
            return response()->json([
                'message' => 'Unable to generate token.',
            ], 412);
        }
    }

    /**
     * @param $request
     * @return mixed
     */
    public function recaptchaValidation($request, $type)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $captcha = $request->recaptcha;
        $secretkey = $type == User::CAPTCHA_TYPE_CHECKBOX ? config('ogps.captcha_secret_key') : config('ogps.inv_captcha_secret_key');
        $response = file_get_contents(config('ogps.captcha_url') . '?secret=' . $secretkey . '&response=' . $captcha . '&remoteip=' . $ip);
        $responseKeys = json_decode($response, true);

        return $responseKeys;
    }
}
