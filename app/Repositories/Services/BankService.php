<?php

namespace App\Repositories\Services;

use App\Bank;
use App\Repositories\BaseRepositoryAbstract;
use App\Repositories\Contracts\BankContract;

class BankService extends BaseRepositoryAbstract implements BankContract
{
    /**
     * @param Bank $Bank
     */
    public function __construct(Bank $bankProvider)
    {
        $this->setModel($bankProvider);
    }

    /**
     * Get available payment provider using bank name
     *
     * @return mixed
     */
    public function getBankAvailablePaymentProvidersByName($bank_name)
    {
        $banks = $this->getModel()
            ->join('payment_providers', 'banks.payment_provider_id', 'payment_providers.id')
            ->where('payment_providers.is_active', '=', '1')
            ->where('bank_name', $bank_name)
            ->get();
        $payment_providers = [];
        
        foreach ($banks as $key => $bank) {
            $payment_providers[] = $bank->payment_provider;
        }

        return $payment_providers;
    }

}