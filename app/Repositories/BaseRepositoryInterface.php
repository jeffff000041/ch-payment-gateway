<?php

namespace App\Repositories;

interface BaseRepositoryInterface
{
    public function getModel();
    
    /**
     * @param $model
     */
    public function setModel($model);
}
