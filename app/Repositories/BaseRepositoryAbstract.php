<?php

namespace App\Repositories;

abstract class BaseRepositoryAbstract
{
    /**
     * @var mixed
     */
    protected $model;

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }
    /**
     * @return void
     */
    public function setModel($model)
    {
        $this->model = $model;
    }
}
