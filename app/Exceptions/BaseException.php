<?php

namespace App\Exceptions;


use Symfony\Component\HttpFoundation\Response;

/**
 *
 */
class BaseException extends \Exception
{
    /**
     * @var mixed
     */
    protected $errors;

    /**
     * @var mixed
     */
    protected $status_code = Response::HTTP_UNPROCESSABLE_ENTITY;

    /**
     * @param $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }
}
