<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Modules\PaymentProvider\Exceptions\PostValidationException;
use App\Modules\PaymentProvider\Exceptions\UnknownPaymentProviderError;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($exception instanceof PostValidationException) {
            // saving
            $exception->storeFailTransactionTrail();
            
        } else if ($exception instanceof UnknownPaymentProviderError) {
            // saving
            $exception->storeFailTransactionTrail();
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof BaseException) {
            return response($exception->getErrors(), $exception->getStatusCode());
        }

        return parent::render($request, $exception);
    }
}
