<?php

namespace App\DataTables;

use DB;
use App\DataTable\DataTable;
use Illuminate\Http\Request;
use App\Repositories\Contracts\PaymentSettingContract;

class PaymentSettingsDataTable extends DataTable
{
    /**
     * @var mixed
     */
    protected $request;

    /**
     * @var mixed
     */
    protected $paymentSettingService;
    /**
     * @param PaymentSettingContract $paymentSettingService
     */
    public function __construct(Request $request, PaymentSettingContract $paymentSettingService)
    {
        $this->request = $request;
        $this->paymentSettingService = $paymentSettingService;
    }

    /**
     * @return mixed
     */
    public function render($client = null)
    {
        $payment_setting = $this->paymentSettingService->getAllWithAccumulatedTransactionAmount();


        //set model
        $this->setModel($payment_setting);

        //set request
        $this->setRequest($this->request);

        //set custom search
        $this->customSearch();

        //set custom sort
        $this->customSort();

        $this->paginate();

        return $this->build();
    }

    public function customSearch()
    {
        $this->search(function ($query) {
            if ($this->request->get('search')) {
                if ($this->request->get('bank_name')) {
                    $query->orWhereHas('bank', function ($query) {
                        $query->where('bank_name', $this->request->get('bank_name'))
                            ->where('bank_name', 'LIKE', '%' . $this->request->get('search') . '%');
                    })->orWhereHas('payment_provider', function ($query) {
                        $query->where('payment_provider_name', 'LIKE', '%' . $this->request->get('search') . '%');
                    });
                } else {
                    $query->orWhereHas('bank', function ($query) {
                        $query->where('bank_name', 'LIKE', '%' . $this->request->get('search') . '%');
                    })->orWhereHas('payment_provider', function ($query) {
                        $query->where('payment_provider_name', 'LIKE', '%' . $this->request->get('search') . '%');
                    });
                }

            }

            if ($this->request->get('bank_name')) {
                $query->whereHas('bank', function ($query) {
                    $query->where('bank_name', $this->request->get('bank_name'));
                });
            }

            if ($this->request->get('type')) {
                $query->where('type', $this->request->get('type'));
            }
        });

    }

    public function customSort()
    {
        switch ($this->request->get('order_col')) {

            case 'bank_code':
                $this->orderBy(function ($query) {
                    $query->addSelect(DB::raw('(SELECT banks.bank_code
                                            FROM
                                            banks
                                            WHERE
                                            banks.id = payment_settings.bank_id)
                                            as sort_bank_code'))
                        ->orderBy('sort_bank_code', $this->request->get('order'));
                });
                break;
            case 'bank_name':
                $this->orderBy(function ($query) {
                    $query->addSelect(DB::raw('(SELECT banks.bank_name
                                            FROM
                                            banks
                                            WHERE
                                            banks.id = payment_settings.bank_id)
                                            as sort_bank_name'))
                        ->orderBy('sort_bank_name', $this->request->get('order'))
                        ->orderBy('priority', 'asc');
                });
                break;
            case 'payment_provider_code':
                $this->orderBy(function ($query) {
                    $query->addSelect(DB::raw('(SELECT payment_providers.payment_provider_code
                                            FROM
                                            payment_providers
                                            WHERE
                                            payment_providers.id = payment_settings.payment_provider_id)
                                            as sort_payment_provider_code'))
                        ->orderBy('sort_payment_provider_code', $this->request->get('order'));
                });
                break;

            case 'client_name':
                $this->orderBy(function ($query) {
                    $query->addSelect(DB::raw('(SELECT oauth_clients.name
                                            FROM
                                            oauth_clients
                                            WHERE
                                            oauth_clients.id = payment_settings.client_id)
                                            as sort_client_name'))
                        ->orderBy('sort_client_name', $this->request->get('order'));
                });
                break;

            default:
                $this->orderBy();
                break;
        }
    }
}
