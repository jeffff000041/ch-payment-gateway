<?php

namespace App\DataTables;

use DB;
use App\DataTable\DataTable;
use Illuminate\Http\Request;
use App\Repositories\Contracts\AuditContract;

class PlatformLogDataTable extends DataTable
{
    /**
     * @var mixed
     */
    protected $auditService;

    /**
     * @var mixed
     */
    protected $request;
    /**
     * @param TransactionContract $transactionService
     */
    public function __construct(AuditContract $auditService, Request $request)
    {
        $this->auditService = $auditService;
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function render($client = null)
    {
        $platform_logs = $this->auditService->getAllAuditLog($this->request);

        if ($client) {
            $platform_logs->where('client_id', $client->id);
        }else{
            $platform_logs->with('client');
        }

        $date_from = $this->request->get('date_from');
        $date_to = $this->request->get('date_to');

        if ($date_from && $date_to) {
            $platform_logs->where('audits.created_at', '>=', $date_from)
                ->where('audits.created_at', '<=', $date_to);
        }

        //set model
        $this->setModel($platform_logs);

        //set request
        $this->setRequest($this->request);

        //set searchable in model context
        $this->setSearchable([
            // 'transaction_number',
            // 'amount',
            'payor_name',
        ]);

        //set custom search
        $this->customSearch();

        //set custom sort
        $this->customSort();

        //set pagination
        $this->paginate();

        //build
        return $this->build();
    }

    /**
     * @return mixed
     */
    public function paginate()
    {
        $rows = null;

        if (!$this->request->type) {
            $rows = $this->getRequest()->get('rows') ? $this->getRequest()->get('rows') : null;
        } else {
            $rows = $this->getModel()->count();
        }

        $this->setModel($this->getModel()->paginate($rows));

        return $this;
    }

    public function customSearch()
    {
        $this->search(function ($query) {
            $query
                ->orWhereHas('payment_setting.payment_provider', function ($query) {
                    $query->where('payment_provider_name', 'LIKE', '%' . $this->request->get('search') . '%');
                })
                ->orWhereHas('payment_setting.bank', function ($query) {
                    $query->where('bank_name', 'LIKE', '%' . $this->request->get('search') . '%');
                })
                ->orWhereHas('currency', function ($query) {
                    $query->where('currency_code', 'LIKE', '%' . $this->request->get('search') . '%');
                })
                // ->orWhereHas('client', function ($query) {
                //     $query->where('name', 'LIKE', '%' . $this->request->get('search') . '%');
                // })
                // ->orWhere('transaction_number', 'LIKE', '%' . $this->request->get('search') . '%')
                // ->orWhere('reference_number', 'LIKE', '%' . $this->request->get('search') . '%')
                ->orWhere('approval_status', 'LIKE', '%' . $this->request->get('search') . '%');
        });
    }

    public function customSort()
    {
        switch ($this->request->get('order_col')) {

            case 'payment_provider':
                $this->orderBy(function ($query) {
                    $query->select('*', DB::raw('(SELECT payment_providers.payment_provider_name
                                            FROM
                                            payment_settings
                                            JOIN
                                            payment_providers
                                            ON
                                            payment_settings.payment_provider_id = payment_providers.id
                                            WHERE
                                            payment_settings.id = transactions.payment_setting_id)
                                            as sort_payment_provider'))
                        ->orderBy('sort_payment_provider', $this->request->get('order'));
                });
                break;

            case 'client':
                $this->orderBy(function ($query) {
                    $query->select('*', DB::raw('(SELECT oauth_clients.name
                                            FROM
                                            oauth_clients
                                            WHERE
                                            oauth_clients.id = transactions.client_id)
                                            as sort_payment_provider'))
                        ->orderBy('sort_payment_provider', $this->request->get('order'));
                });
                break;

            case 'payment_method':
                $this->orderBy(function ($query) {
                    $query->select('*', DB::raw('(SELECT banks.bank_name
                                            FROM
                                            payment_settings
                                            JOIN
                                            banks
                                            ON
                                            payment_settings.bank_id = banks.id
                                            WHERE
                                            payment_settings.id = transactions.payment_setting_id)
                                            as sort_bank'))
                        ->orderBy('sort_bank', $this->request->get('order'));
                });
                break;

            case 'currency':
                $this->orderBy(function ($query) {
                    $query->select('*', DB::raw('(SELECT currencies.currency_code FROM currencies
                                            WHERE currencies.id=transactions.currency_id) as sort_currency'))
                        ->orderBy('sort_currency', $this->request->get('order'));
                });
                break;

            case 'status':
                $this->orderBy(function ($query) {
                    $query->orderBy(DB::raw('CAST(status AS CHAR)'), $this->request->get('order'));
                });
                break;

            default:
                $this->orderBy();
                break;
        }
    }
}
