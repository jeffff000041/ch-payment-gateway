<?php

namespace App\DataTables;

use DB;
use App\DataTable\DataTable;
use Illuminate\Http\Request;
use App\Repositories\Contracts\PaymentProviderContract;

class PaymentProviderDataTable extends DataTable
{
    /**
     * @var mixed
     */
    protected $paymentProviderService;

    /**
     * @var mixed
     */
    protected $request;
    /**
     * @param PaymentProviderContract $paymentProviderService
     */
    public function __construct(PaymentProviderContract $paymentProviderService, Request $request)
    {
        $this->paymentProviderService = $paymentProviderService;
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function render($client = null)
    {
        $payment_providers = $this->paymentProviderService->getActivePaymentProviders($this->request);

        //set model
        $this->setModel($payment_providers);

        //set request
        $this->setRequest($this->request);

        //set searchable in model context
        $this->setSearchable([
            // 'transaction_number',
            // 'amount',
            'payment_provider_name',
        ]);

        //set custom search
        $this->customSearch();

        //set custom sort
        $this->customSort();

        //set pagination
        $this->paginate();

        //build
        return $this->build();
    }

    /**
     * @return mixed
     */
    public function paginate()
    {
        $rows = null;

        if (!$this->request->type) {
            $rows = $this->getRequest()->get('rows') ? $this->getRequest()->get('rows') : null;
        } else {
            $rows = $this->getModel()->count();
        }
        $this->setModel($this->getModel()->paginate($rows));

        return $this;
    }

    public function customSearch()
    {
        $this->search(function ($query) {
            $query->where('payment_provider_name', 'LIKE', '%' . $this->request->get('search') . '%');
        });
    }

    public function customSort()
    {
        switch ($this->request->get('order_col')) {

            case 'payment_provider_name':
                $this->orderBy(function ($query) {
                    $query->select('*', DB::raw('(SELECT payment_providers.payment_provider_name
                                            FROM    
                                            payment_providers'))
                        ->orderBy('sort_payment_provider_name', $this->request->get('order'));
                });
                break;

            default:
                $this->orderBy();
                break;
        }
    }
}
