<?php

namespace App\DataTables;

use App\User;
use App\DataTable\DataTable;
use Illuminate\Http\Request;
use App\Repositories\Contracts\TransactionContract;

class AdminDataTable extends DataTable
{
    /**
     * @var mixed
     */
    protected $request;
    /**
     * @param TransactionContract $transactionService
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function render()
    {
        $model = User::admin();

        //set model
        $this->setModel($model);

        //set request
        $this->setRequest($this->request);

        $this->setSearchable([
            'username',
            'first_name',
            'last_name',
            'email',
        ]);

        //set custom search
        $this->customSearch();

        //set custom sort
        $this->customSort();

        $this->paginate();

        return $this->build();
    }

    public function customSearch()
    {

        if($this->request->get('search')) {
            $search = $this->request->get('search');
            $data = $this->getModel()
                        ->Where("username", 'LIKE', "%".$search."%")
                        ->orWhere("first_name", 'LIKE', "%".$search."%")
                        ->orWhere("last_name", 'LIKE', "%".$search."%");
            $this->setModel($data);
        }
        
        return $this;
    }

    public function customSort()
    {
        switch ($this->request->get('order_col')) {

            default:
                $this->orderBy();
                break;
        }
    }
}
