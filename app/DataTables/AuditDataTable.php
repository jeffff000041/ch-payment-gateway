<?php

namespace App\DataTables;

use DB;
use App\DataTable\DataTable;
use Illuminate\Http\Request;
use App\Repositories\Contracts\AuditContract;

class AuditDataTable extends DataTable
{
    /**
     * @var mixed
     */
    protected $auditService;

    /**
     * @var mixed
     */
    protected $request;
    /**
     * @param AuditContract $auditService
     */
    public function __construct(AuditContract $auditService, Request $request)
    {
        $this->auditService = $auditService;
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function render()
    {
        $audit = $this->auditService->getAllAuditLog($this->request);

        $this->setModel($audit);

        // //set request
        $this->setRequest($this->request);


        // //set custom search
        $this->customSearch();

        // //set custom sort
        $this->customSort();

        // //set pagination
        $this->paginate();

        //build
        return $this->build();
    }

    /**
     * @return mixed
     */
    public function paginate()
    {
        $rows = null;

        if (!$this->request->type) {
            $rows = $this->getRequest()->get('rows') ? $this->getRequest()->get('rows') : null;
        } else {
            $rows = $this->getModel()->count();
        }

        $this->setModel($this->getModel()->paginate($rows));
        return $this;
    }

    /**
     * @return mixed
     */
    public function customSort()
    {
        if ($this->request->get('order_col')) {
            $order = $this->getModel()->orderBy(
                        $this->request->get('order_col'), 
                        $this->request->get('order')
                    );
            $this->setModel($order);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function customSearch()
    {
        $this->getModel()->whereNotNull("user_type");
        if ($this->request->get('date_from') AND $this->request->get('date_to')) {
            $from = date("Y-m-d H:i:s", strtotime($this->request->get('date_from')));
            $to = date("Y-m-d H:i:s", strtotime($this->request->get('date_to')));
            $between = $this->getModel()->whereBetween("created_at", [$from, $to]);
            $this->setModel($between);
        }

        if($this->request->get('search')) {
            $search = $this->request->get('search');
            $between = $this->getModel()
                        ->Where("event", 'LIKE', "%".$search."%")
                        ->orWhere("auditable_type", 'LIKE', "%".$search."%")
                        ->orWhereHas("user", function($query){
                            $query->where('username', "LIKE", "%".$this->request->get('search')."%");
                        });
            $this->setModel($between);
        }
        
        return $this;
    }


}
