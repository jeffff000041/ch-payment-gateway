<?php

namespace App\DataTables;

use DB;
use App\User;
use App\Client;
use App\DataTable\DataTable;
use Illuminate\Http\Request;
use App\Repositories\Contracts\TransactionContract;

class ClientDataTable extends DataTable
{
    /**
     * @var mixed
     */
    protected $request;
    /**
     * @param TransactionContract $transactionService
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function render()
    {
        // $model = Client::query();
        $userId = $this->request->user()->getKey();

        if ($userId != User::SUPER_USER_ID) {
            $project_id = $this->request->user()->project->id;
            $client = Client::whereHas('user.project', function ($query) use ($project_id) {
                $query->where('id', $project_id);
            });
        } else {
            $client = Client::whereNotNull('user_id');
        }

        $client->with('user')
        ->where('is_default', 0)
        ->where('revoked', 0);

        //set model
        $this->setModel($client);

        //set request
        $this->setRequest($this->request);

        $this->setSearchable([
            'name',
        ]);

        $this->customSearch();

        $this->customSort();

        $this->paginate();

        //make 'secret' visible
        $client = $this->getModel()
            ->getCollection()
            ->makeVisible('secret');

        //set collection
        $this->setModel($this->getModel()->setCollection($client));

        return $this->build();
    }

    public function customSearch()
    {
        $this->search(function ($query) {
            $query->orWhere('name', 'LIKE', '%' . $this->request->get('search') . '%');
        });
    }

    public function customSort()
    {
        switch ($this->request->get('order_col')) {
            default:
                $this->orderBy();
                break;
        }
    }
}
