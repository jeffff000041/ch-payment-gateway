<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TransactionMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $email_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_transaction_data)
    {
        $this->email_transaction_data = $email_transaction_data;
    }

    /**
    * Build the message.
    *
    * @return $this
    */
    public function build()
    {
        return $this->view('email.email_transactions', $this->email_transaction_data);
    }
}
