<?php

namespace App\DataTable;

use App\DataTable\DataTable;
use Illuminate\Support\ServiceProvider;

class DataTableServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        #dataTable
        $this->app->bind(DataTable::class, function () {
            return new DataTable;
        });
    }
}
