<?php

namespace App\DataTable;

use App\DataTable\DataTable;
use Illuminate\Support\Facades\Facade;

class DataTableFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return DataTable::class;
    }
}
