<?php

namespace App\DataTable;

class DataTable
{
    /**
     * @var int
     */
    protected $default_row = 10;

    /**
     * @var int
     */
    protected $default_order_col = 'id';

    /**
     * @var int
     */
    protected $default_order = 'asc';

    /**
     * @var int
     */
    protected $request = 10;

    /**
     * @var int
     */
    protected $model;

    /**
     * @var array
     */
    protected $searchable = [

    ];
    /**
     * @param $request
     * @return mixed
     */
    public function __construct($model = null, $request = null)
    {
        $this->setModel($model);
        $this->setRequest($request);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function make($model, $request)
    {
        $this->setModel($model);
        $this->setRequest($request);

        return $this->search()->orderBy()->paginate()->getModel();
    }

    /**
     * @param $request
     * @return mixed
     */
    public function build()
    {
        return $this->getModel();
    }

    /**
     * @param $request
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param $request
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return mixed
     */
    public function search($callback = null)
    {
        $search = $this->getRequest()->get('search') ? $this->getRequest()->get('search') : '';
        $searchable = $this->searchable;

        $query = $this->getModel();

        $query = $query->where(function ($query) use ($searchable, $search, $callback) {

            if(!empty($searchable)){
                foreach ($this->searchable as $key => $field) {
                    $query = $query->orWhere($field, 'LIKE', '%' . $search . '%');
                }
            }
            
            if (is_callable($callback)) {

                return $callback($query);
            }

            return $query;
        });
        
        $this->setModel($query);

        return $this;
    }

    /**
     * @return mixed
     */
    public function orderBy($callback = null)
    {
        if (is_callable($callback)) {
            $callback($this->getModel());

            return $this;
        }
        
        $order_col = $this->getRequest()->get('order_col') ? $this->getRequest()->get('order_col') : $this->default_order_col;
        $order = $this->getRequest()->get('order') ? $this->getRequest()->get('order') : $this->default_order;

        $this->setModel($this->getModel()->orderBy($order_col, $order));

        return $this;
    }

    /**
     * @return mixed
     */
    public function paginate()
    {
        $rows = $this->getRequest()->get('rows') ? $this->getRequest()->get('rows') : $this->default_row;

        $this->setModel($this->getModel()->paginate($rows));

        return $this;
    }

    /**
     * @param $searchable
     * @return mixed
     */
    public function setSearchable($searchable)
    {
        $this->searchable = $searchable;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSearchable()
    {
        return $this->searchable;
    }
}
