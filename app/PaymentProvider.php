<?php

namespace App;

use DB;
use App\Traits\EncryptID;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentProvider extends Model implements Auditable
{
    use SoftDeletes, EncryptID, \OwenIt\Auditing\Auditable;

    const INACTIVE_PROVIDERS = array(
        'ec365',
        'jbp',
    );

    /**
     * @var array
     */
    protected $fillable = [
        'is_active',
        'payment_provider_name',
        'payment_provider_code',
    ];

    //
    /**
     * @return mixed
     */
    public function banks()
    {
        return $this->hasMany(Bank::class);
    }

    /**
     * Unique payment provider fields based
     *
     * @return mixed
     */
    public function field_category_unique_payment_provider_fields()
    {
        return $this->hasManyThrough(FieldCategoryPaymentProviderField::class,
            PaymentProviderField::class)
            ->groupBy('payment_provider_fields.payment_providers_field_code');
    }
    
    /**
     * @return mixed
     */
    public function field_category_payment_provider_fields()
    {
        return $this->hasManyThrough(FieldCategoryPaymentProviderField::class,
            PaymentProviderField::class);
    }

    /**
     * @return mixed
     */
    public function payment_provider_fields()
    {
        return $this->hasMany(PaymentProviderField::class);
    }

    /**
     * @return mixed
     */
    public function currencies()
    {
        return $this->belongsToMany(Currency::class);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWhereStatusActive($query)
    {
        return $query->select('payment_providers.*')->whereNotIn('payment_providers.payment_provider_code', PaymentProvider::INACTIVE_PROVIDERS);
    }
}
