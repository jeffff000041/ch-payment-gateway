<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\TransactionMail;
use Mail;

class EmailTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email_transaction_data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email_transaction_data)
    {
        $this->email_transaction_data = $email_transaction_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new TransactionMail($this->email_transaction_data);
        Mail::to($this->email_transaction_data['user_data']->email)->send($email);
    }
}
