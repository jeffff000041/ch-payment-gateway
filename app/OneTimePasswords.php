<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OneTimePasswords extends Model
{
    /**
     * 
     * @var [type]
     */
    protected $fillable = [
        'user_id', 
        'key',
        'reference',
    ];

    /**
     * 
     * @var [type]
     */
    protected $hidden = [
        'id',
        'user_id',
        'key',
    ];

    /**
     *  value
     */
    const RESET_TOKEN_TIME = 5;
}
