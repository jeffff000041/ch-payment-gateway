<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(function ($router) {
            $router->forClients();
        }, ['middleware' => ['auth:api', 'role:super_admin'], 'prefix' => 'api/v1/admin/oauth']);
        Passport::tokensExpireIn(now()->addDays(1));
        Passport::refreshTokensExpireIn(now()->addDays(30));
        
        Passport::routes(function ($router) {
            $router->forAuthorization();
            $router->forAccessTokens();
            $router->forTransientTokens();
            $router->forPersonalAccessTokens();
        }, ['prefix' => 'api/v1/oauth']);

        Passport::enableImplicitGrant();
        Passport::tokensCan([
            'admin' => 'Client Administrator',
            'transaction' => 'Client User',
        ]);
    }
}
