<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        #Payment Setting
        $this->app->bind(
            'App\Repositories\Contracts\PaymentSettingContract',
            'App\Repositories\Services\PaymentSettingService'
        );

        #Payment Provider
        $this->app->bind(
            'App\Repositories\Contracts\PaymentProviderContract',
            'App\Repositories\Services\PaymentProviderService'
        );

        #Bank
        $this->app->bind(
            'App\Repositories\Contracts\BankContract',
            'App\Repositories\Services\BankService'
        );

        #Transaction
        $this->app->bind(
            'App\Repositories\Contracts\TransactionContract',
            'App\Repositories\Services\TransactionService'
        );

        #Transaction Trail
        $this->app->bind(
            'App\Repositories\Contracts\TransactionTrailContract',
            'App\Repositories\Services\TransactionTrailService'
        );

        #Project
        $this->app->bind(
            'App\Repositories\Contracts\ProjectContract',
            'App\Repositories\Services\ProjectService'
        );

        #Client
        $this->app->bind(
            'App\Repositories\Contracts\ClientContract',
            'App\Repositories\Services\ClientService'
        );

        #Audit
        $this->app->bind(
            'App\Repositories\Contracts\AuditContract',
            'App\Repositories\Services\AuditService'
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'App\Repositories\Contracts\BankContract',
            'App\Repositories\Contracts\ClientContract',
            'App\Repositories\Contracts\ProjectContract',
            'App\Repositories\Contracts\TransactionContract',
            'App\Repositories\Contracts\PaymentSettingContract',
            'App\Repositories\Contracts\PaymentProviderContract',
            'App\Repositories\Contracts\TransactionTrailContract',
            'App\Repositories\Contracts\AuditContract',
        ];
    }
}
