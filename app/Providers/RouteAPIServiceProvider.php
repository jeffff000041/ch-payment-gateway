<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteAPIServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $admin_namespace = 'App\Http\Controllers\API\v1\Admin';
    
    protected $client_namespace = 'App\Http\Controllers\API\v1\Client';

    protected $namespace = 'App\Http\Controllers\API\v1';

    protected $auth_namespace = 'App\Http\Controllers\API\v1\Auth';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapAdminApiRoutes();

        $this->mapClientApiRoutes();

        $this->mapApiAuthRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAdminApiRoutes()
    {
        Route::prefix('api/v1/admin')
             ->middleware('api')
             ->namespace($this->admin_namespace)
             ->group(base_path('routes/api/v1/admin/api.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapClientApiRoutes()
    {
        Route::prefix('api/v1/client')
             ->middleware('api')
             ->namespace($this->client_namespace)
             ->group(base_path('routes/api/v1/client/api.php'));
    }

    /**
     * Define the "api" auth routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiAuthRoutes()
    {
        Route::prefix('api/v1')
             ->middleware('api')
             ->namespace($this->auth_namespace)
             ->group(base_path('routes/api/v1/auth.php'));
    }
}
