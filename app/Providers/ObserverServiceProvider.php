<?php

namespace App\Providers;

use App\TransactionTrail;
use Laravel\Passport\Client;
use App\Observers\OAuthClientObserver;
use Illuminate\Support\ServiceProvider;
use App\Observers\TransactionTrailObserver;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        TransactionTrail::observe(TransactionTrailObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
