<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ProjectHookServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        #Payment Setting
        $this->app->bind(
            'App\Modules\Projects\ProjectHookFactory'
        );
    }
}
