<?php

namespace App\Http\Requests\API;

use Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\API\BaseRequest;

class AccountRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->accountRules();
    }

    protected function accountRules()
    {
        $basic = [
            'first_name' => "required|between:1,50|regex:/^[A-Za-z \'\.\-\p{Han}]+$/u",
            'last_name' => "required|between:1,50|regex:/^[A-Za-z \'\.\-\p{Han}]+$/u",
            'email' => 'required|email|unique:users,email,' . $this->user()->id,
        ];

        $credentials = [
            'old_password' => 'required|different:password|old_password:'.$this->user()->password,
            'password' => 'required_with:old_password,|regex:' . config('ogps.password_regex') . '|min:8',
            'confirm_password' => 'required_with:old_password,|same:password',
        ];

        if (!is_null($this->request->get('old_password')) || !is_null($this->request->get('password')) 
            || !is_null($this->request->get('confirm_password'))) {
            return array_merge($basic, $credentials);
        } else {
            return $basic;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'old_password.old_password' => 'Your old password is incorrect.',
            'password.required_with' => 'The password field is required.',
            'confirm_password.required_with' => 'The confirm password field is required.',
            'password.min' => 'The password field must have at least 8 characters.',
            'password.regex' => 'The password field must contain alphanumeric characters.',
            'first_name.regex' => 'The first name field may have letters, periods and dashes only.',
            'last_name.regex' => 'The last name field may have letters, periods and dashes only.',
        ];
    }
}
