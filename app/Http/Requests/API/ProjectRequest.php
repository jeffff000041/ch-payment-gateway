<?php

namespace App\Http\Requests\API;

use Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\API\BaseRequest;

class ProjectRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //create
        if ($this->isMethod('post')) {

            return $this->createRule();

        } elseif ($this->isMethod('put')) {

            return $this->updateRule();

        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'project_name.unique' => 'This project name has already been taken.',
        ];
    }

    /**
     * @return mixed
     */
    protected function createRule()
    {

        return [
            'project_name' => 'required|unique:projects',
        ];
    }

    /**
     * @return mixed
     */
    protected function updateRule()
    {
        $user = Auth::guard('api')->user();
        return [
            'project_name' => 'required|unique:projects,project_name,' . walle_decrypt($this->route('id')),
        ];
    }
}
