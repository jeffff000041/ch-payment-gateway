<?php

namespace App\Http\Requests\API;

use Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\API\BaseRequest;

class LoginKeyRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->authRules();
    }

     /*
     *
     * @return array
     */
    protected function authRules()
    {
        $credentials = [
            'username' => 'required',
            'password' => 'required',
        ];

        $login_key = [
            'login_key' => 'required|regex:' . config('ogps.login_key_regex') . '|numeric',
            'recaptcha' => 'required|string',
        ];

        if ($this->request->get('login_key')) {
            return $login_key;
        } else {
            return $credentials;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'login_key.numeric' => 'The login key must be a number.',
            'recaptcha.required' => 'Please verify that you are not a robot.',
        ];
    }

}
