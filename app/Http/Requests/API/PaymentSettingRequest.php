<?php

namespace App\Http\Requests\API;

use Auth;
use App\PaymentSetting;
use Illuminate\Validation\Rule;
use App\Http\Requests\API\BaseRequest;

class PaymentSettingRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //create
        if ($this->isMethod('post')) {

            return $this->createRule();

        } elseif ($this->isMethod('put')) {

            return $this->updateRule();

        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'payment_provider_id.unique' => 'The payment provider id and bank id has already been taken for this client.',
        ];
    }

    /**
     * @return mixed
     */
    protected function createRule()
    {
        return [
            'payment_provider_id' => [
                Rule::unique('payment_settings')->where(function ($query) {
                    return $query->where('bank_id', $this->request->get('bank_id'))
                        ->where('client_id', $this->request->get('client_id'))
                        ->where('type', $this->request->get('type') ? $this->request->get('type') : PaymentSetting::TYPE_DEPOSIT)
                        ->whereNull('deleted_at');
                }),
            ],
            'is_active' => 'required',
            'maximum_amount' => 'required|integer',
            'percentage_to_notify' => 'integer|max:100',
            'type' => [
                'nullable',
                Rule::in([PaymentSetting::TYPE_DEPOSIT, PaymentSetting::TYPE_WITHDRAW])
            ],
            'configurations' => 'required|json',
        ];
    }

    /**
     * @return mixed
     */
    protected function updateRule()
    {
        return [
            'payment_provider_id' => [
                Rule::unique('payment_settings')->where(function ($query) {
                    return $query->where('bank_id', $this->request->get('bank_id'))
                        ->where('client_id', $this->request->get('client_id'))
                        ->where('type', $this->request->get('type') ? $this->request->get('type') : PaymentSetting::TYPE_DEPOSIT)
                        ->whereNull('deleted_at');
                })->ignore(walle_decrypt($this->get('id'))),
            ],
            'is_active' => 'required',
            'maximum_amount' => 'required',
            'override_accumulated_amount' => 'nullable',
            'type' => [
                'nullable',
                Rule::in([PaymentSetting::TYPE_DEPOSIT, PaymentSetting::TYPE_WITHDRAW])
            ],
            'configurations' => 'required|json',
        ];
    }
}
