<?php

namespace App\Http\Requests\API;

use Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\API\BaseRequest;

class ClientRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //create
        if ($this->isMethod('post')) {

            return $this->createRule();

        } elseif ($this->isMethod('put')) {

            return $this->updateRule();

        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
        ];
    }

    /**
     * @return mixed
     */
    protected function createRule()
    {
        $rules = [
            'client_name' => 'required|unique:clients',
        ];

        return $rules;
    }

    /**
     * @return mixed
     */
    protected function updateRule()
    {
        
        return [
            'client_name' => 'required|unique:clients,client_name,' . walle_decrypt($this->route('id')),
        ];
    }
}
