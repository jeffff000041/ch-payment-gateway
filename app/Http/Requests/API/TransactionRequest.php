<?php

namespace App\Http\Requests\API;

use Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\API\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //create
        $rules = [
            'type' => 'required',
        ];

        if ($this->type == 'deposit') {
            $rules['amount'] = 'required|numeric';
            $rules['currency'] = 'required';
            $rules['bank_id'] = 'required';
            $rules['payor_name'] = 'required';
            $rules['payment_provider_code'] = 'required';

            if ($this->payment_provider_code == 'astropay') {
                $rules['card_num'] = 'required|numeric';
                $rules['card_code'] = 'required|numeric';
                $rules['card_expiration'] = 'required|string';
            }
        } else if ($this->type == 'withdraw') { //this is for transaction type = withdraw
            $rules = [
                'amount' => 'required|numeric',
                'currency' => 'required',
                'mobile_number' => 'required',
                'receiver_name' => 'required',
                'reference_number' => 'required'
            ];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'card_num.required' => 'The Card Number field is required',
            'card_code.required' => 'The Card Code field is required',
            'card_expiration.required' => 'The Card Expiration field is required',
            'bank_id.required' => 'The Payment Method field is required',
        ];
    }
}
