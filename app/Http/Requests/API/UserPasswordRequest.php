<?php

namespace App\Http\Requests\API;

use Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\API\BaseRequest;

class UserPasswordRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|regex:' . config('ogps.password_regex') . '|min:8',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.regex' => 'The password field must contain alphanumeric characters.',
        ];
    }
}