<?php

namespace App\Http\Requests\API;

use Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\API\BaseRequest;

class UserRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //create
        if ($this->isMethod('post')) {

            return $this->createRule();

        } elseif ($this->isMethod('put')) {

            return $this->updateRule();

        }
    }

    /**
     * @return mixed
     */
    protected function createRule()
    {
        $rules = [
            'first_name' => 'required|between:1,50',
            'last_name' => 'required|between:1,50',
            'email' => 'required|email|unique:users',
            'username' => 'required|regex:' . config('ogps.username_regex') . '|between:6,50|unique:users',
            'password' => 'required',
        ];

        return $rules;
    }

    /**
     * @return mixed
     */
    protected function updateRule()
    {
        
        return [
            'first_name' => 'required|between:1,50',
            'last_name' => 'required|between:1,50',
            'email' => 'required|unique:users,email,' . walle_decrypt($this->route('id')),
            'username' => 'required|regex:' . config('ogps.username_regex') . 
                    '|between:6,50|unique:users,email,' . walle_decrypt($this->route('id')),
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.regex' => 'The username may only contain letters, numbers, underscores and periods.',
        ];
    }
}
