<?php

namespace App\Http\Requests\API;

use Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\API\BaseRequest;

class AccountPasswordRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'old_password' => 'required|different:password|old_password:'.$this->user()->password,
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'old_password.old_password' => 'Your old password is incorrect.'
        ];
    }
}
