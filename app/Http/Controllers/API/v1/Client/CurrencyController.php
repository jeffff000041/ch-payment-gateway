<?php

namespace App\Http\Controllers\API\v1\Client;

use App\Currency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    /**
     * @api {get} /currencies Get all Currency
     * @apiName GetCurrencies
     * @apiGroup Currency
     *
     * @apiHeader {String} Content-Type application/json.
     *
     * @apiHeaderExample Header-Sample:
     *     {
     *       "Content-Type": "application/json",
     *     }
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *   [
     *       {
     *           "currency_name": "CHINA",
     *           "currency_code": "RMB",
     *           "created_by": 1,
     *           "updated_by": 1,
     *           "deleted_at": null,
     *           "created_at": "2018-09-14 06:03:17",
     *           "updated_at": "2018-09-14 06:03:17",
     *           "hash_id": ":hash_currency_id"
     *       },
     *       {
     *           "currency_name": "United States of America",
     *           "currency_code": "USD",
     *           "created_by": 1,
     *           "updated_by": 1,
     *           "deleted_at": null,
     *           "created_at": "2018-09-14 06:03:17",
     *           "updated_at": "2018-09-14 06:03:17",
     *           "hash_id": ":hash_currency_id"
     *       },
     *       {
     *           "currency_name": "Philippines",
     *           "currency_code": "PHP",
     *           "created_by": 1,
     *           "updated_by": 1,
     *           "deleted_at": null,
     *           "created_at": "2018-09-14 06:03:17",
     *           "updated_at": "2018-09-14 06:03:17",
     *           "hash_id": ":hash_currency_id"
     *       }
     *   ]
     */
    public function index(Request $request)
    {
        return response(Currency::all());
    }
}
