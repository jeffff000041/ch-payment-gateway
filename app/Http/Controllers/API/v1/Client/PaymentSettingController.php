<?php

namespace App\Http\Controllers\API\v1\Client;

use DB;
use App\Project;
use App\FieldCategory;
use App\PaymentSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\PaymentSettingsDataTable;
use App\Repositories\Contracts\BankContract;
use App\Traits\PaymentSettingControllerTrait;
use App\Http\Requests\API\PaymentSettingRequest;
use App\Repositories\Contracts\PaymentSettingContract;
use App\Repositories\Contracts\PaymentProviderContract;

class PaymentSettingController extends Controller
{
    use PaymentSettingControllerTrait;

    /**
     * Save payment provider.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request, PaymentSettingsDataTable $paymentSettingsDataTable)
    {
        return $paymentSettingsDataTable->render();
    }

    /**
     * Save payment provider.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(PaymentSettingRequest $projectPaymentSettingRequest,
        PaymentProviderContract $paymentProviderService,
        PaymentSettingContract $paymentSettingService) {
        $this->setClient($projectPaymentSettingRequest);
        #already decrypted in App\Http\Middleware\PaymentSettingsMiddleware
        $payment_provider_id = $projectPaymentSettingRequest->get('payment_provider_id');

        $client_id = $this->getClient()->id;

        $this->validateConfiguration($paymentProviderService,
            $projectPaymentSettingRequest,
            $payment_provider_id);

        return response($paymentSettingService->create($projectPaymentSettingRequest, $client_id, 0), 200);
    }

    /**
     * Update Project's payment provider
     *
     * @param Request $request
     * @return int
     */
    public function update(PaymentSettingRequest $projectPaymentSettingRequest,
        PaymentProviderContract $paymentProviderService,
        PaymentSettingContract $paymentSettingService,
        $id) {

        $id = walle_decrypt($id);
        $this->setClient($projectPaymentSettingRequest);

        $payment_setting = PaymentSetting::where('client_id', $this->getClient()->id)->findOrFail($id);

        $this->validateConfiguration($paymentProviderService,
            $projectPaymentSettingRequest,
            $payment_setting->payment_provider->id);

        return response($paymentSettingService->update($projectPaymentSettingRequest, $payment_setting, 0), 200);
    }

    /**
     * Update Project's payment provider
     *
     * @param Request $request
     * @return int
     */
    public function show(Request $request, PaymentSettingContract $paymentSettingService,
        $id) {

        $this->setClient($request);

        $id = walle_decrypt($id);

        return response((array) $paymentSettingService->getPaymentProviderSettings(
            $id,
            FieldCategory::where('field_category_name', 'credentials')->first()->id,
            $this->getClient()->id
        ), 200);
    }

    /**
     * Soft Delete Project's payment provider
     *
     * @param Request $request
     * @return int
     */
    public function destroy(Request $request, $id)
    {
        $id = walle_decrypt($id);

        $this->setClient($request);

        PaymentSetting::where('client_id', $this->getClient()->id)->findOrFail($id);

        return response(PaymentSetting::destroy($id), 200);
    }

    /**
     * @param BankContract $bankService
     */
    public function getUniqueBanks(BankContract $bankService)
    {
        return response($bankService->getModel()->groupBy('bank_name')->get(), 200);
    }

    /**
     * @param BankContract $bankService
     */
    public function getPaymentProviderSettingsUniqueBank(Request $request, PaymentSettingContract $paymentSettingService)
    {
        $this->setClient($request);

        return response($paymentSettingService->getAllUniqueBanks($this->getClient()->id), 200);
    }

    /**
     * @param BankContract $bankService
     * @param PaymentSettingContract $paymentSettingService
     * @param $id
     *
     * @return mixed
     */
    public function getBankPaymentSettings(Request $request,
        BankContract $bankService,
        PaymentSettingContract $paymentSettingService,
        $id) {

        $this->setClient($request);
        $id = walle_decrypt($id);
        $payment_settings = $paymentSettingService->findPaymentSettings($id, $this->getClient()->id);

        return response($paymentSettingService->getModel()
            ->with('bank', 'payment_provider')
            ->whereHas('bank', function ($query) use ($payment_settings) {
                $query->where('bank_name', $payment_settings->bank->bank_name);
            })
            ->where('client_id', $this->getClient()->id)
            ->groupBy('payment_provider_id')
            ->orderBy('priority')
            ->get(), 200);
    }

    /**
     * Get available payment provider for bank
     *
     * @param  Request                $request               [description]
     * @param  PaymentSettingContract $paymentSettingService [description]
     * @param  BankContract           $bankService           [description]
     * @param  [type]                 $id                    [description]
     * @return [type]                                        [description]
     */
    public function getBankPaymentProviders(Request $request,
        PaymentSettingContract $paymentSettingService,
        BankContract $bankService,
        $id) {

        $id = walle_decrypt($id);

        $bank = $bankService->getModel()->findOrFail($id);

        return response(
            $bankService->getBankAvailablePaymentProvidersByName(
                $bank->bank_name
            , 200)
        );
    }

    /**
     * Prioritize payment provider
     *
     * @param Request $request
     * @return int
     */
    public function prioritize(Request $request)
    {
        $this->setClient($request);
        $payment_providers = $request->get('payment_providers');

        DB::transaction(function () use ($payment_providers) {
            foreach ($payment_providers as $key => $payment_provider) {
                PaymentSetting::where([
                    'id' => walle_decrypt($payment_provider),
                    'client_id' => $this->getClient()->id,
                ])->update(['priority' => ($key + 1)]);
            }
        });

        return response([
            'status' => 'Success',
        ], 200);
    }
}
