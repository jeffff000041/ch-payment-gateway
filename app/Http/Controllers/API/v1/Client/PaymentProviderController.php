<?php

namespace App\Http\Controllers\API\v1\Client;

use App\FieldCategory;
use App\PaymentProvider;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\PaymentProviderContract;

class PaymentProviderController extends Controller
{
    /**
     * @param PaymentProviderContract $paymentProviderService
     * @param $id
     * @return mixed
     */
    public function getFields(PaymentProviderContract $paymentProviderService, $id)
    {
        $id = walle_decrypt($id);
        return (array) $paymentProviderService->getPaymentProviderSettings(
            $id,
            FieldCategory::where('field_category_name', 'credentials')->first()->id
        );
    }
}
