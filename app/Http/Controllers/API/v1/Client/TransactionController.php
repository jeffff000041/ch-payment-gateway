<?php

namespace App\Http\Controllers\API\v1\Client;

use App\Repositories\Services\TransactionService;
use DB;
use App\Currency;
use App\Transaction;
use App\TransactionTrail;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\DataTables\TransactionDataTable;
use App\Modules\Projects\ProjectHookFactory;
use App\Repositories\Contracts\BankContract;
use App\Repositories\Services\ClientService;
use App\Http\Requests\API\TransactionRequest;
use App\Http\Requests\API\UpdateTransactionRequest;
use App\Repositories\Contracts\TransactionContract;
use App\Repositories\Contracts\PaymentSettingContract;
use App\Repositories\Contracts\TransactionTrailContract;
use App\Modules\PaymentProvider\Factory as PaymentProviderFactory;
use App\Repositories\Services\TransactionTrailService;
use Ixudra\Curl\Facades\Curl;

class TransactionController extends Controller
{
    /**
     * [index description]
     * @param  TransactionDataTable $transactionDataTable [description]
     * @return [type]                                     [description]
     */
    public function index(Request $request,
                          TransactionDataTable $transactionDataTable)
    {
        $this->setClient($request);

        return $transactionDataTable->render($this->getClient());
    }

    /**
     * Get available banks
     *
     * @param Request $request
     * @param PaymentSettingContract $paymentSettingService
     * @param $currency_id
     *
     * @return mixed
     */
    public function getAvailableBanks(Request $request,
                                      PaymentSettingContract $paymentSettingService,
                                      $currency_id)
    {

        $this->setClient($request);

        $currency_id = walle_decrypt($currency_id);
        $client_id = $this->getClient()->id;

        return $paymentSettingService->getAvailableUniqueBanksPerClient($currency_id, $client_id);
    }

    /**
     * Save transaction
     *
     * @param TransactionRequest $request
     * @param TransactionContract $transactionService
     * @param TransactionTrailContract $transactionTrailService
     * @param PaymentSettingContract $paymentSettingService
     * @return mixed
     */
    public function store(TransactionRequest $request,
                          TransactionContract $transactionService,
                          TransactionTrailContract $transactionTrailService,
                          PaymentSettingContract $paymentSettingService,
                          ProjectHookFactory $projectHookFactory)
    {
        // var_dump($projectHookFactory->getClass()->adminNotification(['data'=>'test']));exit;

        $this->setClient($request);
        $client = $this->getClient();

        $currency_id = walle_decrypt($request->get('currency'));
        $currency = Currency::findOrFail($currency_id);

        //encapsulated as bank id.
        $payment_settings_id = walle_decrypt($request->get('bank_id'));

        #Do not use findOrFail because of the groupBy function to get unique bank api
        $payment_setting = $paymentSettingService
            ->getModel()
            // ->whereIsActive(PaymentSetting::ACTIVE)
            ->whereHas('payment_provider.currencies', function ($query) use ($currency_id) {
                $query->where('currency_id', $currency_id);
            })
            ->find($payment_settings_id);

        if (!$payment_setting) {
            return response()->json([
                'message' => Transaction::PAYMENT_SETTING_CONTACT_SUPPORT_MSG,
            ], 404);
        }

        //get priority payment settings
        //backup
        // $priority_payment_provider = $paymentSettingService
        //     ->getPriorityPaymentSetting($payment_setting, $client->id);
        $priority_payment_provider = $paymentSettingService
            ->getPriorityPaymentSetting($payment_setting, $transactionService, $request->amount);

        if (!$priority_payment_provider) {
            return response()->json([
                'message' => Transaction::PAYMENT_SETTING_CONTACT_SUPPORT_MSG,
            ], 404);
        }

        // get the fields of deposit(2)/withdraw(1)
        $transaction_type = $request->get('type') == 'deposit' ? 2 : 3;
        $payment_provider = $paymentSettingService
            ->getPaymentProviderSettings($priority_payment_provider->id, $transaction_type);

        $paymentProviderConnection = PaymentProviderFactory::make($payment_provider->code);

        //$paymentProviderConnection->catchReturnURL();

        //check API status
        $api_status = $paymentProviderConnection->preSave($request, $payment_provider);
        if (!$api_status) {
            return response()->json([
                'message' => Transaction::PAYMENT_SETTING_NOT_AVAILABLE_MSG,
            ], 422);
        }

        $transaction = DB::transaction(function () use (
            $request,
            $transactionService,
            $transactionTrailService,
            $payment_provider,
            $client
        ) {
            //saving to database - transactions
            $transaction = $transactionService->storeTransaction(walle_decrypt($payment_provider->hash_id), $request, $client);

            $transactionTrailService->storeTransactionTrail($transaction, TransactionTrail::EXEC_TYPE_REQUEST, TransactionTrail::PROCESSING, null);

            return $transaction;
        });

        sleep(1);

        //generate form and will return url for redirection
        $form = $paymentProviderConnection->postSave($transaction, $payment_provider, $request, $currency);


        if (!$form['redirect']) {
            if ($request->type == 'deposit') {
                $transaction['payment_provider_code'] = $payment_provider->code;
                if ($form['success']) {
                    $data = json_encode([
                        'redirect' => $form['redirect'],
                        'transaction_data' => $transaction,
                        'status' => isset($form['transaction_status']) ? $form['transaction_status'] : '',
//                        'post_data' => isset($form['post_data']) ? $form['post_data'] : '',
                        'payment_provider_response' => isset($form['response_data']) ? $form['response_data'] : '',
                    ]);

                    $transaction['status'] = isset($form['transaction_status']) ? $form['transaction_status'] : '';
                    $transactionTrailService->storeTransactionTrail($transaction, TransactionTrail::EXEC_TYPE_RESPONSE, TransactionTrail::CONFIRMED, $data);
                    $projectHookFactory->getClass()->adminNotification($transaction);
                }

            } else if ($request->type == 'withdraw') {
                $transaction['status'] = 'CONFIRMED'; // only for demo, it must be base on PP response
                // NOTE: this is for demo only, you should comment this on production
                // this will create a transaction_trails data with the status of 'CONFIRMED'
                $this->paymentProviderResponse($transaction, $form, $transactionTrailService);

            }
        }

        if ($form['redirect']) {
            $transaction['payment_provider_code'] = $payment_provider->code;
            $data = json_encode([
                'redirect' => $form['redirect'],
                'transaction_data' => $transaction,
                'status' => isset($form['transaction_status']) ? $form['transaction_status'] : '',
                'payment_provider_response' => isset($form['response_data']) ? $form['response_data'] : '',
            ]);

            $projectHookFactory->getClass()->adminNotification($transaction);

            return $data;
        }

        $transaction['status'] = isset($form['transaction_status']) ? $form['transaction_status'] : '';
        $transaction['payment_provider_response'] = isset($form['response_data']) ? $form['response_data'] : '[]';

        return $transaction;
    }

    /**
     * Get available payment provider for bank
     *
     * @param  Request $request [description]
     * @param  PaymentSettingContract $paymentSettingService [description]
     * @param  BankContract $bankService [description]
     * @param  [type]                 $id                    [description]
     * @return [type]                                        [description]
     */
    public function getBankPaymentProviders(Request $request,
                                            PaymentSettingContract $paymentSettingService,
                                            BankContract $bankService,
                                            $id)
    {

        $id = walle_decrypt($id);
        $payment_settings = $paymentSettingService->findPaymentSettings($id);

        return response(
            $bankService->getBankAvailablePaymentProvidersByName(
                $payment_settings->bank->bank_name
            )
        );
    }

    /**
     * @param UpdateTransactionRequest $request
     */
    public function approvalStatus(Request $request, $id)
    {
        $this->validate($request, [
            'approval_status' => [
                'required',
                Rule::in([Transaction::FOR_REVIEW, Transaction::CONFIRMED]),
            ],
            'is_cancelled' => ['required'],
        ]);

        $id = walle_decrypt($id);

        $transaction = Transaction::findOrFail($id);

        $transaction->update(
            [
                'approval_status' => $request->get('approval_status'),
                'is_cancelled' => (string)$request->get('is_cancelled'),
            ]
        );

        return response($transaction, 200);
    }

    // TODO: CUrl the your response to Project notification URL; dummy data for payment provider confirmation
    public function paymentProviderResponse($transaction, $form, $transactionTrailService)
    {
        $data = json_encode([
            'redirect' => $form['redirect'],
            'transaction_data' => $transaction,
            'status' => isset($form['transaction_status']) ? $form['transaction_status'] : '',
//            'post_data' => isset($form['post_data']) ? $form['post_data'] : '',
            'payment_provider_response' => isset($form['response_data']) ? $form['response_data'] : 'ASTROPAY dummy receiver confirmation',
        ]);

        $transactionTrailService->storeTransactionTrail($transaction, TransactionTrail::EXEC_TYPE_RESPONSE, TransactionTrail::CONFIRMED, $data);

        $notification_url = "emma_url";
        $data_array = array();

        /**
         * TODO: send to project notification URL
         *
         * $response = Curl::to($notification_url)
         * ->withData(
         *      $transaction
         * )
         * ->post();
         */
    }

    /**
     * TODO: this function will catch the notification from Payment Provider
     * @param UpdateTransactionRequest $request
     */
    public function updateTransactionByProvider(TransactionTrailContract $transactionTrailService, Request $request, ClientService $clientService, TransactionService $transactionService)
    {
        //TODO: catch the return of PP and add to transaction_trails and post to project's notification URL
        $confirmReceived = '';

        //JBP
        if (isset($request->mownecum_order_num)) {
            $paymentProviderConnection = PaymentProviderFactory::make('jbp');
            $confirmReceived = $paymentProviderConnection->catchReturnURL($request);
        }

        //RPN
        else{
            $paymentProviderConnection = PaymentProviderFactory::make('rpn');
            $confirmReceived = $paymentProviderConnection->catchReturnURL($request);
        }

        $transaction_id = $confirmReceived['transaction']->id;
        $transaction = $transactionService->getTransactionByID($transaction_id);

        $client = $clientService->getClientData($transaction[0]->client_id);

        DB::transaction(function () use (
            $request,
            $transactionTrailService,
            $confirmReceived
        ) {
            $transactionTrailService->storeTransactionTrail($confirmReceived['transaction'], TransactionTrail::EXEC_TYPE_RESPONSE, $confirmReceived['transaction_status'], $confirmReceived['data_json']);
        });

        /*
         * This code will update the non-OGPS client
         */
        /*
        $data = array(
            'payment_provider_response' => $transaction,
            'redirect' => true,
            'status' =>  $confirmReceived['transaction_status'],
            'transaction_data' => $transaction,
        );

        //post to project's notification URL
        $response = Curl::to($client[0]['postback_url'])
            ->withData($data)
            ->post();

        \Log::info('---NOTIFICATION HOOK START----');
        \Log::info("Send Transaction Update to Client's Notification URL");
        \Log::info($response);
        \Log::info('---NOTIFICATION HOOK END----');
        */

        return $confirmReceived['PP_expected_return'];
    }
}

