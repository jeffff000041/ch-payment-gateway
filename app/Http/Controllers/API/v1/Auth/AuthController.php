<?php
namespace App\Http\Controllers\API\v1\Auth;

use App\User;
use App\Audit;
use Carbon\Carbon;
use App\OneTimePasswords;
use Illuminate\Http\Request;
use App\Http\Requests\AuthRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\API\LoginKeyRequest;
use App\Notifications\LoginKeyNotification;
use App\Repositories\Services\AuthTokenService;
use App\Repositories\Services\OtpWhitelistService;

class AuthController extends Controller
{
    /**
     * @param AuthRequest $request
     */
    public function register(AuthRequest $request)
    {
        $user = new User([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        $user->save();
        // $role = Role::create(['name' => 'super_admin']);
        // $permission = Permission::create(['name' => 'edit_users']);
        // $role->givePermissionTo($permission);
        //
        $user->assignRole('super_admin');
        return response()->json([
            'message' => 'Successfully created user!',
        ], 201);
    }

    /**
     * @param AuthRequest $request
     */

    public function createLoginKey(AuthRequest $request, AuthTokenService $authTokenService)
    {
        $email = [
            'email' => $request->get('username'),
            'password' => $request->get('password'),
        ];

        $username = [
            'username' => $request->get('username'),
            'password' => $request->get('password'),
        ];

        if (!Auth::attempt($email) && !Auth::attempt($username)) {
            return response([
                'errors' => [
                    'password' => 'Unauthorized: Invalid Credentials',
                ],
            ], 401);
        }

        $response = $authTokenService->recaptchaValidation($request, User::CAPTCHA_TYPE_CHECKBOX);

        if (!$response["success"]) {
            return response([
                'errors' => [
                    'recaptcha' => 'Invalid captcha. Please try again.',
                ],
            ], 422);
        }

        $user = $request->user();

        if ($user->is_active == User::DISABLED) {
            return response()->json([
                'errors' => [
                    'password' => 'Unauthorized: Account is locked',
                ],
            ], 401);
        }

        $loginKeyCreate = OneTimePasswords::updateOrCreate(
            ['user_id' => $user->id],
            [
                'user_id' => $user->id,
                'key' => generate_login_key(),
                'reference' => generate_reference_key(),
            ]
        );

        if ($loginKeyCreate) {
            $user->notify(new LoginKeyNotification($user, $loginKeyCreate));
            $loginKeyCreate->email = obfuscate_email($user->email);
            return response([
                'message' => 'Login key generated.',
                'data' => $loginKeyCreate,
            ], 200);
        }

        return response([
            'message' => 'Unable to create login key. Try again.',
        ], 422);
    }

    /**
     * @param AuthRequest $request
     */

    public function resendLoginKey(Request $request)
    {
        $hasLoginKey = OneTimePasswords::where([
            ['reference', $request->reference_key],
        ])->first();

        if ($hasLoginKey) {
            $user = User::where('id', $hasLoginKey->user_id)->first();

            $loginKeyCreate = OneTimePasswords::updateOrCreate(
                ['user_id' => $user->id],
                [
                    'user_id' => $user->id,
                    'key' => generate_login_key(),
                    'reference' => generate_reference_key(),
                ]
            );

            if ($loginKeyCreate) {
                $user->notify(new LoginKeyNotification($user, $loginKeyCreate));
                $loginKeyCreate->email = obfuscate_email($user->email);
                return response([
                    'message' => 'Login key generated.',
                    'data' => $loginKeyCreate,
                ], 200);
            }
        }

        return response([
            'message' => 'Unable to create login key. Try again.',
        ], 422);
    }

    /**
     * @param Request $request
     */

    public function getToken(LoginKeyRequest $request, OtpWhitelistService $otpWhitelistService,
        AuthTokenService $authTokenService) {
        if (!$request->get('login_key')) {
            \Log::info('REQUEST IP : ' . $request->ip());
            $isWhitelisted = $otpWhitelistService->getUserRequest($request);
            if ($isWhitelisted) {
                return $isWhitelisted;
            }
        }

        $response = $authTokenService->recaptchaValidation($request, User::CAPTCHA_TYPE_INVISIBLE);

        if (!$response["success"]) {
            return response([
                'errors' => [
                    'recaptcha' => 'Invalid captcha. Please try again.',
                ],
            ], 422);
        }

        $loginKeyCreate = OneTimePasswords::where([
            ['key', $request->login_key],
        ])->first();

        if (!$loginKeyCreate) {
            return response([
                'errors' => [
                    'login_key' => 'Login key is invalid.',
                ],
            ], 422);
        }

        $user = User::where('id', $loginKeyCreate->user_id)->first();

        if (!$user) {
            return response([
                'message' => 'User not found.',
            ], 404);
        }

        if ($user->is_active == User::DISABLED) {
            return response()->json([
                'message' => 'Unauthorized: Account is locked',
            ], 401);
        }

        if (Carbon::parse($loginKeyCreate->updated_at)->addMinutes(
            OneTimePasswords::RESET_TOKEN_TIME)->isPast()) {
            // $loginKeyCreate->delete();
            return response([
                'errors' => [
                    'login_key' => 'Login key is expired.',
                ],
            ], 404);
        }

        return $authTokenService->createUserToken($user, $request);
    }

    /**
     * @param Request $request
     */
    public function login(Request $request)
    {
        $user = response()->json($request->user());
        if ($user) {
            $this->audit_user($request, "login", "App\Login");
            return response()->json([
                'message' => 'Successfully logged in.',
            ], 200);
        }
    }

    /**
     * @param Request $request
     */
    public function revokeToken(Request $request)
    {
        $this->audit_user($request, "logout", "App\Logout");
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out.',
        ]);
    }

    /**
     * @param Request $request
     */
    public function getUser(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * @param Request $request
     */  
    public function audit_user($request, $event, $type, $old = null, $new = null)
    {
        date_default_timezone_set('Asia/Manila');
        $values = "";
        $audit_log = new Audit;
        if($event == 'logout') {
            $data =  Audit::where("event", "login")
                    ->where("old_values", "LIKE", "%".$request->user()->token()->id."%" )->first();
            $login_time = strtotime($data->created_at);
            $logout_time = time();
            $computed_time= round(abs($login_time - $logout_time) / 60, 2);
            $values = "{'duration': ".$computed_time."}";
            $audit_log->updated_at = $data->created_at;
        }

        $audit_log->user_id = auth()->id();
        $audit_log->user_type = "App\User";
        $audit_log->auditable_type = $type;
        $audit_log->auditable_id = 3;
        $audit_log->event = $event;
        $audit_log->old_values = $old <> null ? json_encode($old) :"{'token': ".$request->user()->token()->id."}";
        $audit_log->new_values = $new <> null ? json_encode($new) : $values;
        $audit_log->ip_address = $request->ip();
        $audit_log->url = $request->route('module_name');

        $audit_log->save();
    }
}
