<?php

namespace App\Http\Controllers\API\v1;

use App\Bank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\BankContract;

class BankController extends Controller
{
    /**
     * Get all payment providers
     *
     * @param Request $request
     * @param $currency
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        return Bank::all();
    }
}
