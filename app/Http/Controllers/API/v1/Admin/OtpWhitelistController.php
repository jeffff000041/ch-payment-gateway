<?php

namespace App\Http\Controllers\API\v1\Admin;

use App\OtpWhitelist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\OtpWhitelistRequest;

class OtpWhitelistController extends Controller
{
    /**
     * get otp whitelists
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        
        $whitelist = OtpWhitelist::all();

        return response([
            'data' => $whitelist
        ], 200);
    }

    /**
     * create otp whitelist
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function create(OtpWhitelistRequest $request)
    {
        
        $whitelist = new OtpWhitelist($request->all());
        $whitelist->save();

        return response(['message' => 'Success'], 200);
    }

    /**
     * edit otp whitelist
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function edit(OtpWhitelistRequest $request, $id)
    {
        $whitelisted = OtpWhitelist::findOrFail($id);
        $whitelisted->update($request->toArray());

        return $whitelisted;
    }

    /**
     * delete otp whitelist
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function delete(Request $request, $id)
    {
        $whitelisted = OtpWhitelist::findOrFail($id);
        $whitelisted->delete();

        return response(['message' => 'Success']);
    }
}