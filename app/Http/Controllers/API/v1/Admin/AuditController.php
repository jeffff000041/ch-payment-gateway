<?php

namespace App\Http\Controllers\API\v1\Admin;

use App\Audit;
use Illuminate\Http\Request;
use App\DataTables\AuditDataTable;
use App\Repositories\Contracts\AuditContract;
use App\Http\Controllers\Controller;

class AuditController extends Controller
{
    /**
     * [index description]
     * @param  AuditDataTable $auditDataTable [description]
     * @return [type]                         [description]
     */
    public function index(AuditDataTable $auditDataTable)
    {
        $audits = $auditDataTable->render();

        return $audits;
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function exportAuditLogs(AuditContract $auditContract, 
        Request $request)
    {
        $response = $auditContract->exportAuditLogs($request);

        return response([
            'path' => $response
        ], 200);
    }
}
