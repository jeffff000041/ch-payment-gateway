<?php

namespace App\Http\Controllers\API\v1\Admin;

use App\AuditLog;
use App\BackOfficeNote;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\DataTables\PlatformLogDataTable;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use App\Http\Requests\API\TransactionRequest;
use App\Repositories\Contracts\TransactionContract;
use App\Http\Requests\API\UpdateTransactionRequest;


class PlatformController extends Controller
{
    /**
     * [index description]
     * @param  PlatformLogsDataTable $platformLogsDataTable [description]
     * @return [type]                                     [description]
     */
    public function index(PlatformLogDataTable $platformLogDataTable)
    {

        $platform_logs = $platformLogDataTable->render();
//
//        foreach ($transactions as $key => $transaction) {
//            $hash_note = BackOfficeNote::where(
//                'transaction_id', '=', $transaction->hash_id
//            );
//
//            $transaction->has_notes = count($hash_note->get()) > 0 ? 1 : 0;
//        }

        return $platform_logs;
    }

    /**
     * @param Request $request
     */
    public function notes(Request $request, $id)
    {
        $notes = BackOfficeNote::where('transaction_id', '=', $id)->with('user');

        return response($notes->get(), 200);
    }

    /**
     * @param UpdateTransactionRequest $request
     */
    public function note(Request $request, $id)
    {
        $this->validate($request, ['note' => 'required']);

        $notes = new BackOfficeNote(
            [
                'notes' => $request->get('note'),
                'transaction_id' => $id,
                'user_id' => $request->user()->id,
            ]
        );
        $notes->save();

        return response($notes, 200);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function approvalStatus(Request $request, $id)
    {
        $this->validate($request, [
            'approval_status' => [
                'required',
                Rule::in([Transaction::FOR_REVIEW, Transaction::CONFIRMED]),
            ],
            'is_cancelled' => ['required'],
        ]);

        $id = walle_decrypt($id);
        $transaction = Transaction::findOrFail($id);
        $transaction->update(
            [
                'approval_status' => $request->get('approval_status'),
                'is_cancelled' => (string) $request->get('is_cancelled'),
            ]
        );

        //jeff
        //$ClientObj = new Client::()->get();

        $client_url = $transaction->client->postback_url; // this must be from db

        $res = Curl::to($client_url)
            ->withData($transaction->toArray())
            ->returnResponseArray()
            ->post();

        \Log::info('---NOTIFICATION HOOK START----');
        \Log::info($transaction->toArray());
        \Log::info(json_encode($res));
        \Log::info('---NOTIFICATION HOOK END----');

        return response($transaction, 200);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function exportTransactionLogs(TransactionContract $transactionService,
                                          Request $request)
    {
        $response = $transactionService->exportTransactionLogs($request);

        return response([
            'path' => $response
        ], 200);
    }
}
