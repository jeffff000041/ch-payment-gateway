<?php

namespace App\Http\Controllers\API\v1\Admin;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Rules\UniqueCaseSensitive;
use App\Http\Controllers\Controller;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Http\Requests\API\PasswordResetRequest as PasswordResetValidation;
use App\PasswordReset;

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'username' => ['required', 'regex:' . config('ogps.username_regex'), 'between:6,50', new UniqueCaseSensitive],
        ]);

        if ($validatedData ) {

            $username = $request->get('username');

            $user = User::where('username', $username)->first();

            if (!$user) {
                return response([
                    'errors' => [
                        'username' => 'User not found.'
                    ],
                ], 404);
            }

            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'email' => $user->email, 
                    'token' => str_random(60), 
                    'username' => $user->username,
                ]
            );

            if ($passwordReset) {
                $user->notify(new PasswordResetRequest($user, $passwordReset->token));
            }
        }

        return response([
            'message' => 'Password reset email successfully sent to ' . 
                obfuscate_email($user->email) . '.'
        ], 200);
    }

    /**
     * Reset password
     *
     * @param  [string] password
     * @param  [string] confirm_password
     * @param  [string] token
     * @return [string] message
     */
    public function reset(PasswordResetValidation $request)
    {
        $passwordReset = PasswordReset::where([
            ['token', $request->token]
        ])->first();

        if (!$passwordReset) {
            return response([
                'message' => 'Password reset token is invalid.'
            ], 422);
        }

        $user = User::where('email', $passwordReset->email)->first();

        if (!$user) {
            return response([
                'message' => 'User not found.'
            ], 404);
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(
            PasswordReset::RESET_TOKEN_TIME)->isPast()) {
            $passwordReset->delete();
            return response([
                'message' => 'Password reset token is expired.'
            ], 404);
        }
            
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));

        return response([
            'message' => 'Password Successfully Reset.'
        ], 200);
    }
}
