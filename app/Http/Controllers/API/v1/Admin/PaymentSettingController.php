<?php

namespace App\Http\Controllers\API\v1\Admin;

use App\PaymentProvider;
use DB;
use Auth;
use App\Client;
use App\Transaction;
use App\FieldCategory;
use App\PaymentSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\PaymentSettingsDataTable;
use App\Repositories\Contracts\BankContract;
use App\Repositories\Contracts\ClientContract;
use App\Traits\PaymentSettingControllerTrait;
use App\Http\Requests\API\PaymentSettingRequest;
use App\Repositories\Contracts\PaymentSettingContract;
use App\Repositories\Contracts\PaymentProviderContract;

class PaymentSettingController extends Controller
{
    use PaymentSettingControllerTrait;

    public function __construct()
    {
        $this->setUser(Auth::guard('api')->user());
    }

    /**
     * Save payment provider.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function index(PaymentSettingsDataTable $paymentSettingsDataTable)
    {
        return $paymentSettingsDataTable->render();
    }

    /**
     * Save payment provider.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(PaymentSettingRequest $projectPaymentSettingRequest,
                          PaymentProviderContract $paymentProviderService,
                          PaymentSettingContract $paymentSettingService) {
        #already decrypted in App\Http\Middleware\PaymentSettingsMiddleware
        $payment_provider_id = $projectPaymentSettingRequest->get('payment_provider_id');
        $payment_provider_id = walle_decrypt($payment_provider_id);

        $client_id = $projectPaymentSettingRequest->client_id == null ? 3 : $projectPaymentSettingRequest->client_id;
        $this->validateConfiguration($paymentProviderService,
            $projectPaymentSettingRequest,
            $payment_provider_id);

        $payment_setting = $paymentSettingService->create($projectPaymentSettingRequest, $client_id, $this->getUser()->id);
        if ($payment_setting[0] === true) {
            $this->reOrderPriority($payment_setting['rsp'], $paymentSettingService);
            
            return ['validated'=> true, 'msg'=> "Inserted."];
        } else {
            return ['validated'=> false, 'msg'=> "Duplicate payment method and payment provider."];
        }
    
    }

    /**
     * Update Project's payment provider
     *
     * @param Request $request
     * @return int
     */
    public function update(PaymentSettingRequest $projectPaymentSettingRequest,
                           PaymentProviderContract $paymentProviderService,
                           PaymentSettingContract $paymentSettingService,
                           $id) {

        $id = walle_decrypt($id);

        $payment_setting = PaymentSetting::findOrFail($id);

        $this->validateConfiguration($paymentProviderService,
            $projectPaymentSettingRequest,
            $payment_setting->payment_provider->id);

        return $paymentSettingService->update($projectPaymentSettingRequest, $payment_setting, $this->getUser()->id);
    }

    /**
     * Update Project's payment provider
     *
     * @param Request $request
     * @return int
     */
    public function show(Request $request, PaymentSettingContract $paymentSettingService,
                         $id) {
        $id = walle_decrypt($id);
        return (array) $paymentSettingService->getPaymentProviderSettings(
            $id,
            FieldCategory::where('field_category_name', 'credentials')->first()->id
        );
    }

    /**
     * Soft Delete Project's payment provider
     *
     * @param Request $request
     * @return int
     */
    public function destroy(Request $request, PaymentSettingContract $paymentSettingService, $id)
    {
        $id = walle_decrypt($id);
        $payment_settings = $paymentSettingService->getAllWithAccumulatedTransactionAmount();
        $payment_setting =  $payment_settings->where(['id' => $id])->get()->first();
        if (!$payment_setting) {
            return response([
                'errors' => [
                    //'message' => 'Payment setting not found.'
                    'message' => Transaction::PAYMENT_SETTING_CONTACT_SUPPORT_MSG
                ]
            ], 422);
        }
        if ($payment_setting->accoumulated_transaction_amount > 0) {
            return response([
                'errors' => [
                    'message' => 'Payment setting cannot be deleted.'
                ]
            ], 422);
        }

        $deleted = PaymentSetting::destroy($id);

        if ($deleted) {
            $this->reOrderPriority($payment_setting, $paymentSettingService);
        }

        return $deleted;
    }

    /**
     * @param BankContract $bankService
     */
    public function getUniqueBanks(BankContract $bankService)
    {
        return response(
            $bankService->getModel()
                ->select('banks.*')
                ->leftJoin('payment_providers', 'banks.payment_provider_id', 'payment_providers.id')
                ->where('payment_providers.is_active', '=', '1')
                ->groupBy('banks.bank_name')
                ->get(),
            200);
    }

    /**
     * @param BankContract $bankService
     */
    public function getPaymentProviderSettingsUniqueBank(PaymentSettingContract $paymentSettingService)
    {
        return $paymentSettingService->getAllUniqueBanks();
    }

    /**
     * @param Request $request
     * @param BankContract $bankService
     * @param PaymentSettingContract $paymentSettingService
     * @param $id
     *
     * @return mixed
     */
    public function getBankPaymentSettings(Request $request,
                                           BankContract $bankService,
                                           PaymentSettingContract $paymentSettingService,
                                           $id) {
        $id = walle_decrypt($id);
        $payment_settings = $paymentSettingService->findPaymentSettings($id);

        $unique_payment_settings = $paymentSettingService->getModel()
            ->with('bank', 'payment_provider')
            ->whereHas('bank', function ($query) use ($payment_settings) {
                $query->where('bank_name', $payment_settings->bank->bank_name);
            })
            ->groupBy('payment_provider_id')
            ->orderBy('priority');

        //add super admin / admin project
        $unique_payment_settings = $paymentSettingService->filterPerProject($unique_payment_settings);

        return $unique_payment_settings->get();
    }

    /**
     * Get available payment provider for bank
     *
     * @param  Request                $request
     * @param  PaymentSettingContract $paymentSettingService
     * @param  BankContract           $bankService
     * @param  Int                    $id
     *
     * @return mixed
     */
    public function getBankPaymentProviders(Request $request,
                                            PaymentSettingContract $paymentSettingService,
                                            BankContract $bankService,
                                            $id) {

        $id = walle_decrypt($id);

        $bank = $bankService->getModel()->findOrFail($id);

        return response(
            $bankService->getBankAvailablePaymentProvidersByName(
                $bank->bank_name
            )
        );
    }

    /**
     * Prioritize payment provider
     *
     * @param Request $request
     * @return int
     */
    public function prioritize(Request $request,
                               PaymentSettingContract $paymentSettingService)
    {
        $this->setClient($request);
        $payment_settings = $request->get('payment_providers');
        $user = auth()->user();

        $paymentSettingService->sortPriority($payment_settings, $user->id);

        return response([
            'status' => 'Success',
        ], 200);
    }

    public function getClients(ClientContract $clientService)
    {
        return $clientService->getClientsGroupByProjects();
    }

    private function reOrderPriority($payment_setting,
                                     PaymentSettingContract $paymentSettingService)
    {
        $user = auth()->user();
        $payment_settings = $paymentSettingService->getAllWithAccumulatedTransactionAmount();
        $payment_settings = $payment_settings->where([
            'bank_id' => $payment_setting->bank_id
        ])
            ->orderBy('priority', 'asc')
            ->get()
            ->pluck('hash_id');
        
        $paymentSettingService->sortPriority($payment_settings, $user->id);
    }
}
