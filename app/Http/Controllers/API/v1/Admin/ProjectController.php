<?php

namespace App\Http\Controllers\API\v1\Admin;

use App\Project;
use Illuminate\Http\Request;
use App\DataTable\DataTableFacade;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\ProjectRequest;

class ProjectController extends Controller
{
    /**
     * Get all projects
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        return DataTableFacade::setSearchable([
            'project_name',
        ])->make(new Project, $request);
    }

    /**
     * @param Request $request
     */
    public function store(ProjectRequest $request)
    {
        $project = new Project;

        $project->fill($request->toArray());
        $project->created_by = $request->user()->id;
        $project->updated_by = $request->user()->id;
        $project->save();

        return $project;
    }

    /**
     * @param Request $request
     */
    public function show(Request $request, $id)
    {
        return Project::findOrFail(walle_decrypt($id));
    }

    /**
     * @param Request $request
     */
    public function update(ProjectRequest $request, $id)
    {
        $project = Project::findOrFail(walle_decrypt($id));
        $project->updated_by = $request->user()->id;
        $project->update($request->toArray());

        return $project;
    }

    /**
     * @param Request $request
     */
    public function destroy(Request $request, $id)
    {
        $project = Project::findOrFail(walle_decrypt($id));
        $project->delete();

        return response(['message' => 'Success']);
    }
}
