<?php

namespace App\Http\Controllers\API\v1\Admin;

use App\Currency;
use App\FieldCategory;
use App\PaymentProvider;
use App\PaymentProviderNote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\PaymentProviderDataTable;
use App\Repositories\Contracts\PaymentProviderContract;

class PaymentProviderController extends Controller
{
    /**
     * Get all payment providers
     *
     * @param Request $request
     * @param $currency
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        #check currency filter
        $currency = walle_decrypt($request->get('currency'));

        if ($currency) {
            try {
                $currencyObj = Currency::findOrFail($currency);
            } catch (\Exception $ex) {
                return response(['error' => 'CurrencyNotFound'], 404);
            }

            return response([
                'data' => PaymentProvider::whereHas('currencies', function ($query) use ($currency) {
                    $query->where('currency_id', $currency);
                })->get(),
            ]);
        }

        return PaymentProvider::all();
    }

    /**
     * @param PaymentProviderContract $paymentProviderService
     * @param $id
     * @return mixed
     */
    public function getFields(PaymentProviderContract $paymentProviderService, $id)
    {
        $id = walle_decrypt($id);
        return (array) $paymentProviderService->getPaymentProviderSettings(
            $id,
            FieldCategory::where('field_category_name', 'credentials')->first()->id
        );
    }

    public function getPaymentProviders(Request $request,
                                        PaymentProviderDataTable $paymentProviderDataTable)
    {
        $this->setClient($request);

        //return $paymentProviderDataTable->render($this->getClient());
        $payment_providers = $paymentProviderDataTable->render($this->getClient());

        foreach ($payment_providers as $key => $payment_provider) {
            $hash_note = PaymentProviderNote::where(
                'payment_provider_code', '=', $payment_provider->payment_provider_code
            );

            $payment_provider->has_notes = count($hash_note->get()) > 0 ? 1 : 0;
        }

        return $payment_providers;
    }

    public function updatePaymentProviderStatus(Request $request, PaymentProviderContract $paymentProviderService)
    {
        $paymentProviderService->createPaymentProviderBONotes($request);

        $paymentProviderService->disablePaymentProvider($request);

        return 200;
    }

    /**
     * @param Request $request
     */
    public function notes(Request $request, $code)
    {
        $notes = PaymentProviderNote::select('payment_provider_notes.*', 'users.username')
            ->leftJoin('users', function ($join) {
            $join->select(['users.username'])
                ->on('users.id', '=', 'payment_provider_notes.user_id');
        })
        ->where('payment_provider_notes.payment_provider_code', '=', $code)
        ->orderBy('payment_provider_notes.created_at', 'desc');

        return response($notes->get(), 200);
    }
}
