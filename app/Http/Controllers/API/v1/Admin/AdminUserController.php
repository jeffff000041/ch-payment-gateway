<?php

namespace App\Http\Controllers\API\v1\Admin;

use DB;
use App\User;
use Illuminate\Http\Request;
use App\DataTables\AdminDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\UserRequest;
use App\Http\Requests\API\UserPasswordRequest;

class AdminUserController extends Controller
{
    /**
     * Get all administrators
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function index(AdminDataTable $adminDataTable)
    {
        return $adminDataTable->render();
    }

    /**
     * @param Request $request
     */
    public function store(UserRequest $request)
    {
        return DB::transaction(function () use ($request) {

            $user = new User;

            $user->fill($request->toArray());
            $user->password = bcrypt($request->get('password'));
            $user->created_by = $request->user()->id;
            $user->updated_by = $request->user()->id;
            $user->project_id = auth()->user()->project_id;
            $user->save();

            $user->assignRole('admin');

            return $user;
        });
    }

    /**
     * @param Request $request
     */
    public function show(Request $request, $id)
    {
        return User::role('admin')->findOrFail(walle_decrypt($id));
    }

    /**
     * @param Request $request
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::role('admin')->findOrFail(walle_decrypt($id));
        $user->updated_by = $request->user()->id;
        $user->update($request->toArray());

        return $user;
    }

    /**
     * @param Request $request
     */
    public function updatePassword(UserPasswordRequest $request, $id)
    {
        $user = User::role('admin')->findOrFail(walle_decrypt($id));
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return $user;
    }

    /**
     * @param Request $request
     */
    public function destroy(Request $request, $id)
    {
        $user = User::role('admin')->findOrFail(walle_decrypt($id));
        $user->delete();

        return response(['message' => 'Success']);
    }
}
