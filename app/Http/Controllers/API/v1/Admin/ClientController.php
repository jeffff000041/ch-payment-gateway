<?php

namespace App\Http\Controllers\API\v1\Admin;

use App\User;
use App\Client;
use Illuminate\Http\Request;
use Laravel\Passport\Passport;
use App\Http\Controllers\Controller;
use App\DataTables\ClientDataTable;
use Laravel\Passport\ClientRepository;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use App\Http\Controllers\API\v1\Auth\AuthController;


class ClientController extends Controller
{
    /**
     * The client repository instance.
     *
     * @var \Laravel\Passport\ClientRepository
     */
    protected $clients;

    /**
     * The validation factory implementation.
     *
     * @var \Illuminate\Contracts\Validation\Factory
     */
    protected $validation;

    /**
     * Create a client controller instance.
     *
     * @param  \Laravel\Passport\ClientRepository  $clients
     * @param  \Illuminate\Contracts\Validation\Factory  $validation
     * @return void
     */
    public function __construct(ClientRepository $clients,
        ValidationFactory $validation) {
        $this->clients = $clients;
        $this->validation = $validation;
    }

    /**
     * Get the active client instances for the given user ID.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function index(ClientDataTable $clientDataTable)
    {
        return $clientDataTable->render();
    }

    /**
     * Save client
     * @param  ClientRepository  $clients
     * @param  ValidationFactory $validation
     * @param  Request           $request
     *
     * @return mixed
     */
    public function store(Request $request)
    {
        $this->validation->make($request->all(), $this->rules())->validate();
        $data = [
            'user_id' => $request->user()->getKey(),
            'name' => $request->name,
            'secret' => str_random(40),
            'redirect' => $request->redirect,
            'postback_url' => $request->postback_url,
            'notification_url' => $request->notification_url,
            'personal_access_client' => false,
            'password_client' => false,
            'revoked' => false,
        ];
        $this->client = Passport::client()->forceFill($data);

        $this->client->save();
        (new AuthController())->audit_user($request, "created", "App\Project", null, $data);
        return $this->client->makeVisible('secret');
    }

    /**
     * Update the given client.
     *
     * @param  ClientRepository  $clients
     * @param  ValidationFactory $validation
     * @param  Request           $request
     * @param  int               $id
     *
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $rules = $this->rules();
        $rules['name'] = 'required|max:50|unique:oauth_clients,name,' . $id;

        $this->validation->make($request->all(), $rules)->validate();

        $client = $this->clients->find($id);

        if (!$client) {
            return response('No client found', 404);
        }

        $old = Client::find($id);
        $new = [
            'name' => $request->name,
            'redirect' => $request->redirect,
            'postback_url' => $request->postback_url,
            'notification_url' => $request->notification_url,
        ];
        $client->forceFill($new)->save();
        (new AuthController())->audit_user($request, "updated", "App\Project", $old, $new);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function destroy(Request $request, $id)
    {
        $client = $this->clients->find($id);

        $this->clients->delete(
            $client
        );
        (new AuthController())->audit_user($request, "deleted", "App\Project", $client);
    }

    /**
     * Validate client
     *
     * @param  Request $request
     *
     * @return mixed
     */
    private function rules()
    {
        return [
            'name' => 'required|max:50|unique:oauth_clients',
            'redirect' => 'url',
            'postback_url' => 'required|url',
            'notification_url' => 'url',
        ];
    }
}
