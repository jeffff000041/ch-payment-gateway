<?php

namespace App\Http\Controllers\API\v1\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\AccountRequest;
use App\Http\Requests\API\AccountPasswordRequest;
use App\Http\Requests\API\UserPasswordRequest;

class AccountController extends Controller
{
    /**
     * Update user information
     *
     * @param AccountRequest $request
     *
     * @return mixed
     */
    public function update(AccountRequest $request)
    {
        $request->user()->update($request->toArray());

        if($request->get('password') && $request->get('old_password') && 
            $request->get('confirm_password')) {
            $request->user()->password = bcrypt($request->get('password'));
            $request->user()->save();

            return response(['message' => 'Success']);
        } 

        $request->user()->save();

        return response(['message' => 'Success']);
    }

    /**
     * Update user information
     *
     * @param AccountRequest $request
     *
     * @return mixed
     */
    public function resetPassword(AccountPasswordRequest $request)
    {
        $request->user()->password = bcrypt($request->get('password'));
        $request->user()->save();

        return response(['message' => 'Success']);
    }

    /**
     * Image Upload
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function imageUpload(Request $request)
    {
        if ($request->get('is_removed')) {
            $request->user()->profile_image = NULL;
            $request->user()->save();

            return response(['message' => 'Success'], 200);
        }

        if ($request->hasFile('file')) {
            $image      = $request->file('file');
            $fileName   = time() . '.' . $image->getClientOriginalExtension();

            $img = \Image::make($image->getRealPath());
            $img->resize(150, 150, function ($constraint) {
                $constraint->aspectRatio();                 
            });

            $img->stream();
            
            \Storage::disk('local')->put('public/images/dp/' .
                $request->user()->hash_id .'/'.$fileName, $img, 'public');

            $request->user()->profile_image = config('app.url') . '/storage/images/dp/' .
                $request->user()->hash_id . '/' . $fileName;
            $request->user()->save();
            
            return response(['message' => 'Success'], 200);
        }

        return response(['message' => 'Unable to process request.'], 422);
    }
}
