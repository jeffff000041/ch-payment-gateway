<?php

namespace App\Http\Middleware;

use Closure;
use App\Client;
use App\User;
use App\Traits\TokenParserTrait;

class PaymentSettingsMiddleware
{
    use TokenParserTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->setClient($request);

        if ($request->get('payment_provider_id')) {
            $request->request->add([
                'payment_provider_id' => walle_decrypt($request->get('payment_provider_id')),
            ]);
        }

        if ($request->get('bank_id')) {
            $request->request->add([
                'bank_id' => walle_decrypt($request->get('bank_id')),
            ]);
        }

        //add client
        if ($request->is('api/v1/client/*')) {
            $request->request->add([
                'client_id' => $this->getClient()->id,
            ]);
        } else if ($request->is('api/v1/admin/*')) {
            $client_id = $request->get('client_id');

            if(!$client_id){

                $admin = User::superAdmin()->get()->first();

                //get first client
                $first_client = Client::where('user_id', $admin->id)->first();
                
                if (!$first_client) {
                    return response('No Clients available', 422);
                }

                $client_id = $first_client->id;
            }

            $request->request->add([
                'client_id' => $client_id ? $client_id : Client::where('user_id', auth()->user()->id)->first()->id,
            ]);
        }

        return $next($request);
    }
}
