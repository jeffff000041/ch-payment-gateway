<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\Services\AuditLogService;

class PlatformLoggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $event)
    {
        $AuditLogService = new AuditLogService();
        $audit_log = $AuditLogService->create($request, $event);

        return $next($request);
    }
}

