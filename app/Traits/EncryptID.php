<?php

namespace App\Traits;

trait EncryptID
{
    public function __construct(array $attributes = [])
    {
        $this->addHidden(['id']);
        $this->append(['hash_id']);

        return parent::__construct($attributes);
    }

    /**
     * @param $value
     */
    public function getHashIdAttribute()
    {
        return walle_encrypt($this->getKey());
    }

    /**
     * @param $value
     */
    public function setHashIdAttribute()
    {
        return walle_decrypt($this->getKey());
    }
}
