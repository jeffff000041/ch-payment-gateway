<?php

namespace App\Traits;

use App\FieldCategory;
use Illuminate\Http\Request;
use App\Exceptions\PaymentProviderConfigurationException;

trait PaymentSettingControllerTrait
{
    /**
     * @var mixed
     */
    protected $user;

    /**
     * Get User
     *
     * @return mixed
     */
    public function getUser()
    {
        $user = new \stdClass();
        $user->id = $this->user->id;

        return $user;
    }

    /**
     * Set User
     *
     * @param $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param $paymentProviderService
     * @param $id
     */
    public function validateConfiguration($paymentProviderService, $request, $id)
    {
        $settings = $paymentProviderService->getPaymentProviderSettings(
            $id,
            FieldCategory::where('field_category_name', 'credentials')->first()->id
        )->configuration;

        $request_configurations = json_decode($request->configurations);
        $errors = [];

        foreach ($settings as $key => $setting) {
            foreach ($request_configurations as $key => $request_configuration) {
                if ($setting['hash_id'] == $request_configuration->id && empty($request_configuration->value)) {
                    array_push($errors, [
                        'id' => $setting['hash_id'],
                        'code' => $setting['code'],
                        'label' => $setting['label'],
                    ]);
                }
            }
        }
        
        if (!empty($errors)) {
            $configException = new PaymentProviderConfigurationException("Validation Error", 1);
            $configException->setErrors([
                'errors' => $errors,
            ]);

            throw $configException;
        }
    }
}
