<?php

namespace App\Traits;

use Lcobucci\JWT\Parser;
use Laravel\Passport\Token;
use App\Client;

trait TokenParserTrait
{
    /**
     * @var mixed
     */
    protected $client;

    /**
     * @param $request
     */
    public function setClient($request = null)
    {

        $request = $request ? $request : request();

        $bearerToken = $request->bearerToken();
        $tokenId = (new Parser())->parse($bearerToken)->getHeader('jti');
        $client = Token::find($tokenId)->client;

        $this->client = Client::with('user')->findOrFail(Token::find($tokenId)->client->id);
    }

    /**
     * @param $request
     */
    public function getClient()
    {
        return $this->client;
    }
}
