<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Contracts\TransactionContract;

class CheckExpiredTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:expire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check expired transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $transactionService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TransactionContract $transactionService)
    {
        $this->transactionService = $transactionService;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int Number of transaction that are expired
     */
    public function handle()
    {
        echo $this->transactionService->expireProcessingTransaction();
    }
}
