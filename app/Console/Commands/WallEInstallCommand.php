<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Laravel\Passport\ClientRepository;

class WallEInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'walle:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute migration and other dependency';

    /**
     * Execute the console command.
     *
     * @return int Number of transaction that are expired
     */
    public function handle(ClientRepository $clients)
    {
        $confirmation = $this->ask(
            'The following command will execute.'.PHP_EOL.
            ' 1. php artisan migrate:refresh --seed'.PHP_EOL.
            ' 2. passport:install'.PHP_EOL.
            ' 3. client:default'.PHP_EOL.
            ' 4. storage:link'.PHP_EOL.
            ' Do you wish to continue? (y/N)'
        );

        if (strtolower($confirmation) === 'y') {
            
            $this->info(PHP_EOL.'Migrating...'.PHP_EOL);
            $this->call('migrate:refresh', ['--seed' => true]);

            $this->info(PHP_EOL.'Installing Passport...'.PHP_EOL);
            $this->call('passport:install');

            $this->info(PHP_EOL.'Creating Default client...'.PHP_EOL);
            $this->call('client:default');

            $this->info(PHP_EOL.'Executing symlink...'.PHP_EOL);
            $this->call('storage:link');

            return true;
        }

        $this->info('Installation has been cancelled');

        return true;
    }
}
