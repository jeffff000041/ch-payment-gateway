<?php

namespace App\Console\Commands;

use App\User;
use Laravel\Passport\Passport;
use Illuminate\Console\Command;
use Laravel\Passport\ClientRepository;

class ClientDefaultCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'client:default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add client default';

    /**
     * Execute the console command.
     *
     * @return int Number of transaction that are expired
     */
    public function handle(ClientRepository $clients)
    {
        $users = User::projectUser();

        foreach ($users as $key => $user) {
            $this->client = Passport::client()->forceFill([
                'user_id' => $user->id,
                'name' => 'DEFAULT_'.$user->full_name,
                'secret' => str_random(40),
                'redirect' => '',
                'postback_url' => '',
                'notification_url' => '',
                'personal_access_client' => false,
                'password_client' => false,
                'revoked' => false,
                'is_default' => 1,
            ]);

            $this->client->save();

            $this->info('Default client created.');
            $this->line('<comment>User ID:</comment> '.$user->id);
            $this->line('<comment>Client Name:</comment> '.$user->full_name);
        }
    }
}
