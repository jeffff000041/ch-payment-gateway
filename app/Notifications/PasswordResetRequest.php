<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordResetRequest extends Notification
{
    use Queueable;

    protected $userData;
    protected $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($userData, $token)
    {
        $this->userData = $userData;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = ogps_url('password/reset') . '?token=' . $this->token; 

        return (new MailMessage)
                    ->greeting('Hi ' . $this->userData->username . ',')
                    ->line('We got a request to reset your OGPS Password.')
                    ->action('Reset Password Link', $url)
                    ->line('If you ignore this message, your password won’t be changed.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
