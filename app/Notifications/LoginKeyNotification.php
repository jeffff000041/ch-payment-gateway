<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class LoginKeyNotification extends Notification
{
    use Queueable;

    protected $userData;
    protected $otpData;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($userData, $otpData)
    {
        $this->userData = $userData;
        $this->otpData = $otpData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hi ' . $this->userData->username . ',')
                    ->line('Good Day.')
                    ->line('Your Reference Number is ' . $this->otpData->reference)
                    ->line('Your Login Key for accessing OGPS is ' . $this->otpData->key);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
