<?php

namespace App;

use App\PaymentProvider;
use App\Traits\EncryptID;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Bank extends Model implements Auditable
{
    use SoftDeletes, EncryptID, \OwenIt\Auditing\Auditable;
    
    /**
     * @var array
     */
    protected $fillable = [
        'bank_name',
        'bank_code',
    ];

    protected $hidden = [
        'payment_provider_id',
    ];

    protected $appends = [
        'hash_payment_provider_id',
    ];

    //
    /**
     * @return mixed
     */
    public function payment_provider()
    {
        return $this->belongsTo(PaymentProvider::class);
    }

    /**
     * @param $value
     */
    public function getHashPaymentProviderIdAttribute()
    {
        return walle_encrypt($this->payment_provider_id);
    }

    /**
     * @param $value
     */
    public function setHashPaymentProviderIdAttribute()
    {
        return walle_decrypt($this->payment_provider_id);
    }
}
