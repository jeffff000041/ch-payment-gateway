<?php

namespace App;

use App\Traits\EncryptID;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class FieldCategoryPaymentProviderField extends Model implements Auditable
{
    use EncryptID, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'payment_provider_field_id',
        'field_category_id',
    ];

    protected $hidden = [
        'payment_provider_field_id',
        'category_id',
    ];

    protected $appends = [
        'hash_payment_provider_field_id',
        'hash_category_id',
    ];
    
    /**
     * @return mixed
     */
    public function field_category()
    {
        return $this->belongsTo(FieldCategory::class);
    }

    /**
     * @return mixed
     */
    public function payment_provider_field()
    {
        return $this->belongsTo(PaymentProviderField::class);
    }

    /**
     * @param $value
     */
    public function getHashPaymentProviderFieldIdAttribute()
    {
        return walle_encrypt($this->payment_provider_field_id);
    }

    /**
     * @param $value
     */
    public function setHashPaymentProviderFieldIdAttribute()
    {
        return walle_decrypt($this->payment_provider_field_id);
    }

    /**
     * @param $value
     */
    public function getHashCategoryIdAttribute()
    {
        return walle_encrypt($this->category_id);
    }

    /**
     * @param $value
     */
    public function setHashCategoryIdAttribute()
    {
        return walle_decrypt($this->category_id);
    }
}
