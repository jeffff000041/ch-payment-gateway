<?php

namespace App;

use App\Client;
use App\Traits\EncryptID;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentSetting extends Model implements Auditable
{
    use SoftDeletes, EncryptID, \OwenIt\Auditing\Auditable;

    const ACTIVE = '1'; //string because of enum

    const INACTIVE = '0'; //string because of enum

    const TYPE_DEPOSIT = 'DEPOSIT'; //string because of enum

    const TYPE_WITHDRAW = 'WITHDRAW'; //string because of enum

    const STATUS_CONFIRMED = 'CONFIRMED';

    const STATUS_PROCESSING = 'PROCESSING';

    const STATUS_CANCELLED = 'CANCELLED';

    const NOT_AUTO_DISABLED = '0'; //string because of enum

    const AUTO_DISABLED = '1'; //string because of enum

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $fillable = [
        'payment_provider_id',
        'bank_id',
        'client_id',
        'priority',
        'created_by',
        'updated_by',
        'is_active',
        'minimum_amount',
        'maximum_amount',
        'auto_disabled',
        'start_enabled_date',
        'percentage_to_notify',
        'type',
        'override_accumulated_amount',

    ];

    /**
     * @var array
     */
    protected $hidden = [
//        'payment_provider_id',
        'bank_id',
        'created_by',
        'updated_by',
        'hash_payment_provider_id',
        'hash_bank_id',
        'auto_disabled',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'hash_payment_provider_id',
        'hash_bank_id',
    ];

    /**
     * @param $value
     */
    public function getHashPaymentProviderIdAttribute()
    {
        return walle_encrypt($this->payment_provider_id);
    }

    /**
     * @param $value
     */
    public function setHashPaymentProviderIdAttribute()
    {
        return walle_decrypt($this->payment_provider_id);
    }

    /**
     * @param $value
     */
    public function getHashBankIdAttribute()
    {
        return walle_encrypt($this->payment_provider_id);
    }

    /**
     * @param $value
     */
    public function setHashBankIdAttribute()
    {
        return walle_decrypt($this->payment_provider_id);
    }

    /**
     * @return mixed
     */
    public function payment_provider()
    {
        return $this->belongsTo(PaymentProvider::class);
    }

    /**
     * @return mixed
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    /**
     * @return mixed
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * @return mixed
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * @return mixed
     */
    public function payment_setting_configs()
    {
        return $this->belongsToMany(PaymentProviderField::class, 'payment_setting_configs');
    }

    /**
     * @return mixed
     */
    public function payment_setting_config_value()
    {
        return $this->hasMany(PaymentSettingConfig::class);
    }
}
