<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    /**
     * 
     * @var [type]
     */
    protected $fillable = [
        'email', 
        'token',
        'username',
    ];

    /**
     * minutes
     */
    const RESET_TOKEN_TIME = 5;
}
