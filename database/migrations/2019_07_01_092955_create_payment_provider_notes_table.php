<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentProviderNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_provider_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_provider_id');
            $table->string('payment_provider_code');
            $table->text('notes')->nullable()->comment('Back office notes')->collation('utf8mb4_unicode_ci');
            $table->integer('user_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_provider_notes');
    }
}
