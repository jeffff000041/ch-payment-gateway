<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->collation('utf8mb4_unicode_ci');
            $table->string('last_name')->collation('utf8mb4_unicode_ci');
            $table->string('email')->unique()->collation('utf8mb4_unicode_ci');
            $table->string('username')->unique()->collation('utf8mb4_unicode_ci');
            $table->string('password')->collation('utf8mb4_unicode_ci');
            $table->boolean('is_notifiable')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('project_id')->nullable();
            $table->enum('is_active', [0, 1])->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
