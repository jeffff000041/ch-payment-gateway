<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentSettingConfigs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_setting_configs', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('payment_setting_id')->unsigned()->index('pps_id_index');
            $table->integer('payment_provider_field_id')->unsigned()->index('ppf_index');

            $table->foreign('payment_setting_id','ppps_foreign')->references('id')->on('payment_settings');
            $table->foreign('payment_provider_field_id','ppf_foreign')->references('id')->on('payment_provider_fields');

            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->string('value')->nullable()->collation('utf8mb4_unicode_ci');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_setting_configs');
    }
}
