<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_setting_id')->unsigned()->index();
            $table->foreign('payment_setting_id')->references('id')->on('payment_settings');
            $table->integer('currency_id')->unsigned()->index();
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->float('amount', 17, 4);
            $table->string('transaction_number')->nullable();
            $table->integer('client_id')->unsigned()->index();
            $table->foreign('client_id')->references('id')->on('oauth_clients');
            // $table->integer('ogps_client_identifier')->nullable()->comment('This field is intended for ogps only!');
            $table->string('reference_number')->nullable()->comment('Client transaction number');
            $table->enum('type', ['WITHDRAW', 'DEPOSIT'])->comment('WITHDRAW - When transaction is for withdrawal. DEPOSIT - When transaction is deposit');
            $table->text('notes')->nullable()->comment('Client note')->collation('utf8mb4_unicode_ci');
            $table->enum('approval_status', ['','FOR REVIEW','CONFIRMED','PROCESSING','FAILED'])
                ->comment('Admin approval status')
                ->default('');
            $table->enum('is_cancelled', [0, 1])->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
