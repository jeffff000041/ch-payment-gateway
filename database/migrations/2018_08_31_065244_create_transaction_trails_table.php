<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTrailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_trails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned()->index();
            $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->enum('status', ['PROCESSING', 'CANCELLED', 'CONFIRMED', 'EXPIRED', 'FAILED'])
                ->comment('PROCESSING - when the transaction is pending. CANCELLED - when transaction is cancelled. CONFIRMED - When transaction is confirmed or done. EXPIRED - When processing transaction is expired');
            $table->enum('execution_type', ['REQUEST', 'RESPONSE'])->comment('REQUEST - When transaction is requested by the system. RESPONSE - When transaction is response of the third party app.');
            $table->text('data_json')->nullable()->comment('Request or Response data')->collation('utf8mb4_unicode_ci');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_trails');
    }
}
