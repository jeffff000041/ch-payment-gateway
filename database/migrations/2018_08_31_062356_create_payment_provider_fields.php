<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentProviderFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_provider_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_provider_id')->unsigned()->index();
            
            $table->foreign('payment_provider_id')->references('id')->on('payment_providers');

            $table->string('payment_providers_field_code')->collation('utf8mb4_unicode_ci');;
            $table->string('label')->collation('utf8mb4_unicode_ci');;
            $table->boolean('is_visible')->default(1);
            $table->string('data_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_provider_fields');
    }
}
