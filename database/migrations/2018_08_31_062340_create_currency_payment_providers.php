<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyPaymentProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_payment_provider', function (Blueprint $table) {

            $table->integer('payment_provider_id')->unsigned()->index();
            $table->integer('currency_id')->unsigned()->index();

            $table->foreign('payment_provider_id')->references('id')->on('payment_providers');
            $table->foreign('currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency_payment_provider');
    }
}
