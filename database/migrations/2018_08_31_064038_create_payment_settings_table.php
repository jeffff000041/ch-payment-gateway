<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_provider_id')->unsigned()->index();
            $table->foreign('payment_provider_id')->references('id')->on('payment_providers');
            $table->integer('bank_id')->unsigned()->index();
            $table->foreign('bank_id')->references('id')->on('banks');
            $table->integer('client_id')->unsigned()->index();
            $table->foreign('client_id')->references('id')->on('oauth_clients');
            $table->tinyInteger('percentage_to_notify')->default(100);
            $table->tinyInteger('priority')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->enum('is_active', [0, 1])->default(1);
            $table->float('minimum_amount', 17, 4)->default(1.0000);
            $table->float('maximum_amount', 17, 4)->default(1.0000);
            $table->timestamp('start_enabled_date')->nullable();
            $table->enum('auto_disabled', [0, 1])->default(0);
            $table->enum('type', ['DEPOSIT', 'WITHDRAW'])->default('DEPOSIT');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_settings');
    }
}
