<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldCategoryPaymentProviderFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_category_payment_provider_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_provider_field_id')->unsigned()->index('ppf_index');
            $table->foreign('payment_provider_field_id', 'pps_foreign')->references('id')->on('payment_provider_fields');
            $table->integer('field_category_id')->unsigned()->index('fc_index');
            $table->foreign('field_category_id', 'fc_foreign')->references('id')->on('field_categories');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_category_payment_provider_fields');
    }
}
