<?php

use App\User;
use App\Project;
use Carbon\Carbon;
use App\UserDetail;
use App\PaymentSetting;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        
        $payment_provider_id = 1;
        $bank_id = 1;

        $ps = (new PaymentSetting([
                'payment_provider_id' => $payment_provider_id,
                'bank_id' => $bank_id,
                'maximum_amount' => 500,
                'created_by' => 1,
                'updated_by' => 1,
                'priority' => 1,
                'start_enabled_date' => Carbon::now(),
            ]));

        $ps->save();

        $payment_provider_id = 1;
        $bank_id = 2;

        $ps = (new PaymentSetting([
                'payment_provider_id' => $payment_provider_id,
                'bank_id' => $bank_id,
                'created_by' => 1,
                'maximum_amount' => 500,
                'updated_by' => 1,
                'priority' => 2,
                'start_enabled_date' => Carbon::now(),
            ]));

        $ps->save();

        $payment_provider_id = 2;
        $bank_id = 11;

        $ps = (new PaymentSetting([
                'payment_provider_id' => $payment_provider_id,
                'bank_id' => $bank_id,
                'maximum_amount' => 500,
                'created_by' => 1,
                'updated_by' => 1,
                'priority' => 3,
                'start_enabled_date' => Carbon::now(),
            ]));

        $ps->save();

        $payment_provider_id = 2;
        $bank_id = 1;

        $ps = (new PaymentSetting([
                'payment_provider_id' => $payment_provider_id,
                'bank_id' => $bank_id,
                'maximum_amount' => 500,
                'created_by' => 1,
                'updated_by' => 1,
                'priority' => 4,
                'start_enabled_date' => Carbon::now(),
            ]));

        $ps->save();
       
    }
}
