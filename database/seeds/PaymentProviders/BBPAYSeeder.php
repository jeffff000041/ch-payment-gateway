<?php

//namespace Database\Seeds\PaymentProviders;

use App\Bank;
use App\Currency;
use App\FieldCategory;
use App\PaymentProvider;
use App\PaymentProviderField;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\FieldCategoryPaymentProviderField;


class BBPAYSeeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;

    /**
     * @var mixed
     */
    protected $payment_provider;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            #Payment Provider
            $this->storePaymentProvider();

            #Currency
            $this->storeCurrency();

            #Payment Provider Fields
            $this->storeField();

            #Banks
            $this->storeBank();
        });
    }

    /**
     * @return mixed
     */
    protected function storePaymentProvider()
    {
        $payment_provider = new PaymentProvider;

        $payment_provider->payment_provider_name = 'BBPAY';
        $payment_provider->payment_provider_code = 'bbpay';
        $payment_provider->created_by = self::ADMINISTRATOR_ID;
        $payment_provider->updated_by = self::ADMINISTRATOR_ID;

        $payment_provider->save();

        $this->setPaymentProvider($payment_provider);
    }

    protected function storeCurrency()
    {
        $payment_provider = $this->getPaymentProvider();

        $payment_provider->currencies()
            ->attach([
                Currency::whereCurrencyCode('RMB')->first()->id,
            ]);
    }

    protected function storeField()
    {
        $payment_provider = $this->getPaymentProvider();

        #mechant ID
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant Code',
                'payment_providers_field_code' => 'MerCode',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('withdraw')->first()->id,
            ],
        ]);

        #merchant key
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant Key',
                'payment_providers_field_code' => 'Key',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('withdraw')->first()->id,
            ],
        ]);

        #Amount
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Amount',
                'payment_providers_field_code' => 'Amount',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();
    }

    protected function storeBank()
    {
        $payment_provider = $this->getPaymentProvider();

        Bank::insert(
            [
                #BBPAY
                'bank_code' => 'bbpay',
                'bank_name' => 'BBPay',
                'created_by' => self::ADMINISTRATOR_ID,
                'updated_by' => self::ADMINISTRATOR_ID,
                'payment_provider_id' => $payment_provider->id,

            ]);
    }

    public function setPaymentProvider($payment_provider)
    {
        $this->payment_provider = $payment_provider;
    }

    public function getPaymentProvider()
    {
        return $this->payment_provider;
    }
}
