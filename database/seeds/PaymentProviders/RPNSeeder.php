<?php

//namespace Database\Seeds\PaymentProviders;

use App\Bank;
use App\Currency;
use App\FieldCategory;
use App\PaymentProvider;
use App\PaymentProviderField;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\FieldCategoryPaymentProviderField;

class RPNSeeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;

    /**
     * @var mixed
     */
    protected $payment_provider;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            #Payment Provider
            $this->storePaymentProvider();

            #Currency
            $this->storeCurrency();

            #Payment Provider Fields
            $this->storeField();

            #Banks
            $this->storeBank();
        });
    }

    /**
     * @return mixed
     */
    protected function storePaymentProvider()
    {
        $payment_provider = new PaymentProvider;

        $payment_provider->payment_provider_name = 'RPN';
        $payment_provider->payment_provider_code = 'rpn';
        $payment_provider->created_by = self::ADMINISTRATOR_ID;
        $payment_provider->updated_by = self::ADMINISTRATOR_ID;

        $payment_provider->save();

        $this->setPaymentProvider($payment_provider);
    }

    protected function storeCurrency()
    {
        $payment_provider = $this->getPaymentProvider();

        $payment_provider->currencies()
            ->attach(
                Currency::whereCurrencyCode('RMB')->first()->id
            );
    }

    protected function storeField()
    {
        $payment_provider = $this->getPaymentProvider();

        #version
        $payment_provider_field = new PaymentProviderField(
                [
                    'payment_provider_id' => $payment_provider->id,
                    'label' => 'Version',
                    'payment_providers_field_code' => 'version',
                    'is_visible' => 0,
                    'data_type' => 'text',
                ]);
        
       	$payment_provider_field->save();
        
        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #sign type
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Sign Type',
                'payment_providers_field_code' => 'sign_type',
                'is_visible' => 0,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #merchant ID
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant ID',
                'payment_providers_field_code' => 'mid',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
        ]);

        #return URL
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Return URL',
                'payment_providers_field_code' => 'return_url',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Notify URL
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Notify URL',
                'payment_providers_field_code' => 'notify_url',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Order ID
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Order ID',
                'payment_providers_field_code' => 'order_id',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Order Amount
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Order Amount',
                'payment_providers_field_code' => 'order_amount',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Order Time
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Order Time',
                'payment_providers_field_code' => 'order_time',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Bank ID
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Bank ID',
                'payment_providers_field_code' => 'bank_id',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Signature
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Signature',
                'payment_providers_field_code' => 'signature',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Key
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Key',
                'payment_providers_field_code' => 'key',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
        ]);

    }

    protected function storeBank()
    {
        $payment_provider = $this->getPaymentProvider();

        Bank::insert(
            [
                #RPN
                [
                    'bank_code' => '1',
                    'bank_name' => 'Industrial And Commercial Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '2',
                    'bank_name' => 'Agriculture Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '3',
                    'bank_name' => 'Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '4',
                    'bank_name' => 'China Construction Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '5',
                    'bank_name' => 'Bank Of Communications',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '6',
                    'bank_name' => 'China Everbright Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '7',
                    'bank_name' => 'Shanghai Pudong Development Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '8',
                    'bank_name' => 'Bank Of Beijing',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '9',
                    'bank_name' => 'China Guangfa Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '10',
                    'bank_name' => 'Ping An Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '11',
                    'bank_name' => 'Industrial Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '12',
                    'bank_name' => 'China Merchants Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '13',
                    'bank_name' => 'Shenzhen Development Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '14',
                    'bank_name' => 'Postal Savings Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '15',
                    'bank_name' => 'Huaxia Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '16',
                    'bank_name' => 'China Minsheng Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
            ]);
    }

    public function setPaymentProvider($payment_provider)
    {
    	$this->payment_provider = $payment_provider;
    }

    public function getPaymentProvider()
    {
    	return $this->payment_provider;
    }
}
