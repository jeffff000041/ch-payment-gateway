<?php

namespace Database\Seeds\PaymentProviders;

use DB;
use App\Bank;
use App\Currency;
use App\FieldCategory;
use App\PaymentProvider;
use App\PaymentProviderField;
use App\FieldCategoryPaymentProviderField;
use Illuminate\Database\Seeder;

class LUUPSeeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;

    /**
     * @var mixed
     */
    protected $payment_provider;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            #Payment Provider
            $this->storePaymentProvider();

            #Currency
            $this->storeCurrency();

            #Payment Provider Fields
            $this->storeField();

            #Banks
            $this->storeBank();
        });
    }

    /**
     * @return mixed
     */
    protected function storePaymentProvider()
    {
        $payment_provider = new PaymentProvider;

        $payment_provider->payment_provider_name = 'LUUP';
        $payment_provider->payment_provider_code = 'luup';
        $payment_provider->created_by = self::ADMINISTRATOR_ID;
        $payment_provider->updated_by = self::ADMINISTRATOR_ID;

        $payment_provider->save();

        $this->setPaymentProvider($payment_provider);
    }

    protected function storeCurrency()
    {
        $payment_provider = $this->getPaymentProvider();

        $payment_provider->currencies()
            ->attach(
                Currency::whereCurrencyCode('RMB')->first()->id
            );
    }

    protected function storeField()
    {
        $payment_provider = $this->getPaymentProvider();

        #Type
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Type',
                'payment_providers_field_code' => 'type',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Amount
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Amount',
                'payment_providers_field_code' => 'amount',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Currency
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Currency',
                'payment_providers_field_code' => 'currency',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Order ID
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Order ID',
                'payment_providers_field_code' => 'orderId',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);


        #firstName
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'First Name',
                'payment_providers_field_code' => 'firstName',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);


        #lastName
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Last Name',
                'payment_providers_field_code' => 'lastName',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #email
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Email',
                'payment_providers_field_code' => 'email',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #phone
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Phone',
                'payment_providers_field_code' => 'phone',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #country
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Country',
                'payment_providers_field_code' => 'country',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #city
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'City',
                'payment_providers_field_code' => 'city',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #address
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Address',
                'payment_providers_field_code' => 'address',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #returnUrl
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Return Url',
                'payment_providers_field_code' => 'returnUrl',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #notifyUrl
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Notify Url',
                'payment_providers_field_code' => 'notifyUrl',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);
    }

    protected function storeBank()
    {
        $payment_provider = $this->getPaymentProvider();

        Bank::insert(
            [
                #LUUP
                [
                    'bank_code' => 'ABC',
                    'bank_name' => 'Agriculture Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => 'BCN',
                    'bank_name' => 'Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => 'BOC',
                    'bank_name' => 'Bank Of Communications',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => 'CCB',
                    'bank_name' => 'China Construction Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => 'CGB',
                    'bank_name' => 'China Guangfa Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => 'IBN',
                    'bank_name' => 'Industrial Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => 'CMB',
                    'bank_name' => 'China Merchants Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => 'CMBC',
                    'bank_name' => 'China Minsheng Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => 'ICBN',
                    'bank_name' => 'Industrial And Commercial Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => 'PSBC',
                    'bank_name' => 'Postal Savings Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],

            ]);
    }

    public function setPaymentProvider($payment_provider)
    {
        $this->payment_provider = $payment_provider;
    }

    public function getPaymentProvider()
    {
        return $this->payment_provider;
    }
}
