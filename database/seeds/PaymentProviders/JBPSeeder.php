<?php

//namespace Database\Seeds\PaymentProviders;

use App\Bank;
use App\Currency;
use App\FieldCategory;
use App\PaymentProvider;
use App\PaymentProviderField;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\FieldCategoryPaymentProviderField;

class JBPSeeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;

    /**
     * @var mixed
     */
    protected $payment_provider;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            #Payment Provider
            $this->storePaymentProvider();

            #Currency
            $this->storeCurrency();

            #Payment Provider Fields
            $this->storeField();

            #Banks
            $this->storeBank();
        });
    }

    /**
     * @return mixed
     */
    protected function storePaymentProvider()
    {
        $payment_provider = new PaymentProvider;

        $payment_provider->payment_provider_name = 'JBP';
        $payment_provider->payment_provider_code = 'jbp';
        $payment_provider->created_by = self::ADMINISTRATOR_ID;
        $payment_provider->updated_by = self::ADMINISTRATOR_ID;

        $payment_provider->save();

        $this->setPaymentProvider($payment_provider);
    }

    protected function storeCurrency()
    {
        $payment_provider = $this->getPaymentProvider();

        $payment_provider->currencies()
            ->attach([
                Currency::whereCurrencyCode('RMB')->first()->id,
//                Currency::whereCurrencyCode('HKD')->first()->id
            ]);
    }

    protected function storeField()
    {
        $payment_provider = $this->getPaymentProvider();

        #Merchant ID:
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant ID',
                'payment_providers_field_code' => 'company_id',
                'is_visible' => 0,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
        ]);

        #bank ID:
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Bank ID',
                'payment_providers_field_code' => 'bank_id',
                'is_visible' => 0,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #amount
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Amount',
                'payment_providers_field_code' => 'amount',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ]
        ]);

        #company order number
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Company Order Number',
                'payment_providers_field_code' => 'company_order_num',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #company user
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Company User',
                'payment_providers_field_code' => 'company_user',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #estimated payment bank
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Estimated Payment Bank',
                'payment_providers_field_code' => 'estimated_payment_bank',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #deposit mode
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Deposit Mode',
                'payment_providers_field_code' => 'deposit_mode',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #group_id
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Group ID',
                'payment_providers_field_code' => 'group_id',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #web url
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Web Url',
                'payment_providers_field_code' => 'web_url',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #memo
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Memo',
                'payment_providers_field_code' => 'memo',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #note
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Note',
                'payment_providers_field_code' => 'note',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #note_model
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Note Model',
                'payment_providers_field_code' => 'note_model',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #key
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Key',
                'payment_providers_field_code' => 'key',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #terminal
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Terminal',
                'payment_providers_field_code' => 'terminal',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);


        #Merchant key
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant Key',
                'payment_providers_field_code' => 'config',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
        ]);

    }

    protected function storeBank()
    {
        $payment_provider = $this->getPaymentProvider();

        Bank::insert(
            [
                #DF
                [
                    'bank_code' => '1',
                    'bank_name' => 'Industrial And Commercial Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '4',
                    'bank_name' => 'Agriculture Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '5',
                    'bank_name' => 'Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '3',
                    'bank_name' => 'China Construction Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '6',
                    'bank_name' => 'Bank Of Communications',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '11',
                    'bank_name' => 'China Everbright Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '9',
                    'bank_name' => 'Shanghai Pudong Development Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '13',
                    'bank_name' => 'China Guangfa Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '8',
                    'bank_name' => 'China CITIC Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '14',
                    'bank_name' => 'HSBC Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '12',
                    'bank_name' => 'Ping An Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '15',
                    'bank_name' => 'Industrial Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '2',
                    'bank_name' => 'China Merchants Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '7',
                    'bank_name' => 'China Minsheng Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '10',
                    'bank_name' => 'Postal Savings Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '40',
                    'bank_name' => 'WeChat',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '52',
                    'bank_name' => 'QQ',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '30',
                    'bank_name' => 'Alipay',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '42',
                    'bank_name' => 'Jingdong Pay',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '51',
                    'bank_name' => 'UNIONPAY',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],

            ]);
    }

    public function setPaymentProvider($payment_provider)
    {
        $this->payment_provider = $payment_provider;
    }

    public function getPaymentProvider()
    {
        return $this->payment_provider;
    }
}
