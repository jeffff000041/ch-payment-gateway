<?php

use App\Bank;
use App\Currency;
use App\FieldCategory;
use App\PaymentProvider;
use App\PaymentProviderField;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\FieldCategoryPaymentProviderField;

class JUHUIZFSeeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;

    /**
     * @var mixed
     */
    protected $payment_provider;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            #Payment Provider
            $this->storePaymentProvider();

            #Currency
            $this->storeCurrency();

            #Payment Provider Fields
            $this->storeField();

            #Banks
            $this->storeBank();
        });
    }

    /**
     * @return mixed
     */
    protected function storePaymentProvider()
    {
        $payment_provider = new PaymentProvider;

        $payment_provider->payment_provider_name = 'JUHUIZF';
        $payment_provider->payment_provider_code = 'juhuizf';
        $payment_provider->created_by = self::ADMINISTRATOR_ID;
        $payment_provider->updated_by = self::ADMINISTRATOR_ID;

        $payment_provider->save();

        $this->setPaymentProvider($payment_provider);
    }

    protected function storeCurrency()
    {
        $payment_provider = $this->getPaymentProvider();

        $payment_provider->currencies()
            ->attach([
                Currency::whereCurrencyCode('RMB')->first()->id,
//                Currency::whereCurrencyCode('HKD')->first()->id
            ]);
    }

    protected function storeField()
    {
        $payment_provider = $this->getPaymentProvider();

        #Parter:
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant ID',
                'payment_providers_field_code' => 'parter',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
        ]);

        #Merchant key
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant Key',
                'payment_providers_field_code' => 'merchant_key',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
        ]);

        #bank type:
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Bank ID',
                'payment_providers_field_code' => 'type',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #amount
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Amount',
                'payment_providers_field_code' => 'value',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ]
        ]);

        #Merchant order number
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant Order Number',
                'payment_providers_field_code' => 'orderid',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #callbackurl
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Callback URL',
                'payment_providers_field_code' => 'callbackurl',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #hrefbackurl
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'hrefback url',
                'payment_providers_field_code' => 'hrefbackurl',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Payer IP
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Payer IP',
                'payment_providers_field_code' => 'payerIp',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Attach
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Attach',
                'payment_providers_field_code' => 'attach',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #key
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Key',
                'payment_providers_field_code' => 'key',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

    }

    protected function storeBank()
    {
        $payment_provider = $this->getPaymentProvider();

        Bank::insert(
            [
                [
                    'bank_code' => '967',
                    'bank_name' => config('banks.en.ICBC'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '964',
                    'bank_name' => config('banks.en.ABC'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '963',
                    'bank_name' => config('banks.en.BC'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '965',
                    'bank_name' => config('banks.en.CCB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '981',
                    'bank_name' => config('banks.en.BOC'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '986',
                    'bank_name' => config('banks.en.CEB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '977',
                    'bank_name' => config('banks.en.SPDB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '962',
                    'bank_name' => config('banks.en.CITIC'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '978',
                    'bank_name' => config('banks.en.PB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '972',
                    'bank_name' => config('banks.en.IB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '970',
                    'bank_name' => config('banks.en.CMEB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '980',
                    'bank_name' => config('banks.en.CMIB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '971',
                    'bank_name' => config('banks.en.PSBC'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '1004',
                    'bank_name' => config('banks.en.Wechat'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '1008',
                    'bank_name' => config('banks.en.QQ'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '992',
                    'bank_name' => config('banks.en.ALI'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '966',
                    'bank_name' => config('banks.en.ICBCM'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '968',
                    'bank_name' => config('banks.en.CZB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '969',
                    'bank_name' => config('banks.en.ZCCB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '973',
                    'bank_name' => config('banks.en.SRCC'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '974',
                    'bank_name' => config('banks.en.SDB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '975',
                    'bank_name' => config('banks.en.BOS'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '976',
                    'bank_name' => config('banks.en.SRCB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '979',
                    'bank_name' => config('banks.en.BON'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '980',
                    'bank_name' => config('banks.en.CMIB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '982',
                    'bank_name' => config('banks.en.HUB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '983',
                    'bank_name' => config('banks.en.HZB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '984',
                    'bank_name' => config('banks.en.GCB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '985',
                    'bank_name' => config('banks.en.GDB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '987',
                    'bank_name' => config('banks.en.BOEA'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '988',
                    'bank_name' => config('banks.en.CBB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '989',
                    'bank_name' => config('banks.en.BJBANK'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '990',
                    'bank_name' => config('banks.en.BRCB'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '1006',
                    'bank_name' => config('banks.en.AM'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
                [
                    'bank_code' => '1007',
                    'bank_name' => config('banks.en.MW'),
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],

            ]);
    }

    public function setPaymentProvider($payment_provider)
    {
        $this->payment_provider = $payment_provider;
    }

    public function getPaymentProvider()
    {
        return $this->payment_provider;
    }
}
