<?php

//namespace Database\Seeds\PaymentProviders;

use App\Bank;
use App\Currency;
use App\FieldCategory;
use App\PaymentProvider;
use App\PaymentProviderField;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\FieldCategoryPaymentProviderField;

class EC365V2Seeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;

    /**
     * @var mixed
     */
    protected $payment_provider;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function () {

            #Payment Provider
            $this->storePaymentProvider();

            #Currency
            $this->storeCurrency();

            #Payment Provider Fields
            $this->storeField();

            #Banks
            $this->storeBank();
        });
    }

    /**
     * @return mixed
     */
    protected function storePaymentProvider()
    {
        $payment_provider = new PaymentProvider;

        $payment_provider->payment_provider_name = 'EC365';
        $payment_provider->payment_provider_code = 'ec365v2';
        $payment_provider->created_by = self::ADMINISTRATOR_ID;
        $payment_provider->updated_by = self::ADMINISTRATOR_ID;

        $payment_provider->save();

        $this->setPaymentProvider($payment_provider);
    }

    protected function storeCurrency()
    {
        $payment_provider = $this->getPaymentProvider();

        $payment_provider->currencies()
            ->attach([
                Currency::whereCurrencyCode('RMB')->first()->id,
                Currency::whereCurrencyCode('HKD')->first()->id
            ]);
    }

    protected function storeField()
    {
        $payment_provider = $this->getPaymentProvider();

        #version
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Version',
                'payment_providers_field_code' => 'version',
                'is_visible' => 0,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Order Serial Number
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Order Serial Number',
                'payment_providers_field_code' => 'ordersn',
                'is_visible' => 0,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #merchant number
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant Number',
                'payment_providers_field_code' => 'merchno',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
        ]);

        #merchant order ID
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Merchant Order ID',
                'payment_providers_field_code' => 'merchOrderid',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Transaction Type
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'transaction Type',
                'payment_providers_field_code' => 'transtype',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Pay Type
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Pay Type',
                'payment_providers_field_code' => 'paytype',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Bank ID
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Bank ID',
                'payment_providers_field_code' => 'bankid',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Card Type
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Card Type',
                'payment_providers_field_code' => 'cardtype',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Order Name
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Order Name',
                'payment_providers_field_code' => 'orderName',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Order Desc
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Order Desc',
                'payment_providers_field_code' => 'orderDesc',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Amount
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Amount',
                'payment_providers_field_code' => 'amount',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Currency
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Currency',
                'payment_providers_field_code' => 'currency',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #ip
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'IP',
                'payment_providers_field_code' => 'ip',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Return Url
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Return Url',
                'payment_providers_field_code' => 'returnUrl',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Notify Url
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Notify Url',
                'payment_providers_field_code' => 'notifyUrl',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Remark
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Remark',
                'payment_providers_field_code' => 'remark',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Signature Type
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'Signature Type',
                'payment_providers_field_code' => 'signType',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #Sign
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'API Key',
                'payment_providers_field_code' => 'sign',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
        ]);

        #API Key
        $payment_provider_field = new PaymentProviderField(
            [
                'payment_provider_id' => $payment_provider->id,
                'label' => 'API Key',
                'payment_providers_field_code' => 'api_key',
                'is_visible' => 1,
                'data_type' => 'text',
            ]);

        $payment_provider_field->save();

        FieldCategoryPaymentProviderField::insert([
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('deposit')->first()->id,
            ],
            [
                'payment_provider_field_id' => $payment_provider_field->id,
                'field_category_id' => FieldCategory::whereFieldCategoryName('credentials')->first()->id,
            ],
        ]);

    }

    protected function storeBank()
    {
        $payment_provider = $this->getPaymentProvider();

        Bank::insert(
            [
                #DF
                [
                    'bank_code' => '666666666666',
                    'bank_name' => 'EC365',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => $payment_provider->id,
                ],
            ]);
    }

    public function setPaymentProvider($payment_provider)
    {
        $this->payment_provider = $payment_provider;
    }

    public function getPaymentProvider()
    {
        return $this->payment_provider;
    }
}
