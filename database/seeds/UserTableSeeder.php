<?php

use Carbon\Carbon;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();

        $user->first_name = 'Super';
        $user->last_name = 'User';
        $user->email = 'superuser@neworientalclub88.com';
        $user->username = 'superuser_neworientalclub88';
        $user->password = bcrypt('NOCP@ssw0rd123!');
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();

        $user->save();
        
        $user->assignRole('super_admin');


        $user = new User();

        $user->first_name = 'OGPS';
        $user->last_name = '';
        $user->email = 'ogps@neworientalclub88.com';
        $user->username = 'ogps';
        $user->project_id = 1; //OGPS
        $user->is_notifiable = 1;
        $user->password = bcrypt('NOCP@ssw0rd');
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();

        $user->save();
        
        $user->assignRole('super_admin');
    }
}
