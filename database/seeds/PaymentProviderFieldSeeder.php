<?php

use App\PaymentProvider;
use App\PaymentProviderField;
use Illuminate\Database\Seeder;

class PaymentProviderFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #RPN
        $rpn = PaymentProvider::find(2);

        $rpn->payment_provider_fields()->saveMany([
            new PaymentProviderField(
                [
                    'label' => 'Version(val:1.0)',
                    'payment_providers_field_code' => 'version',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Sign type(val:MD5)',
                    'payment_providers_field_code' => 'sign_type',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Merchant ID',
                    'payment_providers_field_code' => 'mid',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Return URL',
                    'payment_providers_field_code' => 'return_url',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Notify URL',
                    'payment_providers_field_code' => 'notify_url',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Order ID',
                    'payment_providers_field_code' => 'order_id',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Order Amount',
                    'payment_providers_field_code' => 'order_amount',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Order Time',
                    'payment_providers_field_code' => 'order_time',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Bank ID',
                    'payment_providers_field_code' => 'bank_id',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'signature',
                    'payment_providers_field_code' => 'signature',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Key',
                    'payment_providers_field_code' => 'key',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
        ]);

        #ASTROPAY
        $astropay = PaymentProvider::find(5);

        $astropay->payment_provider_fields()->saveMany([
            new PaymentProviderField(
                [
                    'label' => 'x_login',
                    'payment_providers_field_code' => 'x_login',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'x_trans_key',
                    'payment_providers_field_code' => 'x_trans_key',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField( // default value AUTH_CAPTURE
                [
                    'label' => 'x_type(val:AUTH_CAPTURE)',
                    'payment_providers_field_code' => 'x_type',
                    'is_visible' => 1, //default 0
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Card Number',
                    'payment_providers_field_code' => 'x_card_num',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Card Code(CVV)',
                    'payment_providers_field_code' => 'x_card_code',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Expiration Date',
                    'payment_providers_field_code' => 'x_exp_date',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Amount',
                    'payment_providers_field_code' => 'x_amount',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Currency',
                    'payment_providers_field_code' => 'x_currency',
                    'is_visible' => 1,
                    'data_type' => 'text',
                ]),
            new PaymentProviderField(
                [
                    'label' => 'Invoice Number',
                    'payment_providers_field_code' => 'x_invoice_num',
                    'is_visible' => 1, //default 0
                    'data_type' => 'text',
                ]),
        ]);
    }
}
