<?php

use App\FieldCategoryPaymentProviderField;
use Illuminate\Database\Seeder;

class FieldCategoryPaymentProviderFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #RPN
        #creds
        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 3;
        $fieldCategoryPaymentProviderField->field_category_id = 1;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 11;
        $fieldCategoryPaymentProviderField->field_category_id = 1;
        $fieldCategoryPaymentProviderField->save();

        #deposit
        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 1;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 2;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 3;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 4;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 5;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 6;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 7;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 8;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 9;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 11;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();


        #ASTROPAY
        #creds
        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 12;
        $fieldCategoryPaymentProviderField->field_category_id = 1;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 13;
        $fieldCategoryPaymentProviderField->field_category_id = 1;
        $fieldCategoryPaymentProviderField->save();

        #deposit
        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 12;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 13;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 14;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 15;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 16;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 17;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 18;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 19;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

        $fieldCategoryPaymentProviderField = new FieldCategoryPaymentProviderField();
        $fieldCategoryPaymentProviderField->payment_provider_field_id = 20;
        $fieldCategoryPaymentProviderField->field_category_id = 2;
        $fieldCategoryPaymentProviderField->save();

    }
}
