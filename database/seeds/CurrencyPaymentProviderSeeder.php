<?php

use App\PaymentProvider;
use Illuminate\Database\Seeder;

class CurrencyPaymentProviderSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #LUUP
        $luup = PaymentProvider::find(1);

        $luup->currencies()->attach(1);

        #RPNpay
        $rpn = PaymentProvider::find(2);

        $rpn->currencies()->attach(1);
        
        #GMSTONE
        // $gmstone = PaymentProvider::find(3);

        // $gmstone->currencies()->attach(1);

        #EC365
        // $ec365 = PaymentProvider::find(4);

        // $ec365->currencies()->attach(1);
    }
}
