<?php

use App\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{

	const ADMINISTRATOR_ID = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #rmb
        $rmb = new Currency;

        $rmb->currency_name = 'CHINA';
        $rmb->currency_code = 'RMB';
        $rmb->country_code = 'CH';
        $rmb->created_by = self::ADMINISTRATOR_ID;
        $rmb->updated_by = self::ADMINISTRATOR_ID;

        $rmb->save();
        
        #usd
        $usd = new Currency;

        $usd->currency_name = 'United States of America';
        $usd->currency_code = 'USD';
        $usd->country_code = 'US';
        $usd->created_by = self::ADMINISTRATOR_ID;
        $usd->updated_by = self::ADMINISTRATOR_ID;

        $usd->save();

        #php
        $php = new Currency;

        $php->currency_name = 'Philippines';
        $php->currency_code = 'PHP';
        $usd->country_code = 'PH';
        $php->created_by = self::ADMINISTRATOR_ID;
        $php->updated_by = self::ADMINISTRATOR_ID;

        $php->save();

        #HKD
        $hkd = new Currency;

        $hkd->currency_name = 'Hong Kong';
        $hkd->currency_code = 'HKD';
        $usd->country_code = 'HK';
        $hkd->created_by = self::ADMINISTRATOR_ID;
        $hkd->updated_by = self::ADMINISTRATOR_ID;

        $hkd->save();
    }
}
