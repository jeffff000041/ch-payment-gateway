<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserRolesAndPermissionsSeeder extends Seeder
{
   /**
   * Run the database seeds.
   *
   * @return void
   */

    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'edit_users']);
        Permission::create(['name' => 'delete_users']);
        Permission::create(['name' => 'create_users']);
        Permission::create(['name' => 'read_users']);

        // create roles and assign created permissions

        $role = Role::create(['name' => 'super_admin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(['create_users', 'edit_users', 'read_users']);

        $role = Role::create(['name' => 'user']);
        $role->givePermissionTo('read_users');
    }
}
