<?php

use Illuminate\Database\Seeder;
use App\FieldCategory;

class FieldCategorySeeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FieldCategory::insert(
            [
                [
                    'field_category_name' => 'credentials',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                ],
                [
                    'field_category_name' => 'deposit',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                ],
                [
                    'field_category_name' => 'withdraw',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                ],
                [
                    'field_category_name' => 'transaction_status',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                ]
            ]
        );
    }
}
