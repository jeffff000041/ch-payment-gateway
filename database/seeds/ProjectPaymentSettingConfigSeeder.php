<?php

use App\PaymentSettingConfig;
use Illuminate\Database\Seeder;

class PaymentSettingConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	#LUUP
        $projectPaymentSettingConfig = new PaymentSettingConfig();
        $projectPaymentSettingConfig->payment_setting_id = 1;
        $projectPaymentSettingConfig->payment_provider_field_id = 1; //login-luup
        $projectPaymentSettingConfig->created_by = 2;
        $projectPaymentSettingConfig->value = 'john doe';
        $projectPaymentSettingConfig->save();



        $projectPaymentSettingConfig = new PaymentSettingConfig();
        $projectPaymentSettingConfig->payment_setting_id = 1;
        $projectPaymentSettingConfig->payment_provider_field_id = 2; //password-luup
        $projectPaymentSettingConfig->created_by = 2;
        $projectPaymentSettingConfig->value = 'pass123';
        $projectPaymentSettingConfig->save();
    }
}
