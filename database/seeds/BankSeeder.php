<?php

use App\Bank;
use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bank::insert(
            [
                #LUUP
                [
                    'bank_code' => 'ABC',
                    'bank_name' => 'Agricultural Bank of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],
                [
                    'bank_code' => 'BCN',
                    'bank_name' => 'Bank of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],
                [
                    'bank_code' => 'BOC',
                    'bank_name' => 'Bank of Communications',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],
                [
                    'bank_code' => 'CCB',
                    'bank_name' => 'China Construction Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],
                [
                    'bank_code' => 'CGB',
                    'bank_name' => 'China Guangfa Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],
                [
                    'bank_code' => 'IBN',
                    'bank_name' => 'China Industrial Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],
                [
                    'bank_code' => 'CMB',
                    'bank_name' => 'China Merchants Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],
                [
                    'bank_code' => 'CMBC',
                    'bank_name' => 'China Minsheng Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],
                [
                    'bank_code' => 'ICBN',
                    'bank_name' => 'Industrial and Commercial Bank of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],
                [
                    'bank_code' => 'PSBC',
                    'bank_name' => 'Postal Savings Bank of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 1,
                ],

                #RPN
                [
                    'bank_code' => '1',
                    'bank_name' => 'Industrial And Commercial Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '2',
                    'bank_name' => 'Agriculture Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '3',
                    'bank_name' => 'Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '4',
                    'bank_name' => 'China Construction Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '5',
                    'bank_name' => 'Bank Of Communications',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '6',
                    'bank_name' => 'China Everbright Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '7',
                    'bank_name' => 'Shanghai Pudong Development Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '8',
                    'bank_name' => 'Bank Of Beijing',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '9',
                    'bank_name' => 'China Guangfa Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '10',
                    'bank_name' => 'Ping An Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '11',
                    'bank_name' => 'China Industrial Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '12',
                    'bank_name' => 'China Merchants Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '13',
                    'bank_name' => 'Shenzhen Development Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '14',
                    'bank_name' => 'Postal Savings Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '15',
                    'bank_name' => 'Huaxia Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],
                [
                    'bank_code' => '16',
                    'bank_name' => 'China Minsheng Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 2,
                ],

                #GMSTONE
                [
                    'bank_code' => 'ABC',
                    'bank_name' => 'Agricultural Bank of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'BJBANK',
                    'bank_name' => 'Bank Of Beijing',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],
                // exist
                [
                    'bank_code' => 'BOC',
                    'bank_name' => 'Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'CEB',
                    'bank_name' => 'China Everbright Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'CIB',
                    'bank_name' => 'Industrial Bank Co., LTD.',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'CITIC',
                    'bank_name' => 'China Citic Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],
                // exist
                [
                    'bank_code' => 'CMBC',
                    'bank_name' => 'China Minsheng Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'ICBC',
                    'bank_name' => 'Industrial And Commercial Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'SPABANK',
                    'bank_name' => 'Ping An Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],
                // exist
                [
                    'bank_code' => 'SPDB',
                    'bank_name' => 'SPD Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],
                // exist
                [
                    'bank_code' => 'PSBC',
                    'bank_name' => 'Postal Savings Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'NJCB',
                    'bank_name' => 'Bank Of Nanjing',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'COMM',
                    'bank_name' => 'Bank Of Communications',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],
                // exist
                [
                    'bank_code' => 'CMB',
                    'bank_name' => 'China Merchants Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],
                // exist
                [
                    'bank_code' => 'CCB',
                    'bank_name' => 'China Construction Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'GDB',
                    'bank_name' => 'China GUANGFA Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'HKBEA',
                    'bank_name' => 'Bank Of East Asia',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'alipay',
                    'bank_name' => 'Alipay',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                [
                    'bank_code' => 'wxcode',
                    'bank_name' => 'Wechatpay',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],
                [

                    'bank_code' => 'qqpay',
                    'bank_name' => 'qqpay',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 3,
                ],

                # EC365
                [
                    'bank_code' => '102100099996', 
                    'bank_name' => 'Industrial Commercial Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '103100000026', 
                    'bank_name' => 'Agricultural Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '104100000004', 
                    'bank_name' => 'Bank of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '105100000017', 
                    'bank_name' => 'China Construction Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '301290000007', 
                    'bank_name' => 'Bank of Communications',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '403100000004', 
                    'bank_name' => 'Postal Savings Bank Of China',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '306581000003', 
                    'bank_name' => 'Guangfa Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '305100000013', 
                    'bank_name' => 'China Minsheng Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '303100000006', 
                    'bank_name' => 'China Everbright Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '313100000013', 
                    'bank_name' => 'Bank Of Beijing',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '325290000012', 
                    'bank_name' => 'Bank Of Shanghai',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '302100011000', 
                    'bank_name' => 'China Citic Shanghai',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '310290000013', 
                    'bank_name' => 'Shanghai Pudong Development Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '304100040000', 
                    'bank_name' => 'HSBC Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '309391000011', 
                    'bank_name' => 'Industrial Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '308584000013', 
                    'bank_name' => 'China Merchants Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '307584007998', 
                    'bank_name' => 'Ping An Bank',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '666666666602', 
                    'bank_name' => 'WeChat',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
                [
                    'bank_code' => '666666666603', 
                    'bank_name' => 'QQ',
                    'created_by' => self::ADMINISTRATOR_ID,
                    'updated_by' => self::ADMINISTRATOR_ID,
                    'payment_provider_id' => 4,
                ],
            ]
        );
    }
}
