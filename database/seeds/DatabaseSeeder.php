<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        #Users and Roles
        $this->call(UserRolesAndPermissionsSeeder::class);
        $this->call(ProjectTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CurrencySeeder::class);
        
        #Field Category
        $this->call(FieldCategorySeeder::class);

        #Payment Providers
        $this->call(ASTROPAYSeeder::class);
        $this->call(RPNSeeder::class);
        $this->call(JBPSeeder::class);
        $this->call(EC365V2Seeder::class);
        $this->call(JUHUIZFSeeder::class);

        #For Reference to be deleted!!!!
        // $this->call(PaymentProviderSeeder::class);
        // $this->call(CurrencyPaymentProviderSeeder::class);
        // $this->call(PaymentProviderFieldSeeder::class);
        // $this->call(FieldCategoryPaymentProviderFieldSeeder::class);
        // $this->call(BankSeeder::class);
        
        
        #For Testing
        //$this->call(ProjectSeeder::class);
        //$this->call(PaymentSettingConfigSeeder::class);
    }
}
