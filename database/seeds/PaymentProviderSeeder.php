<?php

use App\PaymentProvider;
use Illuminate\Database\Seeder;

class PaymentProviderSeeder extends Seeder
{

	const ADMINISTRATOR_ID = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #LUUP
        $luup = new PaymentProvider;

        $luup->payment_provider_name = 'LUUP';
        $luup->payment_provider_code = 'luup';
        $luup->created_by = self::ADMINISTRATOR_ID;
        $luup->updated_by = self::ADMINISTRATOR_ID;

        $luup->save();

        #RPNpay
        $rpn = new PaymentProvider;

        $rpn->payment_provider_name = 'RPNpay';
        $rpn->payment_provider_code = 'rpn';
        $rpn->created_by = self::ADMINISTRATOR_ID;
        $rpn->updated_by = self::ADMINISTRATOR_ID;

        $rpn->save();
        
        #GMSTONE
        $gmstone = new PaymentProvider;

        $gmstone->payment_provider_name = 'GMSTONE';
        $gmstone->payment_provider_code = 'gmstone';
        $gmstone->created_by = self::ADMINISTRATOR_ID;
        $gmstone->updated_by = self::ADMINISTRATOR_ID;

        $gmstone->save();

        #EC365
        $ec365 = new PaymentProvider;

        $ec365->payment_provider_name = 'EC365';
        $ec365->payment_provider_code = 'ec365';
        $ec365->created_by = self::ADMINISTRATOR_ID;
        $ec365->updated_by = self::ADMINISTRATOR_ID;

        $ec365->save();

        #Astropay
        $astropay = new PaymentProvider;

        $astropay->payment_provider_name = 'Astropay';
        $astropay->payment_provider_code = 'astropay';
        $astropay->created_by = self::ADMINISTRATOR_ID;
        $astropay->updated_by = self::ADMINISTRATOR_ID;

        $astropay->save();
    }
}
