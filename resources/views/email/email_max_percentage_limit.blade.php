Hi {{$user_data->username}}, <br /><br />

This is to inform that your maximum percentage limit for <b>[{{$transaction->transaction->payment_setting->payment_provider->payment_provider_name}}]</b> has been reached. <br/>
Current Accumulated Deposit Amount is <b>[{{$accumulated_amount}}]</b>. <br/>
Kindly update your settings.

<br/><br/>

<i><b>Note:</b> Once the maximum amount has been reached, resulting incapability to accept future deposited amount from clients.</i>
