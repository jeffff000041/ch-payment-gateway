Hi {{$user_data->username}}, <br /><br />

This is to inform that you have received a payment with Transaction Number: <b>[ {{$transaction->transaction_number}} ]</b>.

<br /><br />

Please check and confirm the transaction.

<br /><br />

Click <a href="{{$redirect_url}}">here</a> to view details.
