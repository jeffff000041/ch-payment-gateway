<!DOCTYPE html>
<html>
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <link href="{{ URL::to('/') }}/css/style.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ URL::to('/') }}/js/axios.min.js"></script>
    <script src="{{ URL::to('/') }}/js/js-cookie.js"></script>
    <script src="{{ URL::to('/') }}/js/vue-js-cookie.min.js"></script>
</head>
<body>
@auth <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
    <a class="dropdown-item" href="{{ route('logout') }}"
       onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>@endauth
<div id="app">
    <v-app dark>
        <v-content>
            <v-container>
                @auth <p class="username">Uzumaki Naruto</p> @endauth
                <v-toolbar id="navigation">
                    <v-toolbar-title class="logo"><img src="../images/og-logo.png"></v-toolbar-title>
                    <v-spacer></v-spacer>

                    <v-toolbar-items>
                        @auth
                        <v-btn dark color="primary" flat @click="pageSection = 'account_management'">Account Management</v-btn>
                        <v-menu offset-y>
                            <v-btn slot="activator" color="primary" dark>
                                Payment Management
                            </v-btn>
                            <v-list>
                                <v-list-tile>
                                    <v-list-tile-title><a flat @click="pageSection = 'payment_settings'">Payment Settings</a></v-list-tile-title>
                                </v-list-tile>
                                <v-list-tile>
                                    <v-list-tile-title><a flat @click="pageSection = 'payment_transaction'">Payment Transaction</a></v-list-tile-title>
                                </v-list-tile>
                                <v-list-tile>
                                    <v-list-tile-title><a flat @click="pageSection = 'payment_logs'">Payment Logs</a></v-list-tile-title>
                                </v-list-tile>
                            </v-list>
                        </v-menu>
                        @endauth
                        @yield('login')
                    </v-toolbar-items>


                </v-toolbar>

                <section id="page">
                    <!--ACCOUNT MANAGEMENT-->
                    <div v-if="pageSection == 'account_management'" >
                        @yield('account_management')
                    </div>

                    <!--PAYMENT MANAGEMENT-->
                    <div v-if="pageSection == 'payment_logs'">
                        <superadmin-transaction-logs></superadmin-transaction-logs>
                        <admin-transaction-logs></admin-transaction-logs>
                        <client-transaction-logs></client-transaction-logs>
                    </div>

                    <div v-if="pageSection == 'payment_settings'">
                        <payment-settings></payment-settings>
                    </div>

                    <div v-if="pageSection == 'payment_transaction'">
                        <payment-transaction></payment-transaction>
                    </div>
                </section>
            </v-container>
        </v-content>
    </v-app>
</div>

<script src="https://cdn.rawgit.com/alertifyjs/alertify.js/v1.0.10/dist/js/alertify.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
<script src="{{ URL::to('/') }}/components/search.js"></script>
<script src="{{ URL::to('/') }}/components/addAccount.js"></script>
<script src="{{ URL::to('/') }}/components/addSettings.js"></script>
<script src="{{ URL::to('/') }}/components/editSettings.js"></script>
<script src="{{ URL::to('/') }}/components/setPriority.js"></script>
<script src="{{ URL::to('/') }}/components/editAccount.js"></script>
<script src="{{ URL::to('/') }}/components/deleteItem.js"></script>
<script src="{{ URL::to('/') }}/components/note.js"></script>
<script src="{{ URL::to('/') }}/components/updatePassword.js"></script>
<script src="{{ URL::to('/') }}/components/datePicker.js"></script>
<script src="{{ URL::to('/') }}/components/login.js"></script>

<script src="{{ URL::to('/') }}/components/pages/paymentSettings.js"></script>
<script src="{{ URL::to('/') }}/components/pages/paymentTransaction.js"></script>

<script src="{{ URL::to('/') }}/components/admin/listAccounts.js"></script>
<script src="{{ URL::to('/') }}/components/superAdmin/project/listAccounts.js"></script>
<script src="{{ URL::to('/') }}/components/superAdmin/admin/listAccounts.js"></script>
<script src="{{ URL::to('/') }}/components/superAdmin/client/listAccounts.js"></script>
<script src="{{ URL::to('/') }}/components/superAdmin/superadminTransactionLogs.js"></script>
<script src="{{ URL::to('/') }}/components/superAdmin/admin/adminTransactionLogs.js"></script>
<script src="{{ URL::to('/') }}/components/superAdmin/client/clientTransactionLogs.js"></script>
<script src="//cdn.jsdelivr.net/npm/sortablejs@1.7.0/Sortable.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.15.0/vuedraggable.min.js"></script>
<script src="https://unpkg.com/vue-rangedate-picker"></script>
<script src="{{ URL::to('/') }}/js/config.js"></script>
<script>

    var nathan = new Vue({
        el: '#app',
        data:{
            active: 0,
            pageTabSections: [
                {
                    title: 'Project',
                    component: 'project'
                },
                {
                    title: 'Admin',
                    component: 'admin'
                },
                {
                    title: 'Client',
                    component: 'client'
                }
            ],
            pageSection: 'account_management',
            componentSection: 'project'
        },
        methods: {
            Active(index) {
                this.active = index
            }
        }
    })
</script>
</body>
</html>
