@extends('layouts.app')

@section('account_management')

<nav>
    <v-btn v-for="(section, index) in pageTabSections" :class="{info: active === index, '': active !== index}" @click="Active(index)" @click.native="componentSection = section.component">@{{section.component}}</v-btn>
</nav>
<div v-if="componentSection == 'project'">
    <project-list-account></project-list-account>
</div>
<div v-if="componentSection == 'admin'">
    <super-admin-list-account></super-admin-list-account>
</div>
<div v-if="componentSection == 'client'">
    <client-list-account></client-list-account>
</div>
<!--                        <admin-list-accounts></admin-list-accounts>-->
<!-- <admin-list-accounts></a> -->

@endsection
