<?php

namespace Tests\Unit\Modules\PaymentProvider;

use Tests\TestCase;
use App\Modules\PaymentProvider\Connector\AstropayConnector;
use App\Modules\PaymentProvider\Factory as PaymentProviderFactory;

class PaymentProviderFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function it_expects_valid_connector()
    {
        //$astropayConnector = PaymentProviderFactory::make('astropay');

        //$this->assertEquals($astropayConnector, new AstropayConnector());
    }

    /**
     * @test
     */
    public function it_expects_exception_connector()
    {
        $this->expectException('App\Modules\PaymentProvider\Exceptions\ConnectorNotFoundException');

        PaymentProviderFactory::make('notprovider');
    }
}
