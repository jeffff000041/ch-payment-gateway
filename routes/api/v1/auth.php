<?php

use Illuminate\Http\Request;
use Laravel\Passport\Passport;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Auth Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
// Passport::routes();

Route::group(['prefix' => 'admin'], function() {
    Route::post('auth', 'AuthController@getToken');
    Route::post('register', 'AuthController@register');
    Route::post('auth/key', 'AuthController@createLoginKey');
	Route::post('auth/key/resend', 'AuthController@resendLoginKey');

	Route::group(['middleware' => ['auth:api', 'role:super_admin|admin|user|project']], function() {
	    Route::get('login', 'AuthController@login');
	    Route::get('user', 'AuthController@getUser');
	    Route::get('logout', 'AuthController@revokeToken');
	});
});

