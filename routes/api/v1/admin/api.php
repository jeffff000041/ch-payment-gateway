<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

#Payment Providers
Route::get('payment_providers', 'PaymentProviderController@index')
    ->middleware(['auth:api', 'role:super_admin|admin']);
Route::group(['prefix' => 'payment_provider', 'middleware' => ['auth:api', 'role:super_admin|admin']], function () {
    Route::get('{id}/fields', 'PaymentProviderController@getFields');
    Route::get('providers','PaymentProviderController@getPaymentProviders');
    Route::get('{code}/notes', 'PaymentProviderController@notes');
    Route::post('provider_settings','PaymentProviderController@updatePaymentProviderStatus');
});

#Projects
Route::get('projects', 'ProjectController@index')->middleware(['auth:api', 'role:super_admin|admin']);
Route::group(['prefix' => 'project', 'middleware' => ['auth:api', 'role:super_admin|admin']], function () {
    Route::post('', 'ProjectController@store');
    Route::get('{id}', 'ProjectController@show');
    Route::put('{id}', 'ProjectController@update');
    Route::delete('{id}', 'ProjectController@destroy');
});

#Clients
Route::group(['prefix' => 'clients', 'middleware' => ['auth:api', 'role:super_admin|admin']], function () {
    Route::get('', 'ClientController@index');
    Route::post('', 'ClientController@store');
    Route::put('{id}', 'ClientController@update');
    Route::delete('{id}', 'ClientController@destroy');
});

#Administrators
Route::get('administrators', 'AdminUserController@index')->middleware(['auth:api', 'role:super_admin']);
Route::group(['prefix' => 'administrator', 'middleware' => ['auth:api', 'role:super_admin']], function () {
    Route::post('', 'AdminUserController@store');
    Route::get('{id}', 'AdminUserController@show');
    Route::put('{id}', 'AdminUserController@update');
    Route::put('{id}/password', 'AdminUserController@updatePassword');
    Route::delete('{id}', 'AdminUserController@destroy');
});

#Account
Route::group(['prefix' => 'account', 'middleware' => ['auth:api', 'role:super_admin|admin']], function () {
    Route::put('', 'AccountController@update');
    Route::put('password/reset', 'AccountController@resetPassword');
    Route::post('image/upload', 'AccountController@imageUpload');
});


#Project Payment Settings
Route::group(['prefix' => 'payment_settings', 'middleware' => ['auth:api', 'role:super_admin|admin']], function () {
    Route::get('', 'PaymentSettingController@index');
    Route::get('unique_by_banks', 'PaymentSettingController@getPaymentProviderSettingsUniqueBank');
    Route::post('prioritize', 'PaymentSettingController@prioritize');
});

Route::group(['prefix' => 'payment_setting', 'middleware' => ['auth:api', 'role:super_admin|admin']], function () {

    Route::get('clients', 'PaymentSettingController@getClients');
    Route::get('banks', 'PaymentSettingController@getUniqueBanks');
    Route::get('bank/{id}/payment_settings', 'PaymentSettingController@getBankPaymentSettings');
    Route::get('bank/{id}/payment_providers', 'PaymentSettingController@getBankPaymentProviders');

    Route::post('', 'PaymentSettingController@store');
    Route::get('{id}', 'PaymentSettingController@show');
    Route::put('{id}', 'PaymentSettingController@update');
    Route::delete('{id}', 'PaymentSettingController@destroy');
});

#Transactions
Route::get('transactions', 'TransactionController@index')
    ->middleware(['auth:api', 'role:super_admin|admin']);

Route::group(['prefix' => 'transaction', 'middleware' => ['auth:api', 'role:super_admin|admin']], function () {

    Route::get('{id}/notes', 'TransactionController@notes');
    Route::post('{id}/note', 'TransactionController@note');
    Route::post('{id}/approval_status', 'TransactionController@approvalStatus');
    Route::get('export', 'TransactionController@exportTransactionLogs');
});

#forgot password
Route::group(['prefix' => 'password', 'middleware' => 'api'], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::post('reset', 'PasswordResetController@reset');
});


#whitelisting
Route::group(['prefix' => 'account', 'middleware' => ['auth:api', 'role:super_admin']], function () {
    Route::get('whitelist', 'OtpWhitelistController@index');
    Route::post('whitelist', 'OtpWhitelistController@create');
    Route::put('whitelist/{id}', 'OtpWhitelistController@edit');
    Route::delete('whitelist/{id}', 'OtpWhitelistController@delete');
});

#Currency
Route::group(['middleware' => ['auth:api', 'role:super_admin']], function () {
    Route::get('currencies', 'CurrencyController@index');
});

#Audit Logs
Route::get('audit_logs', 'PlatformController@index')
    ->middleware(['auth:api', 'role:super_admin|admin']);

Route::group(['prefix' => 'audit_log', 'middleware' => ['auth:api', 'role:super_admin|admin']], function () {
    Route::get('{id}/notes', 'PlatformController@notes');
    Route::post('{id}/note', 'PlatformController@note');
    Route::post('{id}/approval_status', 'PlatformController@approvalStatus');
    Route::get('export', 'PlatformController@exportAuditLogs');
});

#Audit Logs
Route::group(['prefix' => 'audit', 'middleware' => ['auth:api', 'role:super_admin|admin']], function () {
    Route::get('', 'AuditController@index');
    Route::get('export', 'AuditController@exportAuditLogs');
});
