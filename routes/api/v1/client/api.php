<?php

/*
|--------------------------------------------------------------------------
| API Routes for projects
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

#Currency
Route::get('currencies', 'CurrencyController@index')->middleware('client:transaction');

#Catch Payment provider response
Route::post('transaction/provider/update', 'TransactionController@updateTransactionByProvider');

Route::group(['prefix' => 'transaction', 'middleware' => 'client:transaction'], function () {
	
	Route::get('', 'TransactionController@index');

    #Get all banks available
    Route::get('currency/{currency_id}/banks', 'TransactionController@getAvailableBanks');
    Route::post('', 'TransactionController@store');
    Route::get('{id}/notes', 'TransactionController@notes');
    Route::post('{id}/note', 'TransactionController@note');
    Route::post('{id}/approval_status', 'TransactionController@approvalStatus');

});

#Payment Providers
Route::group(['prefix' => 'payment_provider', 'middleware' => ['client:admin']], function () {
    Route::get('{id}/fields', 'PaymentProviderController@getFields');
});

#Project Payment Settings
Route::group(['prefix' => 'payment_settings', 'middleware' => ['client:admin']], function () {
    Route::get('', 'PaymentSettingController@index');
    Route::get('unique_by_banks', 'PaymentSettingController@getPaymentProviderSettingsUniqueBank');
    Route::post('prioritize', 'PaymentSettingController@prioritize');
});

Route::group(['prefix' => 'payment_setting', 'middleware' => ['client:admin']], function () {

    Route::get('banks', 'PaymentSettingController@getUniqueBanks');
    Route::get('bank/{id}/payment_settings', 'PaymentSettingController@getBankPaymentSettings');
    Route::get('bank/{id}/payment_providers', 'PaymentSettingController@getBankPaymentProviders');

    Route::post('', 'PaymentSettingController@store')->middleware('payment_settings');
    Route::get('{id}', 'PaymentSettingController@show');
    Route::put('{id}', 'PaymentSettingController@update');
    Route::delete('{id}', 'PaymentSettingController@destroy');
});